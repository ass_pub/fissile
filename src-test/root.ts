import _var from "./export-default-var";
import _const from "./export-default-const";
import _fn from "./export-default-fn";
import _class from "./export-default-class";

import * as _intUsage from "./export-internal-usage";

import { _constA, _constB } from "./export-multiple";

import { _constA as _constAA, _constB as _constBB } from "./export-multiple";

console.log(_var);
console.log(_const);
console.log(_fn);
console.log(_class);
console.log(_intUsage);
console.log(_constA, _constB);
console.log(_constAA, _constBB);

let __var;
let __const;
let __fn;
let __class;

function fn() {

   let __var;
   let __const;
   let __fn;
   let __class;

}