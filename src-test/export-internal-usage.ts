export function exportedAndUsed() {
   console.log("Exported and used function");
}

function test2() {

   const exportedAndUsed = () => { console.log("Overriden exported function declaration"); };
   exportedAndUsed();

}

function test1() {
   exportedAndUsed();
}

test1();
test2();
