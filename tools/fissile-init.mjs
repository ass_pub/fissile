/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { spawn } from "child_process";
import { resolve } from "path";

async function runCommand(cwd, command, ...args) {

   try {

      const cmd = `${command} ${args.join(" ")}`;
      const err = await new Promise(

         (resolve, reject) => {
            try {

               console.log(`Current path: '${cwd}'`);
               console.log(`Executing command '${cmd}'`);
               console.log();

               const options = { shell: true, stdio: "inherit", cwd };

               const proc = spawn(command, args, options);

               proc.on("exit", (code) => {
                  resolve(code);
               })

               proc.on("error", (error) => {
                  reject(error);
               });

            } catch(e) {
               reject(e);
            }
         }

      )

      if (!err) {
         console.log("Command executed succesfully.");
      } else {
         console.error(`Command execution failed, error code: ${err}`);
      }

   } catch(e) {
      console.error(`Command execution failed.`, e);
      throw(e);
   }

}

async function installDeps(cwd) {

   console.log(`Installing project dependencies (${cwd})...`);
   console.log();
   await runCommand(cwd, "npm", "install");
   console.log();

}

async function build(cwd) {

   console.log(`Building Fissile tool (${cwd})...`);
   console.log();
   await runCommand(cwd, "node", "./node_modules/typescript/bin/tsc", "-p", "./tsconfig.json");
   console.log();

}

async function main() {

   console.log();
   console.log("Fissile project initialization");
   console.log();

   try {
      await installDeps(process.cwd());

      await installDeps(resolve(process.cwd(), "tools", "fissile-build"));
      await build(resolve(process.cwd(), "tools", "fissile-build"));

      await installDeps(resolve(process.cwd(), "tools", "fissile-dev-server"));
      await build(resolve(process.cwd(), "tools", "fissile-dev-server"));

      console.log("Fissile project initialization successfull!");

   } catch(e) {

      console.error("Fissile project initialization failed!", e);

   }

}

await main();
