/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import FissileBuildContext from "../FissileBuildContext";
import { PipelineManager } from "../pipeline/PipelineManager.js";
import { PipelineManagerDefault } from "../pipeline/PipelineManagerDefault.js";


/*


async function buildTS(context: FissileBuildContext) {

   try {

      const config = new TsConfig(context);
      const compiler = new TsCompiler(context, config);
      compiler.updateConfigFile("./tsconfig.json");

      context.diag.info("tasks-build-tsc", 0, `Starting TypeScript compiler v. ${compiler.version} for ${config.parsedCmdLine?.fileNames.length} files`);
      const result = compiler.compile(true, config.parsedCmdLine?.fileNames);

      if (result.preEmitDiagnostics.length > 0) {

         for(const tsdiag of result.preEmitDiagnostics) {

            let diagStr = compiler.formatDiagnostics(
               tsdiag,
               {
                  getCurrentDirectory: (): string => { return ""; },
                  getCanonicalFileName: (fileName: string) => { return fileName; },
                  getNewLine: () => { return "\n"; }
               }
            );

            if (diagStr.endsWith("\r\n")) diagStr = diagStr.substr(0, diagStr.length - 2);
            if (diagStr.endsWith("\n")) diagStr = diagStr.substr(0, diagStr.length - 1);

            context.diag.error("tasks-build-tsc", `${diagStr}`);

         }

      } {
         context.diag.info("tasks-build-tsc", 0, `TypeScript compoler finished with no errors. ${result.writtenFiles.length} files emmited.`);
      }


   } catch (e) {
      context.diag.error("tasks-build", `Failed to compile TypeScript files: ${e}`);
   }


}
*/

export default async function build(context: FissileBuildContext) {

   context.diag.info("task-build", "build", 0, "Starting build task");

   const pipelineManager: PipelineManager = new PipelineManagerDefault(context);

   await pipelineManager.initAsync();
   const result = await pipelineManager.runAsync(false);

   if (result) {
      context.diag.info("task-build", "build", 0, "Build task finished.");
   } else {
      context.diag.error("task-build", "build", "Build task didn't finished.");
   }

}