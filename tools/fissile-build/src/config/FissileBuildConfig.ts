/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { KeyValue } from "../lib/@types/KeyValue";
import { PluginCtor, PluginOptions } from "../plugins/Plugin";

export type FissileBuildEnvironment = "dev" | "prod";

type KeyValueDevProd<T> = { [key in FissileBuildEnvironment]: T }
type KeyValueDevProdCommon<T> = { [key in (FissileBuildEnvironment | "common")]: T }

export interface FissileBuildConfig {

   /**
    * Environment (can be "dev" or "prod")
    * This is just a default value if the "--env" argument is not passed to the fissile-build
    */
   env: FissileBuildEnvironment

   /**
    * Directory containing source files
    * Used by some pluging (i.e. eslint) to determine list of files to be processed
    */
   srcDir: string,

   /**
    * Target root directory for all files being processed
    * Please note that target directory must be explicitly configured in build tools in use
    */
   distDir: KeyValueDevProd<string>;

   /**
    * Extermal plugins to be loaded
    * External plugin must:
    * - be imported to this file and the class name must be put into this array
    */
   plugins: PluginCtor<any>[];

   /**
    * File processing pipelines
    */
   pipelines: KeyValueDevProdCommon<KeyValue<PluginOptions>>;

   /**
    * References to plugin config files
    * If no config file is provided for given plugin and env configuration, default config will be used
    */
   pluginConfig: KeyValueDevProdCommon<any>;

}

export const defaultConfig: FissileBuildConfig = {

   /**
    * Environment (can be "dev" or "prod")
    * This is just a default value if the "--env" argument is not passed to the fissile-build
    */
   env: "dev",

   /**
    * Directory containing source files
    * Used by some pluging (i.e. eslint) to determine list of files to be processed
    */
   srcDir: "./src",

   /**
    * Target root directory for all files being processed
    * Please note that target directory must be explicitly configured in build tools in use
    */
   distDir: {
      "dev": "./dist/dev",
      "prod": "./dist/prod"
   },

   /**
    * Extermal plugins to be loaded
    * External plugin must:
    * - be imported to this file and the class name must be put into this array
    * - be inherited from the abstract Plugin class
    */
   plugins: [
   ],

   /**
    * File processing pipelines to be used
    * Please note that because of configuration and builder code simplicity it
    * is not possible to have various pipeline plugin configurations. It seemed
    * not to be necessary during the version 1.0 design and can be added in
    * future versions.
    */
   pipelines: {

      /** env configuration */

      /* pipeline name   pipeline configuration                            plugins to run in order */

      "common": {
         "eslint":       { fileProvider: "eslint",     stopOnError: false, plugins: [ "eslint" ] },
         "tplcheck":     { fileProvider: "tplcheck",   stopOnError: false, plugins: [ "tplcheck", "emit" ] },
         "copy":         { fileProvider: "copy",       stopOnError: false, plugins: [ "copy" ] },
      },

      "dev": {
         "typescript":   { fileProvider: "typescript", stopOnError: false, plugins: [ "typescript", "emit" ] },
      },

      "prod": {
         "typescript":   { fileProvider: "typescript", stopOnError: false, plugins: [ "eslint", "typescript", "treeshaker", "uglify", "emit" ] },
      }

   },

   /**
    * References to plugin config files
    * - If no config file is provided (or found) for given plugin and env configuration, default config will be used
    * - Paths are relative to the project root
    * - For simplicity of working with some libraries (typescript, eslint) and also IDE compatibility,
    *   config files are in JSON format and are separate for each plugin
    */
   pluginConfig: {

      "common": {
         "copy": "./config/fsb-copy.json",
         "eslint": "./config/eslint.json",
         "tplcheck": "./config/fsb-tplcheck.json"
      },

      "dev": {
         "docgen": "./config/fsb-docgen.json",
         "typescript": "./config/tsconfig.dev.json"
      },

      "prod": {
         "treeshaker": "./config/fsb-treeshaker.json",
         "typescript": "./config/tsconfig.prod.json",
         "uglify": "./config/fsb-uglify.json"
      }

   }

};