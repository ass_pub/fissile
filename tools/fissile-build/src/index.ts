/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { defaultConfig, FissileBuildConfig } from "./config/FissileBuildConfig.js";
import FissileBuildContext from "./FissileBuildContext";

import * as diag from "./lib/diag/index.js";
import diagHeader from "./_header.js";
import diagUsage from "./_usage.js";

import build from "./tasks/build.js";
import clean from "./tasks/clean.js";
import watch from "./tasks/watch.js";

interface CmdLineOptions {

   help?: boolean;
   debugLevel?: number;
   noDebugColors?: boolean
   env?: "dev" | "prod";
   task?: "clean" | "build" | "watch";

}


function sanitizeConfig(config: FissileBuildConfig, errors: string[]): FissileBuildConfig {

   const cfg = config;

   // sanitization

   if (cfg.env === undefined) cfg.env = defaultConfig.env;

   if (cfg.distDir === undefined) cfg.distDir = defaultConfig.distDir;
   if (cfg.distDir.dev === undefined) cfg.distDir.dev = defaultConfig.distDir.dev;
   if (cfg.distDir.prod === undefined) cfg.distDir.prod = defaultConfig.distDir.prod;

   if (cfg.plugins === undefined) cfg.plugins = defaultConfig.plugins;

   if (cfg.pipelines === undefined) cfg.pipelines = defaultConfig.pipelines;
   if (cfg.pipelines.common === undefined) cfg.pipelines.common = defaultConfig.pipelines.common;
   if (cfg.pipelines.dev === undefined) cfg.pipelines.dev = defaultConfig.pipelines.dev;
   if (cfg.pipelines.prod === undefined) cfg.pipelines.prod = defaultConfig.pipelines.prod;

   if (cfg.pluginConfig === undefined) cfg.pluginConfig = defaultConfig.pluginConfig;
   if (cfg.pluginConfig.common === undefined) cfg.pluginConfig.common = defaultConfig.pluginConfig.common;
   if (cfg.pluginConfig.dev === undefined) cfg.pluginConfig.dev = defaultConfig.pluginConfig.dev;
   if (cfg.pluginConfig.prod === undefined) cfg.pluginConfig.prod = defaultConfig.pluginConfig.prod;

   // simple validation - can be broken easily

   if (cfg.env !== "dev" && cfg.env !== "prod") errors.push(`Invalid 'env' option value "'${cfg.env}'": "dev" or "prod" expected`);

   for (const k in cfg.distDir) {
      if (k !== "dev" && k !== "prod") {
         errors.push(`Invalid 'distDir' option environment name "'${k}'": "dev" or "prod" expected`);
      } else {
         if (typeof cfg.distDir[k] !== "string") errors.push(`Invalid 'distDir["${k}"]' option: string value expected`);
      }
   }

   if (!(cfg.plugins instanceof Array)) {
      errors.push(`Invalid 'plugins' option "'${cfg.plugins}'": array of plugin constructors expected.`);
   } else {
      cfg.plugins.forEach(plugin => {
         if (typeof plugin !== "function") errors.push(`Invalid 'plugins' value "'${plugin}'": class constructor expected`);
      });
   }

   for (const k in cfg.pipelines) {
      if (k !== "dev" && k !== "prod" && k !== "common") errors.push(`Invalid 'pipelines' option environment name "'${k}'": "dev", "prod" or "common" expected`);
      if (typeof (<any>cfg.pipelines)[k] !== "object") errors.push(`Invalid 'pipelines["${k}"]' option: KeyValue object expected`);
   }

   for (const k in cfg.pluginConfig) {
      if (k !== "dev" && k !== "prod" && k !== "common") errors.push(`Invalid 'pluginConfig' option environment name "'${k}'": "dev", "prod" or "common" expected`);
      if (typeof (<any>cfg.pluginConfig)[k] !== "object") errors.push(`Invalid 'pluginConfig["${k}"]' option: KeyValue object expected`);
   }

   return cfg;

}

function setupContext(config: FissileBuildConfig): FissileBuildContext {

   const context: FissileBuildContext = <any>{};

   context.fbConfig = config;
   context.diag = {
      info: diag.info,
      warn: diag.warn,
      error: diag.error
   };

   return context;

}

function printConfig(context: FissileBuildContext): void {

   function walkObj(obj: any, indent = "   "): void {

      for(const key in obj) {

         if (typeof(obj[key]) === "object" && !(obj[key] instanceof Array)) {

            context.diag.info("main", "printConfig", 0, `${indent}${key}:`);
            walkObj(obj[key], indent + "   ");
            continue;

         }

         if ((obj[key] instanceof Array)) {
            context.diag.info("main", "printConfig", 0, `${indent}${key}: [ '${obj[key].join(", ")}' ]`);
            continue;
         }

         context.diag.info("main", "printConfig", 0, `${indent}${key}: '${obj[key]}'`);

      }

   }

   context.diag.info("main", "printConfig", 0, "Configuration in use:");
   walkObj(context.fbConfig);

}

function getCmdLineOptions(errors: string[]): CmdLineOptions {

   const result: CmdLineOptions = {};

   let i = 2;

   while (i < process.argv.length) {

      switch(process.argv[i]) {

         case "-h":
         case "--help":

            result.help = true;
            break;

         case "-d":
         case "--debug":

            if (i < process.argv.length - 1) {

               const level = parseInt(process.argv[i + 1]);
               if (isNaN(level)) {
                  errors.push("Numeric argument (debug level) expected after -d/--debug option argument");
                  break;
               }

               result.debugLevel = level;
               i++;

            } else {
               errors.push("Debug level argument expected after -d/--debug option argument");
               break;
            }
            break;

         case "-n":
         case "--no-colors":
            result.noDebugColors = true;
            break;

         case "-e":
         case "--env":

            if (i < process.argv.length - 1) {

               const env = process.argv[i + 1];
               if (env !== "dev" && env !== "prod") {
                  errors.push("'dev' or 'prod' value expected after -e/--env option argument");
                  break;
               }

               result.env = env;
               i++;

            } else {
               errors.push("Environment name argument expected after -e/--env option argument");
               break;
            }
            break;

         case "-c":
         case "--clean":
            if (result.task !== undefined) {
               errors.push("Only single task can be specified");
               break;
            }
            result.task = "clean";
            break;

         case "-b":
         case "--build":
            if (result.task !== undefined) {
               errors.push("Only single task can be specified");
               break;
            }
            result.task = "build";
            break;

         case "-w":
         case "--watch":
            if (result.task !== undefined) {
               errors.push("Only single task can be specified");
               break;
            }
            result.task = "watch";
            break;

         default:
            errors.push(`Invalid command line argument ${process.argv[i]}`);
      }

      if (errors.length > 0) break;
      i++;

   }

   return result;

}

export default async function main(config: FissileBuildConfig): Promise<void> {

   /**
    * Command line argument parsing
    */

   const cmdErrors: string[] = [];
   const cmdLineOptions = getCmdLineOptions(cmdErrors);

   if (cmdErrors.length > 0) {
      diag.info(diagHeader);
      diag.info(diagUsage);
      cmdErrors.forEach(error => diag.error("main", "main", `Command line argument error: ${error}`));
      process.exit(1);
   }


   /**
    * Help
    */

   if (cmdLineOptions.help === true) {
      diag.info(diagHeader);
      diag.info(diagUsage);
      process.exit(0);
   }


   /**
    * Task check
    */

   if (cmdLineOptions.task === undefined) {
      diag.info(diagHeader);
      diag.info(diagUsage);
      diag.error("main", "main", "No task specified. Nothing to do.");
      process.exit(2);
   }


   /**
    * Configuration check
    */

   const cfgErrors: string[] = [];
   const cfg = sanitizeConfig(config, cfgErrors);

   if (cfgErrors.length > 0) {
      diag.info(diagHeader);
      cfgErrors.forEach(error => diag.error("main", "main", `Configuration error: ${error}`));
      process.exit(3);
   }


   /**
    * Configuration patching (from cmd line options)
    */

   if (cmdLineOptions.env !== undefined) cfg.env = cmdLineOptions.env;


   /**
    * Diag configuration
    */

   if (cmdLineOptions.debugLevel !== undefined) diag.debugLevel(cmdLineOptions.debugLevel);
   if (cmdLineOptions.noDebugColors === true) diag.disableColors();


   /**
    * Initialization & start
    */

   const context = setupContext(cfg);

   context.diag.info(diagHeader);
   printConfig(context);

   switch(cmdLineOptions.task) {
      case "clean": await clean(context); break;
      case "build": await build(context); break;
      case "watch": await watch(context); break;
   }

}
