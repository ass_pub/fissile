/* eslint-disable no-inner-declarations */
/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2021 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { KeyValue } from "../@types/KeyValue";

// eslint-disable-next-line @typescript-eslint/no-namespace
namespace colors {

   interface ColorData {
      open: string;
      close: string;
   }

   const colors: KeyValue<ColorData> = {
      reset: { open: "\x1B[0m", close: "\x1B[0m" },
      bold: { open: "\x1B[1m", close: "\x1B[22m" },
      dim: { open: "\x1B[2m", close: "\x1B[22m" },
      italic: { open: "\x1B[3m", close: "\x1B[23m" },
      underline: { open: "\x1B[4m", close: "\x1B[24m" },
      inverse: { open: "\x1B[7m", close: "\x1B[27m" },
      hidden: { open: "\x1B[8m", close: "\x1B[28m" },
      strikethrough: { open: "\x1B[9m", close: "\x1B[29m" },
      black: { open: "\x1B[30m", close: "\x1B[39m" },
      red: { open: "\x1B[31m", close: "\x1B[39m" },
      green: { open: "\x1B[32m", close: "\x1B[39m" },
      yellow: { open: "\x1B[33m", close: "\x1B[39m" },
      blue: { open: "\x1B[34m", close: "\x1B[39m" },
      magenta: { open: "\x1B[35m", close: "\x1B[39m" },
      cyan: { open: "\x1B[36m", close: "\x1B[39m" },
      white: { open: "\x1B[37m", close: "\x1B[39m" },
      gray: { open: "\x1B[90m", close: "\x1B[39m" },
      grey: { open: "\x1B[90m", close: "\x1B[39m" },
      brightRed: { open: "\x1B[91m", close: "\x1B[39m" },
      brightGreen: { open: "\x1B[92m", close: "\x1B[39m" },
      brightYellow: { open: "\x1B[93m", close: "\x1B[39m" },
      brightBlue: { open: "\x1B[94m", close: "\x1B[39m" },
      brightMagenta: { open: "\x1B[95m", close: "\x1B[39m" },
      brightCyan: { open: "\x1B[96m", close: "\x1B[39m" },
      brightWhite: { open: "\x1B[97m", close: "\x1B[39m" }
   };

   function terminalSupportsColors(): boolean {

      if (process.env.FORCE_COLOR) {
         return true;
      } else if (process.env.FORCE_NO_COLOR) {
         return false;
      } else if (!process.stdout.isTTY) {
         return false;
      } else if (process.platform === "win32") {
         return true;
      } else if (process.env.CI || process.env.TEAMCITY_VERSION) {
         return false;
      } else if (process.env.COLORTERM) {
         return true;
      } else if (process.env.TERM === "dumb") {
         return false;
      } else if (/^xterm-256(color)?/.test(process.env.TERM || "")) {
         return true;
      } else {
         return /^screen|^xterm|^vt100|color|ansi|cygwin|linux/i.test(process.env.TERM || "");
      }

   }

   let enabled = true;

   function applyColor(color: ColorData, text: string): string {
      // if (!supportsColors) return text;
      if (!enabled) return text;
      // eslint-disable-next-line no-control-regex
      const txt = text.replace(/\x1B\[39m/gm, color.close + color.open);
      return color.open + txt + color.close;
   }

   export type Color = (text: string) => string;

   export function stripColors(text: string): string {
      // eslint-disable-next-line no-control-regex
      return text.replace(/\x1B\[\d+m/g, "");
   }

   export function reset(text: string): string { return applyColor(colors["reset"], text); }
   export function bold(text: string): string { return applyColor(colors["bold"], text); }
   export function dim(text: string): string { return applyColor(colors["dim"], text); }
   export function italic(text: string): string { return applyColor(colors["italic"], text); }
   export function underline(text: string): string { return applyColor(colors["underline"], text); }
   export function inverse(text: string): string { return applyColor(colors["inverse"], text); }
   export function hidden(text: string): string { return applyColor(colors["hidden"], text); }
   export function strikethrough(text: string): string { return applyColor(colors["strikethrough"], text); }
   export function black(text: string): string { return applyColor(colors["black"], text); }
   export function red(text: string): string { return applyColor(colors["red"], text); }
   export function green(text: string): string { return applyColor(colors["green"], text); }
   export function yellow(text: string): string { return applyColor(colors["yellow"], text); }
   export function blue(text: string): string { return applyColor(colors["blue"], text); }
   export function magenta(text: string): string { return applyColor(colors["magenta"], text); }
   export function cyan(text: string): string { return applyColor(colors["cyan"], text); }
   export function white(text: string): string { return applyColor(colors["white"], text); }
   export function gray(text: string): string { return applyColor(colors["gray"], text); }
   export function grey(text: string): string { return applyColor(colors["grey"], text); }
   export function brightRed(text: string): string { return applyColor(colors["brightRed"], text); }
   export function brightGreen(text: string): string { return applyColor(colors["brightGreen"], text); }
   export function brightYellow(text: string): string { return applyColor(colors["brightYellow"], text); }
   export function brightBlue(text: string): string { return applyColor(colors["brightBlue"], text); }
   export function brightMagenta(text: string): string { return applyColor(colors["brightMagenta"], text); }
   export function brightCyan(text: string): string { return applyColor(colors["brightCyan"], text); }
   export function brightWhite(text: string): string { return applyColor(colors["brightWhite"], text); }

   export function disable() { enabled = false; }
   export function enable() { enabled = true; }

   export const supportsColors = terminalSupportsColors();

}

export default colors;