/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2021 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { readFileSync } from "fs";
import * as sourceMap from "source-map-js";
import { fileURLToPath } from "url";

import colors from "./colors.js";

/**
 * Current flag specifying if the debugging is enabled or not (default true)
 */
let _enabled = true;

/**
 * Current value of logging level (default = 0)
 */
let _debugLevel = 9;

/**
 * Start time to be used to determine the log message time since log module is loaded
 */
const startTime = Date.now();


/**
 * Adds spaces to end of the string to fulfill requirement on total number of characters
 * @param s String to which spaces will be added
 * @param totalChars Total number of characters the string has to contain
 */
function addSpacesAfter(s: string, totalChars: number): string {
   let out: string = s;
   while (out.length < totalChars) out += " ";
   return out;
}


/**
 * Adds spaces to beginning of the string to fulfill requirement on total number of characters
 * @param s String to which spaces will be added
 * @param totalChars Total number of characters the string has to contain
 */
function addSpacesBefore(s: string, totalChars: number): string {
   let out = s;
   while (out.length < totalChars) out = " " + out;
   return out;
}


/**
 * Converts miliseconds to time in HH:MM:SS:MSC format
 * @param ms Time in miliseconds to be converted
 */
function msToTime(ms: number): string {
   return new Date(ms).toISOString().slice(11, -1);
}


function colorizeQuoted(message: string): string {

   const match = message.match(/'.*?'/g);

   if (match) {
      for (const m of match) {
         const str = m.substring(1, m.length - 1);
         const rx = new RegExp(`${str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")}`, "gm");
         message = message.replace(rx, colors.cyan(str));
      }
   }

   return message;

}


/**
 * Logs a message to the console
 * @param color Color to be used to display the message (usually white, yellow, red)
 * @param severity Severity of the log message (based on the current log level it will be shown or not)
 * @param moduleName Name of the module or class where the log request was generated
 * @param message Message to be displayed
 */
function log(color?: colors.Color, severity?: string, moduleName?: string, methodName?: string, message?: string, ...args: any[]): void {

   const srcFileLineCol = getSourceFileLineCol();

   if (color && severity && moduleName && message) {

      const msgTime = msToTime(Date.now() - startTime);
      const sev = addSpacesAfter(severity, 7);
      const pid = addSpacesBefore(process.pid.toString(), 5);
      const mod = addSpacesAfter(moduleName, 20);
      const meth = addSpacesAfter(methodName || "", 20);
      const src = addSpacesAfter(srcFileLineCol, 50);

      message = colorizeQuoted(message);

      if (args.length > 0) {
         console.log(color(`${msgTime} ${pid} ${sev} ${mod} ${meth} ${src} ${message}`));

         let i = 0;
         let indent = "";
         while (message[i] === " " && i < message.length) {
            indent += " ";
            i++;
         }

         logArgs(color, `${msgTime} ${pid} ${sev} ${mod} ${meth} ${src} `, indent, ...args);
      } else {
         console.log(color(`${msgTime} ${pid} ${sev} ${mod} ${meth} ${src} ${message}`));
      }

   } else if (color && message) {

      console.log(color(message));

   } else if (message) {

      console.log(message);

   } else {

      console.log("");

   }

}


function logArgs(color: colors.Color, msg: string, indent: string, ...args: any[]) {

   for (const arg of args) {

      switch(typeof arg) {

         case "bigint":
         case "boolean":
         case "number":
         case "string":
            console.log(color(`${colorizeQuoted(msg)}${indent}   ${colorizeQuoted(arg.toString())}`));
            break;

         case "object": {
            const lines = JSON.stringify(arg, null, 3).split("\n");

            if (lines.length > 1) {
               lines.shift();
               lines.length = lines.length - 1;
            }

            for (const line of lines) {
               if (line === "[]" || line === "{}") continue;
               let out = line.replace(/"/g, "'");
               out = colorizeQuoted(out.replace(/\\\\/gm, "\\"));
               console.log(color(`${msg}${indent}`) + `${out}`);
            }
         }

      }
   }
}


function getSourceFileLineCol(): string {

   let stackTrace: string;

   try {
      throw new Error("Get Stack Trace");
   } catch (e) {
      stackTrace = (<Error>e).stack ?? "";
   }

   if (stackTrace === "") return "";

   const lines = stackTrace.split("\n");
   lines.shift();
   lines.shift();
   lines.shift();
   lines.shift();

   if (lines.length < 1) return "";

   let fileLineCol;

   if (lines[0].indexOf("(file:///") !== -1) {
      fileLineCol = lines[0].substring(lines[0].indexOf("(file:///") + 1);
      fileLineCol = fileLineCol.substring(0, fileLineCol.length - 1);
   } else {
      fileLineCol = lines[0].substring(lines[0].indexOf("file:///"));
   }

   return mapToSource(fileLineCol);

}

function mapToSource(fileLineCol: string): string {

   const colPos = fileLineCol.lastIndexOf(":") + 1;
   const linePos = fileLineCol.lastIndexOf(":", colPos - 2) + 1;

   const fileURL = fileLineCol.substring(0, linePos - 1);
   const line = parseInt(fileLineCol.substring(linePos, colPos - 1));
   const column = parseInt(fileLineCol.substring(colPos));

   const file = fileURLToPath(fileURL);

   const js = readFileSync(file).toString();
   const sourceMappingRx = /^\/\/#\s*sourceMappingURL=(.*)$/gm.exec(js);

   if (!sourceMappingRx) return fileLineCol;
   if (sourceMappingRx.length < 2) return fileLineCol;

   let mappingsJSON: string;

   if (sourceMappingRx[1].startsWith("data:application/json;base64,")) {
      mappingsJSON = Buffer.from(sourceMappingRx[1].substring(29), "base64").toString();
   } else {
      // other than inline mappings are unssuported now
      mappingsJSON = "";
   }

   if (!mappingsJSON) return fileLineCol;

   const smc = new sourceMap.SourceMapConsumer(JSON.parse(mappingsJSON));

   const orig = smc.originalPositionFor({ line, column });

   if (orig.source === null || orig.line === null || orig.column === null) return fileLineCol;

   return `${orig.source.replace(/(\.\.\/)*/, "./")}:${orig.line}:${orig.column}`;

}


/**
 * Enables or disables logging completely
 * Logging is enabled by default
 * @param value Specifies if the logging will be enabled or disabled
 */
export function enabled(value: boolean): void {
   _enabled = value;
}


/**
 * Sets the debug level
 * Default logging level is 0
 * @param value Debug level in range from 0 to x
 */
export function debugLevel(value: number): void {
   if (value < 0) return;
   _debugLevel = value;
}


/**
 * Disables colored console output
 */
export function disableColors(): void {
   colors.disable();
}


/**
 * Enables colored console output
 */
export function enableColors(): void {
   colors.enable();
}


export function info(message?: string): void;
export function info(module?: string, methodName?: string, level?: number, message?: string, ...args: any[]): void;

/**
 * Logs an information message
 * @param module Name of the class or module where log event occured (usually this.constructor.name)
 * @param level Debug or verbosity level (message will be shown only if debug level is set to same or higher value)
 * @param message Message to be shown. Text in single quotes will be highlighted (i.e. "exmaple 'log' message")
 */
export function info(module?: string, methodName?: string, level = 0, message?: string, ...args: any[]): void {

   if (!_enabled || level > _debugLevel) return;

   if (message) {
      log(colors.white, "INFO", module, methodName, message, ...args);
   } else if (module) {
      console.log(colors.white(module));
   } else {
      console.log();
   }

}


/**
 * Logs an warning message
 * @param module Name of the class or module where log event occured (usually this.constructor.name)
 * @param message Message to be shown. Text in single quotes will be highlighted (i.e. "exmaple 'log' message")
 */
export function warn(module: string, methodName: string, message: string, ...args: any[]): void {
   if (!_enabled) return;
   log(colors.yellow, "WARNING", module, methodName, message, ...args);
}


/**
 * Logs an error message
 * @param module Name of the class or module where log event occured (usually this.constructor.name)
 * @param message Message to be shown. Text in single quotes will be highlighted (i.e. "exmaple 'log' message")
 */
export function error(module: string, methodName: string, message: string, ...args: any[]): void {
   log(colors.red, "ERROR", module, methodName, message, ...args);
}
