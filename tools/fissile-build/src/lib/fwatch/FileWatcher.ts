/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2021 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import * as fs from "fs";
import * as path from "path";

import * as diag from "../diag/log.js";

import { FileEventInfo } from "./FileEventInfo";
import { WFSWatcher } from "./WFSWatcher";
import { WatchedFiles } from "./WatchedFiles";
import { FileEventListener } from "./FileEventListener";
import { FileType } from "./FileType.js";
import { dirname } from "path";

interface GroupByListener {
   listener: FileEventListener;
   events: FileEventInfo[];
}

interface ForcedEventFileInfo {
   [fileName: string]: fs.Stats;
}

/**
 * Simple File Watcher implementation (Windows, OSX, Linux)
 *
 * - implements feature of event aggreagtion
 *    - same events on the same file fired in configured timeframe are aggregated to single event
 * - implements collection of events within defined time frame from first event occured
 *    - usually it is necessary to wait for multiple events before file processing can be done by build tools
 *    - this prevents multiple calls to be performed on the build tool when multiple files are modified
 * Although directories can be watched and basically internally only directories are watched, events are fired
 * only in case the file to throw the event is registered
 *
 * FileWatcher is using functionalities of NodeJS for file system watching so same caveats applies
 */
export class FileWatcher {

   /**
    * Defines for how long after the first FS event captured are other events collected and aggregated before the final event is thrown
    */
   #FIRE_EVENT_DELAY: number;

   /**
    * Defines a time frame for aggragation of same events occured on same files
    */
   #EVENT_AGGREGATION_TIME: number;

   /**
    * Holds list of files being watched and maps listeners and watcher to them
    */
   #watchedFiles: WatchedFiles;

   /**
    * Holds list of directories being watched and maps listeners and watcher to them
    */
   #watchedDirs: WatchedFiles;

   /**
    * List of aggregated events
    */
   #aggregated: FileEventInfo[];

   /**
    * Indicator that first event if bunch occured and currently is running collection of other events during #FIRE_EVENT_DELAY time
    */
   #waitingNextEvents: boolean;

   /**
    * Information about files collected before forced eevent is trhown
    * Normal events are ignored if the stats of the file are the same as stored in this map
    */
   #forcedEventFileInfo: ForcedEventFileInfo;

   constructor() {

      this.#EVENT_AGGREGATION_TIME = 100;
      this.#FIRE_EVENT_DELAY = 1000;

      this.#watchedFiles = {};
      this.#watchedDirs = {};
      this.#aggregated = [];
      this.#waitingNextEvents = false;
      this.#forcedEventFileInfo = {};
   }

   /**
    * Starts watching given files and direcories
    * At least one root directory for watching should be specified
    * @param filesAndDirs Files and directories to be watched
    * @param listener Listener to be called when file or directory change occurs
    * @param recursive Specifies if complete directory structures starting by passed directories will be watched
    */
   public watch(filesAndDirs: string[], listener: FileEventListener, recursive = false): boolean {

      const dirs: string[] = [];
      const files: string[] = [];

      // separate files and directories
      for (const fd of filesAndDirs) {
         if (!fs.existsSync(fd)) continue;
         const stat = fs.statSync(fd);
         if (stat.isDirectory()) dirs.push(fd);
         if (stat.isFile()) files.push(fd);
      }

      // extract directories and add them to dirs if not exist already
      // store dirs to be watched to global list ans assign listeners to them
      for (const f of files) {

         const dir = path.dirname(f);
         if (dirs.indexOf(dir) === -1) dirs.push(dir);

         if (Object.prototype.hasOwnProperty.call(this.#watchedFiles, f)) {
            if (this.#watchedFiles[f].listeners.indexOf(listener) === -1) {
               this.#watchedFiles[f].listeners.push(listener);
            }
         } else {
            this.#watchedFiles[f] = {
               listeners: [ listener ]
            };
         }
      }

      // collect complete directory structure for dirs to be watched
      let wDirs: string[];

      if (recursive) {
         wDirs = [];
         for (const d of dirs) {
            this.#findSubDirs(d, wDirs);
         }
      } else {
         wDirs = dirs;
      }

      // store dirs to be watched to global list and assign listeners to them
      for (const d of wDirs) {
         if (Object.prototype.hasOwnProperty.call(this.#watchedDirs, d)) {
            if (this.#watchedDirs[d].listeners.indexOf(listener) === -1) {
               this.#watchedDirs[d].listeners.push(listener);
            }
         } else {
            this.#watchedDirs[d] = {
               listeners: [ listener ]
            };
         }
      }

      // watch collected dirs for changes
      return this.#watchDirs(wDirs);
   }

   /**
    * Forces processing of files passed as parameter. Next event for each file
    * will be ignored if stats of the file will be the same
    * This method can be used to avoid file event aggregation and waiting delays
    * @param files Files for which the event will be forced. Normalized paths are expected
    */
   public forceChangeEvents(files: string[]): void {

      const groupByListener: GroupByListener[] = [];

      for (const file of files) {
         this.#forcedEventFileInfo[file] = fs.statSync(file);

         const eventInfo: FileEventInfo = {
            eventName: "change",
            fileType: FileType.File,
            fileName: file,
            timestamp: Date.now(),
            stats: this.#forcedEventFileInfo[file]
         };

         if (Object.prototype.hasOwnProperty.call(this.#watchedFiles, file)) {
            diag.info(this.constructor.name, "forceChangeEvents", 4, `Forcing event: '${eventInfo.eventName}', '${eventInfo.fileName}'`);
            this.#groupByListener(this.#watchedFiles[file].listeners, eventInfo, groupByListener);
         }
      }

      for (const gpl of groupByListener) {
         gpl.listener(gpl.events);
      }

   }

   /**
    * Unwatches file or directory being watched and releases held resources
    * @param fileOrDirectory File or directory to be unwatched
    */
   /*public unwatch(fileOrDirectory: string): void {
   }*/

   /**
    * Collect list of subdirectories recursively
    * @param dir Directory to be scanned
    * @param dirs Array to be filled with discovered sirectories
    */
   #findSubDirs(dir: string, dirs: string[]): void {

      if (dirs.indexOf(dir) === -1) dirs.push(dir);

      const dc = fs.readdirSync(dir);
      for (const fd of dc) {
         const fullFn = path.join(dir, fd);
         const stat = fs.statSync(fullFn);
         if (stat.isDirectory()) this.#findSubDirs(fullFn, dirs);
      }

   }

   /**
    * Starts watching on all passed directories if they are not watched already
    * @param dirs List of directories to be watched
    */
   #watchDirs(dirs: string[]): boolean {

      try {

         for (const d of dirs) {
            if (!this.#watchedDirs[d] || !this.#watchedDirs[d].watcher) this.#watchDir(d);
         }

         return true;

      } catch (e) {
         diag.error(this.constructor.name, "watchDirs", `Failed to start file watcher: '${e}'`);
         return false;
      }
   }

   /**
    * Uses NodeJS File watcher to create watcher on passed directory
    * Caller must take care of thrown exceptions
    * @param dir Directory to be watched
    * @throws Exceptions fom NodeJS fs.watch
    */
   #watchDir(dir: string): void {

      let wdir: string;

      // on windows, always watch the cwd
      if (process.platform === "win32") {
         wdir = process.cwd();
      } else {
         wdir = dir;
      }

      // Create watcher object
      const watcher: WFSWatcher = <WFSWatcher>fs.watch(
         wdir,
         {
            encoding: "utf-8",
            persistent: true,
            recursive: process.platform === "win32"
         },
      );

      // Extend watcher object with necessary information
      watcher.wdir = wdir;
      watcher.directory = dir;
      watcher.fileWatcher = this;

      // Assign event listeners
      this.#assignWatcherEventHandlers(watcher);

      // Store watcher to #watchedDirs
      this.#watchedDirs[dir].watcher = watcher;

   }

   /**
    * Event handlers for the given watcher
    * @param watcher Watcher to be assigned with event handlers
    */
   #assignWatcherEventHandlers(watcher: WFSWatcher) {

      watcher.on(
         "change", function(this: WFSWatcher, event: string, fd: string) {
            if (fd === null) return;
            // diag.info(this.constructor.name, "fileChanged", 0 ,`Filesystem event: '${event}' on file '${fd}' ('${path.join(watcher.directory, fd)}')`);
            this.fileWatcher.#fileChanged(this, event, path.join(watcher.wdir, fd));
         }
      );

      watcher.on(
         "error", function(this: WFSWatcher, error: Error) {

            // Workaround: errors occurs on deletion of empty folders created before watcher is started
            if (!fs.existsSync(watcher.wdir)) {
               this.fileWatcher.#aggregate(this, "unlink", this.directory, FileType.Directory, undefined);
               return;
            }

            // Otherwise, real error occured
            diag.warn(this.constructor.name, "assignWatcherEventHanfdlers", `File watcher '${watcher.directory}' error: '${error.message}'`);
         }
      );

   }

   /**
    * Handles change events fired by file system watchers
    * Normalizes events thrown by different OS
    * Event processing is queued to end of the JS task queue
    * @param watcher WFSWatcher which fired the event
    * @param event Event fired
    * @param fileName Name of the file or directory on which the change occured
    */
   #fileChanged(watcher: WFSWatcher, event: string, fileName: string): void {

      // check if change relates to the watcher monitored folder
      if (dirname(fileName) !== watcher.directory) return;

      // check if the file / directory still exist and if not, aggreate the "unlink" event and unwatch it if possible
      if (!fs.existsSync(fileName)) {
         this.#aggregate(watcher, "unlink", fileName, FileType.Unknown, undefined);
         return;
      }

      const stats = fs.statSync(fileName);
      const type: FileType = stats.isDirectory() ? FileType.Directory : stats.isFile() ? FileType.File : FileType.Unknown;

      // only files and directories are handled (not devices, links or other file types)
      if (type === FileType.Unknown) return;

      // normalize event names
      if (event === "rename") event = "create";

      if (event === "create") {

         // if a directory was created, add it to watch list and assign same listeners as parent folder
         if (type === FileType.Directory) {

            if (!Object.prototype.hasOwnProperty.call(this.#watchedDirs, fileName)) {
               this.#watchedDirs[fileName] = {
                  listeners: this.#watchedDirs[watcher.directory].listeners.slice()
               };
            }
            this.#watchDir(fileName);

         }

         if (type === FileType.File) {

            if (!Object.prototype.hasOwnProperty.call(this.#watchedFiles, fileName)) {
               this.#watchedFiles[fileName] = {
                  listeners: this.#watchedDirs[watcher.directory].listeners.slice()
               };
            }

         }

      }

      // aggregate the event
      this.#aggregate(watcher, <any>event, fileName, type, stats);

      // as files or dirs could be created inside of the dir before the dir started to be watched, check them and eventually fire appropriate events
      if (event === "create" && type === FileType.Directory) this.#checkNewFilesOnNewDir(watcher, event, fileName);

   }

   /**
    * As files or dirs could be created before the dir started to be watched, check it and eventually fire appropriate events
    * @param watcher WFSWatcher which fired the event
    * @param event Event fired
    * @param fileName Name of the file or directory on which the change occured
    */
   #checkNewFilesOnNewDir(watcher: WFSWatcher, event: string, fileName: string): void {

      const rd = fs.readdirSync(fileName);

      for (const f of rd) {
         const fn = path.resolve(fileName, f);
         this.#fileChanged(watcher, "rename", fn);
      }

   }

   /**
    * Aggregates same events on the same file or directory for defined time to minimize number of events thrown per file
    * @param eventName Name of the event occured
    * @param fileName Name of the file on which the event occured
    * @param fileType Type of the file
    */
   #aggregate(watcher: WFSWatcher, eventName: "create" | "unlink" | "change", fileName: string, fileType: FileType, stats: fs.Stats | undefined): void {

      // setup info for event aggreation
      const eventInfo: FileEventInfo = {
         timestamp: Date.now(),
         eventName,
         fileName,
         fileType,
         stats
      };

      // check if event on the same file occured already and discard it if so
      if (this.#isAggregated(eventInfo)) return;

      diag.info(this.constructor.name, "aggregate", 4, `New event: '${eventInfo.eventName}', '${eventInfo.fileName}', aggregated '${this.#aggregated.length}'`);

      // otherwise store it to aggreaged event list
      this.#aggregated.push(eventInfo);

      // and if it is the first event after all previous events were fire
      // schedule firing of the final event after defined time
      if (!this.#waitingNextEvents) {
         this.#waitingNextEvents = true;
         setTimeout(() => { this.#fireEventsAsync(watcher); }, this.#FIRE_EVENT_DELAY);
      }

   }

   /**
    * Checks if the event on the same file occured peviously within the defined time frame
    * @param event Event descriptor
    */
   #isAggregated(event: FileEventInfo): boolean {

      for (const e of this.#aggregated) {

         if (
            e.eventName === event.eventName &&
            e.fileName === event.fileName &&
            e.fileType === event.fileType &&
            event.timestamp - e.timestamp < this.#EVENT_AGGREGATION_TIME
         ) return true;

      }

      return false;

   }

   /**
    * Fires aggreated events
    */
   async #fireEventsAsync(watcher: WFSWatcher): Promise<void> {

      diag.info(this.constructor.name, "fireEvents", 3, "Firing events...");

      const currentTime = Date.now();
      const groupByListener: GroupByListener[] = [];

      while (this.#aggregated.length > 0) {

         // Work only on events occured before current time
         if (this.#aggregated[0].timestamp >= currentTime) break;

         // fiFO event from aggragated list
         const e = this.#aggregated.shift();
         if (!e) continue;

         // if file is in forced event list, remove it. if current stats are the same, don't fire the event
         if (Object.prototype.hasOwnProperty.call(this.#forcedEventFileInfo, e.fileName)) {
            const forcedStats = this.#forcedEventFileInfo[e.fileName];
            delete this.#forcedEventFileInfo[e.fileName];

            if (
               e.stats &&
               forcedStats.mtime.getTime() === e.stats.mtime.getTime() &&
               forcedStats.ctime.getTime() === e.stats.ctime.getTime()
            ) {
               diag.info(this.constructor.name, "fireEvents", 4, `Ignoring previously fired forced events for the file '${e.fileName}'.`);
               continue;
            }

         }

         // check if there is file or directory being watched and add its listener to grouped list
         // also, if new file is created fire the event
         if (e.fileType === FileType.File && (Object.prototype.hasOwnProperty.call(this.#watchedFiles, e.fileName) || e.eventName === "create")) {
            if (e.eventName === "create") {
               this.#groupByListener(this.#watchedDirs[watcher.directory].listeners, e, groupByListener);
            } else {
               this.#groupByListener(this.#watchedFiles[e.fileName].listeners, e, groupByListener);
            }
         }

         if (e.fileType !== FileType.File) {
            for (const d in this.#watchedDirs) {
               if (e.fileName.startsWith(d)) {
                  this.#groupByListener(this.#watchedDirs[d].listeners, e, groupByListener);
                  break;
               }
            }
         }

      }

      // fire grouped events
      let statListeners = 0;
      let statEvents = 0;
      for (const gpl of groupByListener) {
         statListeners++;
         statEvents += gpl.events.length;
         await gpl.listener(gpl.events);
      }

      diag.info(this.constructor.name, "fireEvents", 3, `Firing of '${statEvents}' event/s for '${statListeners}' listener/s done.`);

      // if there are still aggregated events in the queue, schedule firing them out
      if (this.#aggregated.length > 0) {
         setTimeout(() => { this.#fireEventsAsync(watcher); }, this.#FIRE_EVENT_DELAY);
      } else {
         this.#waitingNextEvents = false;
      }

   }

   /**
    * Groups events occured by listeners assigned to files or directories on which event occured
    * @param listeners List of listeners assigned to signle file or directory
    * @param eventInfo Event descriptor (info about the event occured)
    * @param groupByListener List of listeners to be extended with events to be passed to them
    */
   #groupByListener(listeners: FileEventListener[], eventInfo: FileEventInfo, groupByListener: GroupByListener[]) {

      for (const l of listeners) {

         let found = false;

         for (const gpl of groupByListener) {
            if (gpl.listener === l) {
               found = true;
               gpl.events.push(eventInfo);
               break;
            }
         }

         if (!found) {
            groupByListener.push({ listener: l, events: [ eventInfo ] });
         }

      }

   }

}