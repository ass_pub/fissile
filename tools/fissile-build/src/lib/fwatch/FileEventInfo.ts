/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2021 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { Stats } from "fs";
import { FileType } from "./FileType";

/**
 * File event information
 */
export interface FileEventInfo {
   /** Date.now() on the event occurence processing begin time */
   timestamp: number;
   /** create, change, unlink */
   eventName: "create" | "change" | "unlink";
   /** File name caused the event */
   fileName: string;
   /** Type of the file caused the event - directory, file, unknown - unknown in case of unlink or other than defined file types */
   fileType: FileType;
   /** File stats a soon as possible after event occured - undefined when unlink */
   stats: Stats | undefined;
}
