/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { EventListener } from "./EventListener";

/**
 * Provides functionality for registering and unregistering event listeners and dispatching events
 */
export class EventDispatcher<TEvent> {

   /**
    * List of registered listeners
    */
   private __listeners: EventListener<TEvent>[];

   constructor() {
      this.__listeners = [];
   }

   /**
    * Adds a listener to particular event
    * @param listener Listener to be called when event occurs
    */
   public addListener(listener: EventListener<TEvent>): void {
      if (this.__listeners.indexOf(listener) !== -1) return;
      this.__listeners.push(listener);
   }

   /**
    * Removes registered event listener
    * @param listener Removes listener from list of registered listeners
    */
   public removeListener(listener: EventListener<TEvent>): void {
      const index = this.__listeners.indexOf(listener);
      if (index === -1) return;
      this.__listeners.splice(index, 1);
   }

   /**
    * Removes all registered listeners
    */
   public removeAllListeners(): void {
      this.__listeners.length = 0;
   }

   /**
    * Fires event (calls all listeners in order they've registered)
    * @param sender Event sender (usually this)
    * @param event Event data
    */
   public dispatch(sender: any, event: TEvent): void {
      for (const listener of this.__listeners) {
         if (listener(sender, event)) return;
      }
   }

}