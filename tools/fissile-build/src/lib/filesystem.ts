/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import * as diag from "./diag/index.js";

import { resolve } from "path";
import { promises as fs } from "fs";

import { statSync, readdirSync } from "fs";

/**
 * Deletes all children files and folders from the specified path
 * @param dir Target directory to be cleaned
 */
export async function rmDirContent(dir: string) {

   let dd;
   // eslint-disable-next-line no-empty
   try { dd = await fs.stat(dir); } catch(e) {}

   if (!dd || !dd.isDirectory()) {
      diag.error("lib-filesystem", "rmDirContent", `Can՚t remove files from the directory specified '${dir}'. `);
      return;
   }


   const files = await fs.readdir(dir);

   try {

      await Promise.all(
         files.map(f => {
            const file = resolve(dir, f);
            diag.info("lib-filesystem", "rmDirContent", 0, `Deleting '${file}'`);
            return fs.rm(file, { recursive: true, force: true });
         })
      );

   } catch(e) {
      diag.error("lib-filesystem", "rmDirContent", "Failed to cleanup target folder.", e);
   }

}


/**
 * Creates a directory if it not exists.
 * If the file exists on the same path as the directory it is deleted
 * @param dir Directory to be created
 */
export async function mkdir(dir: string) {

   diag.info("lib-filesystem", "mkdir", 0, `Creating directory '${dir}'`);

   let dd;
   // eslint-disable-next-line no-empty
   try { dd = await fs.stat(dir); } catch(e) {}

   if (!dd) {
      await fs.mkdir(dir);
   } else if (!dd.isDirectory()) {
      await fs.rm(dir, { recursive: true, force: true });
      await fs.mkdir(dir);
   }

}

/**
 * Copies files recursively from one folder to another including sub-directories
 * Root source directory is not copied, just files and subdirs inside of it
 * Target directory is created on the specified path, if it does not exist (no
 * recursion is used so path must exist)
 * @param srcDir Source folder to be copied from
 * @param dstDir Target directory files to be copied to
 */
export async function copyDir(srcDir: string, dstDir: string) {

   diag.info("lib-filesystem", "copyDir", 0, `Copying files from '${srcDir}' to '${dstDir}'`);

   await mkdir(dstDir);

   const files = await fs.readdir(srcDir);

   await Promise.all(

      files.map(

         async file => {
            const srcFile = resolve(srcDir, file);
            const dstFile = resolve(dstDir, file);

            const srcStat = await fs.stat(srcFile);

            if (srcStat.isDirectory()) {

               mkdir(dstDir);
               return copyDir(srcFile, dstFile);

            } else {
               return fs.copyFile(srcFile, dstFile);
            }

         }

      )

   );

}


/**
 * Recursively lists files and directories under the passed directory.
 * Special files, such as devices, symliks or hardlinks are ommited.
 * @param dir Absolute path to directory to be listed recursively
 * @returns List of files and directories
 */
export function lsRecursive(dir: string): string[] {

   const filesDirs: string[] = [];

   function walkDir(dir: string): void {

      const files = readdirSync(dir);

      for (const file of files) {

         const filename = resolve(dir, file);
         const stat = statSync(filename);

         if (stat.isFile()) {
            filesDirs.push(filename);
         }

         if (stat.isDirectory()) {
            // filesDirs.push(filename);
            walkDir(filename);
         }

      }

   }

   walkDir(dir);

   return filesDirs;

}