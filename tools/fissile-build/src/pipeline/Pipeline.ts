/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import * as fs from "fs";
import { dirname, relative, sep } from "path";

import { FileEventInfo, FileType, FileWatcher } from "../lib/fwatch/index.js";

import FissileBuildContext from "../FissileBuildContext";
import { FileInfo, Plugin } from "../plugins/Plugin";
import { createHash } from "crypto";

interface FileChangeEventQueueRecord {
   eventInfo: FileEventInfo[];
   filesInfo: FileInfo[];
}


export class Pipeline {

   #context: FissileBuildContext;
   #name: string;
   #fileProviderPlugin: Plugin<any>;
   #stopOnError: boolean;
   #plugins: Plugin<any>[];

   #fileNames: string[];

   #fileWatcher: FileWatcher;
   #fileChangeEventQueue: FileChangeEventQueueRecord[];
   #processingFileChangeEventQueue: boolean;


   public get stopOnError(): boolean { return this.#stopOnError; }


   constructor(
      context: FissileBuildContext,
      name: string,
      fileProviderPlugin: Plugin<any>,
      stopOnError: boolean,
      pluginIds: Plugin<any>[]
   ) {

      this.#context = context;
      this.#name = name;
      this.#fileProviderPlugin = fileProviderPlugin;
      this.#stopOnError = stopOnError;
      this.#plugins = pluginIds;

      this.#fileNames = [];

      this.#fileWatcher = new FileWatcher();
      this.#fileChangeEventQueue = [];
      this.#processingFileChangeEventQueue = false;

   }


   public async runAsync(watch: boolean): Promise<boolean> {

      this.#context.diag.info(this.constructor.name, "runAsync", 0, `Starting pipeline '${this.#name}'`);

      if (this.#plugins.length === 0) {
         this.#context.diag.info(this.constructor.name, "runAsync", 0, "No plugins configured. Nothing to do.");
         return false;
      }

      this.#context.diag.info(this.constructor.name, "runAsync", 0, `   Obtaining list of files and directories to ${watch ? "watch" : "process"} from '${this.#fileProviderPlugin.id}'`);

      const ls = this.#fileProviderPlugin.ls();

      if (!ls) {
         this.#context.diag.error(this.constructor.name, "runAsync", "   Plugin can't be used as the first pipeline plugin as it does not provide list of files/directories to be processed by the pipeline.");
         this.#context.diag.error(this.constructor.name, "runAsync", "Pipeline dind't finished.");
         return false;
      }

      this.#fileNames = ls;

      const fileInfo: FileInfo[] | false = await this.#loadFiles();
      if (fileInfo === false) return false;


      if (watch) {

         this.#context.diag.info(this.constructor.name, "runAsync", 0, `   Processing '${this.#fileNames.length}' files.`, this.#fileNames);
         await this.#runPluginsAsync(fileInfo);

         this.#watch(fileInfo);

         return true;

      } else {

         this.#context.diag.info(this.constructor.name, "runAsync", 0, `   Processing '${this.#fileNames.length}' files.`, this.#fileNames);

         return await this.#runPluginsAsync(fileInfo);

      }

   }


   #extractDirs(filesInfo: FileInfo[]): string[] {

      let dirs: string[] = [];

      filesInfo.forEach(fi => {

         const d = dirname(fi.srcFileName);

         let add = true;

         dirs.forEach(addedDir => {
            if (d.startsWith(addedDir)) add = false;
         });

         if (dirs.indexOf(d) !== -1) add = false;

         if (add) dirs.push(d);
      });

      dirs = dirs.sort();

      let i = 0;
      while (i < dirs.length) {

         let j = i + 1;
         while (j < dirs.length) {

            let remove = false;
            for (let k = 0; k <= i; k++) {
               if (dirs[j].startsWith(dirs[k])) remove = true;
            }

            if (remove) {
               dirs.splice(j, 1);
            } else {
               j++;
            }

         }

         i++;
      }

      return dirs;

   }


   async #loadFiles(): Promise<FileInfo[] | false> {

      try {

         this.#context.diag.info(this.constructor.name, "#loadFiles", 0, "   Loading files");

         const promises = this.#fileNames.map(file => fs.promises.readFile(file));
         const files = await Promise.all(promises);

         let size = 0;
         const contents = files.map(
            (file, index) => {

               size += file.length;

               const fi: FileInfo = {
                  srcFileName: this.#fileNames[index],
                  fileContent: file,
               };

               fi.srcFileHash = createHash("md5").update(file).digest("hex");

               return fi;
            }
         );

         this.#context.diag.info(this.constructor.name, "#loadFiles", 0, `   ${contents.length} files (${size} bytes) loaded`);

         return contents;

      } catch(e) {

         this.#context.diag.error(this.constructor.name, "#loadFiles", `   Error occured while loading files ${e}`);
         return false;

      }

   }


   async #runPluginsAsync(fileInfo: FileInfo[]): Promise<boolean> {

      let error = false;

      let fi: FileInfo[] = fileInfo;

      for (const plugin of this.#plugins) {

         let pluginError = false;

         this.#context.diag.info(this.constructor.name, "runPlugins", 0, `   Processing ${fi.length} files using '${plugin.id}' plugin`);

         const result = await plugin.processFiles(fi);

         for (const diag of result.diag) {

            const msg = `${relative(process.cwd(), diag.fileName)}:${diag.line}:${diag.column} - ${diag.type}(${diag.code}) ${diag.message}`;

            switch(diag.type) {

               case "warning":
                  this.#context.diag.warn(this.constructor.name, "runPlugins", `      ${plugin.id}: ${msg}`);
                  break;

               case "error":
                  pluginError = true;
                  this.#context.diag.error(this.constructor.name, "runPlugins", `      ${plugin.id}: ${msg}`);
                  break;

            }

         }

         if (pluginError && this.#stopOnError) {
            this.#context.diag.error(this.constructor.name, "runPlugins", `   Plugin ${plugin.id} finished with errors. Pipeline processing stopped.`);
            error = true;
            break;
         }

         if (pluginError) {
            this.#context.diag.warn(this.constructor.name, "runPlugins", `   Plugin ${plugin.id} finished with errors.`);
            error = true;
         }

         fi = result.files;

         if (fi.length === 0) {
            this.#context.diag.info(this.constructor.name, "runPlugins", 0, `   Plugin '${plugin.id}' returned 0 files to process with next plugin in pipeline. Stopping pipeline processing.`);
            break;
         }

      }

      return !error;

   }


   #watch(filesInfo: FileInfo[]): void {


      const dirs = this.#extractDirs(filesInfo);
      const watch = [ ...dirs, ...this.#fileNames ];

      this.#context.diag.info(this.constructor.name, "runAsync", 0, "   Configuring file watcher to watch: ", watch);

      this.#fileWatcher.watch(
         watch,
         async (eventInfo: FileEventInfo[]) => {
            this.#fileChangeEventQueue.push({ eventInfo, filesInfo });
            await this.#processFileChangeEventQueue();
         },
         true

      );

   }

   async #processFileChangeEventQueue(): Promise<void> {

      if (this.#processingFileChangeEventQueue) return;
      this.#processingFileChangeEventQueue = true;

      while (this.#fileChangeEventQueue.length > 0) {
         const ei = this.#fileChangeEventQueue.shift();
         if (!ei) continue;
         await this.#onFileEvent(ei.eventInfo, ei.filesInfo);
      }

      this.#processingFileChangeEventQueue = false;

   }

   async #onFileEvent(eventInfo: FileEventInfo[], watchedFilesInfo: FileInfo[]): Promise<void> {

      const toProcess: FileInfo[] = [];

      eventInfo.forEach(

         ei => {
            switch(ei.eventName) {

               case "create":
                  if (ei.fileType === FileType.File) this.#onFilesCreated(ei, watchedFilesInfo, toProcess);
                  break;

               case "change":
                  if (ei.fileType === FileType.File) this.#onFilesUpdated(ei, watchedFilesInfo, toProcess);
                  break;

               case "unlink":
                  if (ei.fileType === FileType.File) this.#onFilesDeleted(ei, watchedFilesInfo);
                  if (ei.fileType === FileType.Directory) this.#onDirsDeleted(ei, watchedFilesInfo);
                  break;


            }

         }
      );

      if (toProcess.length === 0) return;

      this.#context.diag.info(this.constructor.name, "onFileEvent", 0, `File system changes detected, pipeline '${this.#name}' is processing changes`);

      await this.#runPluginsAsync(toProcess);

      this.#context.diag.info(this.constructor.name, "onFileEvent", 0, "File system changes processing done");

   }


   #onFilesCreated(eventInfo: FileEventInfo, watchedFilesInfo: FileInfo[], toProcess: FileInfo[]): void {

      const content = fs.readFileSync(eventInfo.fileName);

      const fi: FileInfo = {
         srcFileName: eventInfo.fileName,
         fileContent: content,
         srcFileHash: createHash("md5").update(content).digest("hex")
      };

      watchedFilesInfo.push(fi);
      toProcess.push(fi);


   }


   #onFilesUpdated(eventInfo: FileEventInfo, watchedFilesInfo: FileInfo[], toProcess: FileInfo[]): void {

      const watchedFile = watchedFilesInfo.find(wfi => wfi.srcFileName === eventInfo.fileName);

      if (!watchedFile) {
         this.#context.diag.warn(this.constructor.name, "onFilesUpdated", `File watcher reported a changed file '${eventInfo.fileName}', but the file not found in list of monitored files. Processing will be skipped.`);
         return;
      }

      const content = fs.readFileSync(eventInfo.fileName);
      const hash = createHash("md5").update(content).digest("hex");

      if (watchedFile.srcFileHash === hash) {
         this.#context.diag.info(this.constructor.name, "onFilesUpdated", 0, `   File '${watchedFile.srcFileName}' content not changed.`);
         return;
      }

      watchedFile.fileContent = content;
      watchedFile.srcFileHash = hash;

      toProcess.push(watchedFile);

   }


   #onFilesDeleted(eventInfo: FileEventInfo, watchedFilesInfo: FileInfo[]): void {

      const watchedFileIndex = watchedFilesInfo.findIndex(wfi => wfi.srcFileName === eventInfo.fileName);
      if (watchedFileIndex !== -1) watchedFilesInfo.splice(watchedFileIndex, 1);

   }


   #onDirsDeleted(eventInfo: FileEventInfo, watchedFilesInfo: FileInfo[]): void {

      for (let i = watchedFilesInfo.length - 1; i >=0; i--) {
         if (watchedFilesInfo[i].srcFileName.startsWith(eventInfo.fileName + sep)) {
            watchedFilesInfo.splice(i, 1);
         }
      }

   }

}