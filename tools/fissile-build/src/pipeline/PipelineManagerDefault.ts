/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { fileURLToPath } from "url";
import { promises as fs } from "fs";
import { dirname, resolve } from "path";
import { extname } from "path";

import FissileBuildContext from "../FissileBuildContext";

import { FissileBuildPipelineContext, Plugin, PluginCtor, PluginOptions } from "../plugins/Plugin";
import { Pipeline } from "./Pipeline.js";
import { KeyValue } from "../lib/@types/KeyValue";
import { PipelineManager } from "./PipelineManager";


const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);


export class PipelineManagerDefault implements PipelineManager {

   #context: FissileBuildContext;
   #plugins: Plugin<any>[];
   #pipelines: Pipeline[];

   constructor (context: FissileBuildContext) {

      this.#context = context;
      this.#plugins = [];
      this.#pipelines = [];

   }


   public async initAsync(): Promise<void> {

      const loadedPlugins = await this.#loadPluginsAsync();

      this.#context.diag.info(this.constructor.name, "initAsync", 0, `${this.#context.fbConfig.plugins.length} Available external plugins: [ ${this.#context.fbConfig.plugins.join(", ")} ]`);

      const pluginCtors = [ ...loadedPlugins, ...this.#context.fbConfig.plugins ];

      const usedPlugins = this.#getUsedPlugins();

      this.#configurePlugins(pluginCtors, usedPlugins);
      this.#configurePipelines();

   }


   public async runAsync(watch: boolean): Promise<boolean> {

      let result = true;

      for (const pipeline of this.#pipelines) {
         result = result && await pipeline.runAsync(watch);
      }

      if (watch) {
         this.#context.diag.info(this.constructor.name, "runAsync", 0, "Initial processing finished. Watching for file changes");
      }

      return result;

   }


   async #loadPluginsAsync(): Promise<PluginCtor<any>[]> {

      this.#context.diag.info(this.constructor.name, "loadPluginsAsync", 0, "Loading available internal plugins");

      const pluginCtors = await this.#searchAndLoadLocalPluginsAsync();

      this.#context.diag.info(this.constructor.name, "loadPluginsAsync", 0, `   ${pluginCtors.length} Plugins loaded: [ ${pluginCtors.map(pc => pc.name).join(", ")} ]`);

      this.#context.diag.info(this.constructor.name, "loadPluginsAsync", 0, "Plugins loaded successfully");

      return pluginCtors;

   }


   #getUsedPlugins(): string[] {

      const pipelinesPlugins: string[] = [];


      for (const pluginId in this.#context.fbConfig.pipelines.common) {

         if (pipelinesPlugins.indexOf(this.#context.fbConfig.pipelines.common[pluginId].fileProvider) === -1) {
            pipelinesPlugins.push(this.#context.fbConfig.pipelines.common[pluginId].fileProvider);
         }

         for (const k of this.#context.fbConfig.pipelines.common[pluginId].plugins) {
            if (pipelinesPlugins.indexOf(k) === -1) pipelinesPlugins.push(k);
         }
      }


      for (const pluginId in this.#context.fbConfig.pipelines[this.#context.fbConfig.env]) {

         if (pipelinesPlugins.indexOf(this.#context.fbConfig.pipelines[this.#context.fbConfig.env][pluginId].fileProvider) === -1) {
            pipelinesPlugins.push(this.#context.fbConfig.pipelines[this.#context.fbConfig.env][pluginId].fileProvider);
         }

         for (const k of this.#context.fbConfig.pipelines[this.#context.fbConfig.env][pluginId].plugins) {
            if (pipelinesPlugins.indexOf(k) === -1) pipelinesPlugins.push(k);
         }
      }


      this.#context.diag.info(this.constructor.name, "getUsedPlugins", 0, `${pipelinesPlugins.length} Plugins will be used: [ ${pipelinesPlugins.join(", ")} ]`);

      return pipelinesPlugins;


   }


   #configurePlugins(pluginCtors: PluginCtor<any>[], usedPlugins: string[]): void {

      const plugins: Plugin<any>[] = [];

      this.#context.diag.info(this.constructor.name, "configurePlugins", 0, "Configuring plugins");

      for (const pluginCtor of pluginCtors) {

         const plugin = this.#instantiatePlugin(pluginCtor);
         if (!plugin) continue;

         if (usedPlugins.indexOf(plugin.id) === -1) {
            this.#context.diag.info(this.constructor.name, "configurePlugins", 0, `   Plugin '${plugin.displayName}' (id: '${plugin.id}') not in use.`);
            continue;
         } else {
            this.#configurePlugin(plugin);
         }

         plugins.push(plugin);

      }

      this.#context.diag.info(this.constructor.name, "configurePlugins", 0, "Plugins configured successfully");

      this.#plugins = plugins;

   }


   async #searchAndLoadLocalPluginsAsync(): Promise<PluginCtor<any>[]> {

      const pluginCtors: PluginCtor<any>[] = [];

      const pluginsDir = resolve(__dirname, "..", "plugins");

      const pluginsDirs = await fs.readdir(pluginsDir);

      for (const pluginDir of pluginsDirs) {

         const aPluginDir = resolve(pluginsDir, pluginDir);

         const stat = await fs.stat(aPluginDir);
         if (!stat.isDirectory()) continue;

         const pluginFiles = await fs.readdir(aPluginDir);

         for (const pluginFile of pluginFiles) {

            const aPluginFile = resolve(aPluginDir, pluginFile);

            const stat = await fs.stat(aPluginFile);
            if (!stat.isFile()) continue;

            const extName = extname(aPluginFile);
            if (extName !== ".js" && extName !== ".mjs") continue;

            const pluginCode = (await fs.readFile(aPluginFile)).toString();

            if (pluginCode.indexOf("export default class") === -1) continue;

            const rPluginFile = `../plugins/${pluginDir}/${pluginFile}`;

            const pluginCtor = await this.#loadAndInstantiatePluginAsync(rPluginFile);

            if (pluginCtor) pluginCtors.push(pluginCtor);

         }

      }

      return pluginCtors;

   }


   async #loadAndInstantiatePluginAsync(pluginPath: string): Promise<PluginCtor<any> | undefined> {

      try {

         const pluginCtor: PluginCtor<any> = (await import(pluginPath)).default;
         if (typeof pluginCtor !== "function") return undefined;
         return pluginCtor;

      } catch(e) {

         this.#context.diag.warn(this.constructor.name, "loadAndInstantiatePluginAsync", `Failed to load plugin '${pluginPath}': ${e}`);
         return undefined;

      }

   }


   #instantiatePlugin(pluginCtor: PluginCtor<any>): Plugin<any> | undefined {

      try {

         this.#context.diag.info(this.constructor.name, "instantiatePlugin", 0, `   Instantiating plugin '${pluginCtor.name}'`);

         const context: FissileBuildPipelineContext = {
            fbConfig: this.#context.fbConfig,
            diag: this.#context.diag,
            pipelineManager: this,
            pluginConfigPath: ""
         };

         const plugin = new pluginCtor(context);

         if (
            plugin.displayName === undefined ||
            plugin.copyrights === undefined ||
            plugin.id === undefined ||
            plugin.init === undefined
         ) return undefined;

         this.#context.diag.info(this.constructor.name, "instantiatePlugin", 0, `      '${plugin.displayName}' ('${plugin.id}') version '${plugin.version}'`);
         plugin.copyrights.map(copyright => this.#context.diag.info(this.constructor.name, "instantiatePlugin", 0, `      '${copyright}'`));

         this.#context.diag.info(this.constructor.name, "instantiatePlugin", 0, `   Plugin '${pluginCtor.name}' instantiated.`);

         return plugin;

      } catch (e) {

         this.#context.diag.warn(this.constructor.name, "instantiatePlugin", `   Failed to instantiate plugin '${pluginCtor.name}': ${e}`);
         return undefined;

      }

   }


   #configurePlugin(plugin: Plugin<any>): void {

      this.#context.diag.info(this.constructor.name, "configurePlugin", 0, `   Configuring plugin '${plugin.constructor.name}'`);

      plugin.context.pluginConfigPath = this.#getConfigPath(plugin);
      plugin.init();

      this.#context.diag.info(this.constructor.name, "configurePlugin", 0, `   Plugin '${plugin.constructor.name}' configured successfully`);

   }


   #getConfigPath(plugin: Plugin<any>): string {

      const configPath =
         this.#context.fbConfig.pluginConfig[this.#context.fbConfig.env][plugin.id] ||
         this.#context.fbConfig.pluginConfig.common[plugin.id];

      if (configPath === undefined && plugin.configurable) {
         this.#context.diag.warn(this.constructor.name, "getConfigPath", `      No config file set for '${this.#context.fbConfig.env}' environment. Default configuration will be used.`);
      }

      return configPath;

   }


   #configurePipelines() {

      this.#context.diag.info(this.constructor.name, "configurePipelines", 0, "Configuring pipelines");

      this.#configureEnvPipelines(this.#context.fbConfig.pipelines.common);
      this.#configureEnvPipelines(this.#context.fbConfig.pipelines[this.#context.fbConfig.env]);

      this.#context.diag.info(this.constructor.name, "configurePipelines", 0, "Pipelines configuration finished succesfully");

   }


   #configureEnvPipelines(envPipelineConfig: KeyValue<PluginOptions>) {

      for (const pipelineName in envPipelineConfig) {

         const pipelinePlugins: Plugin<any>[] = [];

         this.#context.diag.info(this.constructor.name, "configurePipelines", 0, `   Configuring 'common' pipeline '${pipelineName}'`);

         for (const pluginId of envPipelineConfig[pipelineName].plugins) {

            this.#context.diag.info(this.constructor.name, "configurePipelines", 0, `      Adding plugin '${pluginId}'`);
            const plugin = this.#plugins.find(p => p.id === pluginId);

            if (!plugin) {
               this.#context.diag.error(this.constructor.name, "configurePipelines", `      Plugin '${pluginId}' not loaded!`);
               process.exit(10);
            }

            pipelinePlugins.push(plugin);

         }

         const fileProviderPlugin = this.#plugins.find(p => p.id === envPipelineConfig[pipelineName].fileProvider);

         if (!fileProviderPlugin) {
            this.#context.diag.error(this.constructor.name, "configurePipelines", `      Plugin '${envPipelineConfig[pipelineName].fileProvider}' not loaded!`);
            process.exit(11);
         }

         this.#pipelines.push(
            new Pipeline(
               this.#context,
               pipelineName,
               fileProviderPlugin,
               envPipelineConfig[pipelineName].stopOnError,
               pipelinePlugins
            )
         );

      }


   }


}