/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { ESLint, Linter } from "eslint";
import { statSync, readFileSync } from "fs";
import { extname, resolve } from "path";
import { lsRecursive } from "../../lib/filesystem.js";
import { stripComments } from "../../lib/json/index.js";
import { DiagEntry, FileInfo, Plugin, FissileBuildPipelineContext, ProcessedInfo } from "../Plugin.js";

const defaultConfig: Linter.Config<Linter.RulesRecord> = {

   root: true,

   env: {
      browser: true
   },
   extends: [
      "eslint:recommended",
      "plugin:@typescript-eslint/eslint-recommended",
      "plugin:@typescript-eslint/recommended"
   ],
   parser: "@typescript-eslint/parser",
   parserOptions: {
      "project": ["./config/tsconfig.dev.json"]
   },
   plugins: [
      "@typescript-eslint"
   ],
   rules: {

      indent: [
         "error", 3,
         { "SwitchCase": 1 }
      ],
      quotes: ["error", "double"],
      semi: ["error", "always"],

      "@typescript-eslint/no-explicit-any": "off"

   }

};


export default class EsLint extends Plugin<Linter.Config<Linter.RulesRecord>> {


   protected _displayName: string;
   protected _copyrights: string[];
   protected _version: string;
   protected _id: string;

   protected _configurable = true;

   #esLint?: ESLint;


   constructor(context: FissileBuildPipelineContext) {

      super(context);

      this._displayName =  "EsLint";
      this._version = "1.0.0";
      this._id = "eslint";
      this._copyrights = [
         "Copyright (c)2021 Atom Software Studios, released under MIT license",
         "Copyright OpenJS Foundation and other contributors, <www.openjsf.org>, released under MIT license"
      ];
   }


   public override init(): void {

      this.#configure();

      this.#esLint = new ESLint({
         baseConfig: this._options,
         useEslintrc: false
      });

   }


   public override ls(): string[] | undefined {

      this._context.diag.info(this.constructor.name, "ls", 0, "   Listing files and directories to be processed");

      const root = resolve(this._context.fbConfig.srcDir);

      return lsRecursive(root);

   }


   public override async processFiles(files: FileInfo[]): Promise<ProcessedInfo> {

      if (!this.#esLint) throw "EsLint plugin not initialized!";

      const diag: DiagEntry[] = [];

      try {
         const results = await Promise.all(
            files.map(
               f => {
                  const ext = extname(f.srcFileName);
                  if (ext !== ".ts" && ext !== ".js" && ext !== ".mjs") {
                     this._context.diag.info(this.constructor.name, "processFiles", 0, `   Skipping non TypeScript or JavaScript file '${f.srcFileName}'`);
                     return null;
                  }
                  return this.#esLint!.lintText(f.fileContent.toString(), { filePath: f.srcFileName });
               }
            )
         );

         results.forEach(
            lintResults => {
               if (lintResults) lintResults.forEach(
                  lintResult => {
                     lintResult.messages.forEach(
                        msg => {
                           if (msg.severity > 0) {
                              diag.push({
                                 fileName: lintResult.filePath,
                                 line: msg.line,
                                 column: msg.column,
                                 pos: -1,
                                 message: msg.message,
                                 code: msg.ruleId || "",
                                 snip: "",
                                 type: msg.severity === 1 ? "warning" : "error"
                              });
                           }
                        }
                     );
                  }
               );
            }
         );

      } catch(e) {

         this.context.diag.error(this.constructor.name, "processFiles", "   Failed to lint passed files.", e);

      }

      return {
         files,
         diag
      };

   }


   #configure(): void {

      const aConfigPath = resolve(this._context.pluginConfigPath);

      let stat;
      // eslint-disable-next-line no-empty
      try { stat = statSync(aConfigPath); } catch {}

      if (stat === undefined || !stat.isFile()) {
         this._context.diag.warn(this.constructor.name, "configure", `      Configured config file '${aConfigPath}' not found. Default configuration will be used.`);
         this._options = defaultConfig;
         return;
      }

      try {

         const configContent = readFileSync(aConfigPath);
         const configStripped = stripComments(configContent.toString());
         this._options = JSON.parse(configStripped);

      } catch (e) {
         this._context.diag.warn(this.constructor.name, "configure", `      Failed to load config file '${aConfigPath}': ${e}. Default configuration will be used.`);
         this._options = defaultConfig;
         return;
      }

   }


}