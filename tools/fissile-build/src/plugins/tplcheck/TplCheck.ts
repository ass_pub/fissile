/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { resolve } from "path";
import { DiagEntry, FileInfo, Plugin, FissileBuildPipelineContext, ProcessedInfo } from "../Plugin.js";


interface TplCheckConfigOtions {
   srcDir: string;
}


const defaultOptions: TplCheckConfigOtions = {
   srcDir: "./templates"
};


export default class TplCheck extends Plugin<TplCheckConfigOtions> {

   protected _displayName: string;
   protected _copyrights: string[];
   protected _version: string;
   protected _id: string;

   protected _configurable = true;

   constructor(context: FissileBuildPipelineContext) {

      super(context);

      this._displayName =  "TplCheck";
      this._version = "1.0.0";
      this._id = "tplcheck";
      this._copyrights = [ "Copyright (c)2021 Atom Software Studios, released under MIT license" ];

   }


   public override init(): void {

      this._options = this._loadPluginOptions(defaultOptions, this._context.pluginConfigPath);

   }


   public override ls(): string[] | undefined {

      if (!this._options || !this._options.srcDir) return undefined;
      return super.ls(resolve(this._options.srcDir));

   }


   public override processFiles(files: FileInfo[]): ProcessedInfo {

      const diag: DiagEntry[] = [];

      this._context.diag.warn(this.constructor.name, "processFiles", "      Not implemented");

      return {
         files,
         diag
      };

   }

}