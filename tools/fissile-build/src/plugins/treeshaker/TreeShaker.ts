/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { Program,ExportAllDeclaration, ImportDeclaration, ImportExpression, Node, SimpleCallExpression, ExportNamedDeclaration, ExportDefaultDeclaration, Literal } from "estree";
import { NodePath, traverse } from "estree-toolkit";
import { parseModule } from "meriyah";
import { Identifier } from "meriyah/dist/src/estree";
import { extname } from "path";
import { DiagEntry, FileInfo, Plugin, FissileBuildPipelineContext, ProcessedInfo } from "../Plugin.js";
import { Export, Import, Module } from "./Module.js";


interface TreeShakerConfigOtions {
   implement?: unknown;
}


const defaultOptions: TreeShakerConfigOtions = {
   implement: "IMPLEMENT!"
};


/**
 * 3rd party plugin resolver
 * tree shaking
 * import rewrite
 * dep map generator
 * commonjs bundler??
 */
export default class TreeShaker extends Plugin<TreeShakerConfigOtions> {

   protected _displayName: string;
   protected _copyrights: string[];
   protected _version: string;
   protected _id: string;

   protected _configurable = true;


   #modules: Module[];


   constructor(context: FissileBuildPipelineContext) {

      super(context);

      this._displayName =  "TreeShaker";
      this._version = "1.0.0";
      this._id = "treeshaker";
      this._copyrights = [ "Copyright (c)2021 Atom Software Studios, released under MIT license" ];

      this.#modules = [];

   }


   public override init(): void {

      this._options = this._loadPluginOptions(defaultOptions, this._context.pluginConfigPath);

   }


   public override processFiles(files: FileInfo[]): ProcessedInfo {

      const diag: DiagEntry[] = [];

      const changedFiles = this.#updateModules(files, diag);

      return {
         files,
         diag
      };

   }


   #updateModules(files: FileInfo[], diag: DiagEntry[]): FileInfo[] {

      const changedFiles: FileInfo[] = [];

      // Only changed JavaScript files are processed
      const jsModuleSources = files.filter(
         f =>
            f.dstFileName &&
            (extname(f.dstFileName) === ".js" || extname(f.dstFileName) === ".cjs" || extname(f.dstFileName) === ".mjs")
      );

      // For each changed JavaScript file the module object will be created / updated
      const updatedModules = this.#processFileChanges(jsModuleSources, diag);

      return changedFiles;

   }


   #processFileChanges(files: FileInfo[], diag: DiagEntry[]): Module[] {

      const updatedModules: Module[] = [];

      for (const f of files) {

         console.log(f);

         const astTree: Program = <any>parseModule(f.fileContent.toString(), { module: true, next: true, loc: true });

         const { exports, imports } = this.#analyzeModule(f, astTree, diag);

         const depMap = Buffer.from("");

         const module: Module = {

            sourceFileName: f.srcFileName,
            destinationFileName: f.dstFileName!,
            fileContent: f.fileContent,
            sourceMap: f.sourceMapfileContent,
            depMap,

            astTree,
            exports,
            imports

         };

         console.log(module.sourceFileName);
         console.log(module.destinationFileName);
         if (module.exports.length > 0) console.log("   Exports:");
         for (const exp of module.exports) {
            console.log("      Export name:        ", exp.name);
            console.log("      Is default:         ", exp.default);
            console.log("      External ref count: ", exp.externalRefCount);
            console.log("      Internal ref count: ", exp.internalRefCount);
            console.log("");
         }

         if (module.imports.length > 0) console.log("   Imports:");
         for (const imp of module.imports) {
            console.log("      Module name:        ", imp.moduleName);
            console.log("      Export name:        ", imp.exportName);
            console.log("      Local name:         ", imp.importName);
            console.log("      Is Reexport:        ", imp.isReexport);
            console.log("      Internal ref count: ", imp.internalRefCount);
            console.log("");
         }


      }


      return updatedModules;

   }


   #analyzeModule(fileInfo: FileInfo, astTree: Program, diag: DiagEntry[]): { exports: Export[], imports: Import[] } {

      const exports: Export[] = [];
      const imports: Import[] = [];

      traverse(
         astTree,

         {
            "CallExpression": (path: NodePath<SimpleCallExpression, Node>): void => {
               this.#processRequireImport(path, imports);
            },

            "ImportDeclaration": (path: NodePath<ImportDeclaration, Node>): void => {
               this.#processImportDeclaration(path, imports);
            },

            "ImportExpression": (path: NodePath<ImportExpression, Node>): void => {
               this.#processImportExpression(path, imports);
            },

            "ExportAllDeclaration": (path: NodePath<ExportAllDeclaration, Node>) => {
               this.#processExportAllDeclaration(path, exports);

            },

            "ExportDeclaration": (path: NodePath<ExportNamedDeclaration | ExportDefaultDeclaration | ExportAllDeclaration, Node>) => {
               this.#processExportDeclaration(path, exports, imports);
            },

            "ExportDefaultDeclaration": (path: NodePath<ExportDefaultDeclaration, Node>) => {
               this.#processExportDefaultDeclaration(path, exports);
            },

            "Identifier": (path: NodePath<Identifier>) => {

               console.log(`${path.node?.name}`);

               if (
                  path.parent?.type !== "ImportSpecifier" &&
                  path.parent?.type !== "ImportNamespaceSpecifier"
               ) {

                  if (path.parent?.type === "MemberExpression") {
                     //
                  }

               }
            }

         }

      );

      return {
         exports,
         imports
      };

   }


   #processRequireImport = (path: NodePath<SimpleCallExpression, Node>, imports: Import[]): void => {

      // path node must exist
      if (!path.node) return;

      // look just for CallExpression with the require identifier
      if (path.node.callee.type !== "Identifier") return;

      if (path.node.callee.name !== "require") return;

      // // exactly 1 grgument must be used within the require/import CallExpression
      if (!path.node.arguments || path.node.arguments.length !== 1) return;

      if (path.node.arguments[0].type !== "Literal" || !typeof path.node.arguments[0].value) return;

      // identify if the require is static or dynamic (in meaning of ES6)

      let dynamic = false;

      // if require call argument is anything else than string literal it is a dynamic import

      if (path.node.arguments[0].type !== "Literal" || typeof path.node.arguments[0].value !== "string") dynamic = true;

      const module = dynamic ? undefined : (<string>(<Literal>path.node.arguments[0]).value);

      let ancestor = path.parentPath;

      // also, if the require is not in the root block of the document it is definitely a dynamic import
      while(ancestor !== null) {
         if (ancestor.node?.type === "BlockStatement") {
            dynamic = true;
            break;
         }
         ancestor = ancestor.parentPath;
      }

      // push info about the discovered dependency
      console.log(module);

   };


   #processImportDeclaration(path: NodePath<ImportDeclaration, Node>, imports: Import[]): void {

      if (!path.node) return;

      if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

      path.node.specifiers.forEach(s => {

         if (s.type === "ImportDefaultSpecifier") {
            imports.push({
               astNodePath: path,
               moduleName: <string>(path.node!.source.value),
               exportName: "__default__",
               importName: s.local.name,
               isReexport: false,
               internalRefCount: 0
            });
         }

         if (s.type === "ImportNamespaceSpecifier") {
            imports.push({
               astNodePath: path,
               moduleName: <string>(path.node!.source.value),
               importName: s.local.name,
               exportName: s.local.name,
               isReexport: false,
               internalRefCount: 0
            });
         }

         if (s.type === "ImportSpecifier")  {
            imports.push({
               astNodePath: path,
               moduleName: <string>(path.node!.source.value),
               importName: s.local.name,
               exportName: s.imported.name,
               isReexport: false,
               internalRefCount: 0
            });
         }

      });

   }

   #processImportExpression(path: NodePath<ImportExpression, Node>, imports: Import[]): void {

      if (!path.node) return;

      if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

      console.log("ImportExpression: ", path);

   }

   #processExportAllDeclaration(path: NodePath<ExportAllDeclaration, Node>, exports: Export[]): void {

      if (!path.node) return;
      if (!path.node.source) return;
      if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

      console.log("ExportAllDeclaration: ", path);

   }

   #processExportDeclaration(path: NodePath<ExportDefaultDeclaration | ExportNamedDeclaration | ExportAllDeclaration, Node>, exports: Export[], imports: Import[]): void {

      if (!path.node) return;

      if (path.node.type === "ExportNamedDeclaration") {

         if (path.node.declaration) {

            switch (path.node.declaration.type) {

               case "VariableDeclaration":

                  path.node.declaration.declarations.forEach(d => {
                     if (d.type === "VariableDeclarator" && d.id && d.id.type === "Identifier") {
                        exports.push({
                           astNodePath: path,
                           default: false,
                           name: d.id.name,
                           internalRefCount: 0,
                           externalRefCount: 0
                        });
                     } else {
                        console.log("ExportNamedDeclaration: VariableDeclarator - unknown export type ", d.type);
                     }
                  });

                  break;

               case "FunctionDeclaration":

                  if (!path.node.declaration.id) {
                     console.log("ExportNamedDeclaration: missing declaration id");
                     break;
                  }

                  exports.push({
                     astNodePath: path,
                     default: false,
                     name: path.node.declaration.id.name,
                     internalRefCount: 0,
                     externalRefCount: 0
                  });

                  break;

               case "ClassDeclaration":

                  if (!path.node.declaration.id) {
                     console.log("ExportNamedDeclaration: missing declaration id");
                     break;
                  }

                  exports.push({
                     astNodePath: path,
                     default: false,
                     name: path.node.declaration.id.name,
                     internalRefCount: 0,
                     externalRefCount: 0
                  });
                  break;

               default:
                  console.log(`ExportNamedDeclaration: invalid declaration node type '${(<any>(path.node.declaration)).type}'`);

            }

         }

         else if (path.node.source) {

            if (path.node.specifiers) path.node.specifiers.forEach(

               s => {

                  if (s.exported.type === "Identifier") {

                     imports.push({
                        astNodePath: path,
                        exportName: s.exported.name,
                        importName: s.exported.name,
                        moduleName: (<string>(<ExportNamedDeclaration>path.node).source!.value!),
                        isReexport: true,
                        internalRefCount: 0
                     });

                     exports.push({
                        astNodePath: path,
                        default: s.exported.name === "default",
                        name: s.exported.name,
                        internalRefCount: 0,
                        externalRefCount: 0
                     });

                  }

               }

            );

         } else {

            exports.push({
               astNodePath: path,
               default: false,
               name: "",
               internalRefCount: 0,
               externalRefCount: 0
            });

         }


      }

      if (path.node.type === "ExportDefaultDeclaration") {
         console.log("Unprocessed ExportDefaultDeclaration: ", path.node.declaration);
      }

      if (path.node.type === "ExportAllDeclaration") {
         console.log("Unprocessed ExportAllDeclaration: ", path.node.source);
      }

   }

   #processExportDefaultDeclaration(path: NodePath<ExportDefaultDeclaration, Node>, exports: Export[]): void {

      if (!path.node) return;
      if (!path.node.declaration) return;

      switch (path.node.declaration.type) {

         case "ClassDeclaration":
         case "FunctionDeclaration":
            if (!path.node.declaration.id) return;
            exports.push({
               astNodePath: path,
               default: true,
               name: path.node.declaration.id.name,
               internalRefCount: 0,
               externalRefCount: 0
            });
            break;

         case "Identifier":
            if (!path.node.declaration.name) return;
            exports.push({
               astNodePath: path,
               default: true,
               name: path.node.declaration.name,
               internalRefCount: 0,
               externalRefCount: 0
            });

      }


   }

}