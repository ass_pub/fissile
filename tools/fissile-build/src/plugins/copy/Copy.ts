/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { join, resolve } from "path";
import { KeyValue } from "../../lib/@types/KeyValue.js";
import { DiagEntry, FileInfo, Plugin, FissileBuildPipelineContext } from "../Plugin.js";

interface TplCopyConfigOtions {
   copy: KeyValue<string>;
}


const defaultOptions: TplCopyConfigOtions = {
   copy: {}
};

export default class Copy extends Plugin<TplCopyConfigOtions> {

   protected _displayName: string;
   protected _copyrights: string[];
   protected _version: string;
   protected _id: string;

   protected _configurable = true;

   constructor(context: FissileBuildPipelineContext) {

      super(context);

      this._displayName =  "Recursive File Copier";
      this._version = "1.0.0";
      this._id = "copy";
      this._copyrights = [ "Copyright (c)2021 Atom Software Studios, released under MIT license" ];

   }

   public override init(): void {

      this._options = this._loadPluginOptions(defaultOptions, this._context.pluginConfigPath);

   }

   public override ls(): string[] | undefined {

      let files: string[] = [];

      if (!this._options || !this._options.copy) {
         this._context.diag.warn(this.constructor.name, "ls", "No files specified, nothing to do.");
         return [];
      }

      for (const src in this._options.copy) {

         const f = super.ls(src);
         if (!f) continue;

         files = [ ...files, ...f ];
      }

      return files;
   }

   public override processFiles(files: FileInfo[]): { files: FileInfo[], diag: DiagEntry[] } {

      const outFiles: FileInfo[] = files.map(
         f => <FileInfo>{
            srcFileHash: f.srcFileHash,
            srcFileName: f.srcFileName,
            fileContent: f.fileContent,
            sourceMapfileContent: f.sourceMapfileContent,
            dstFileName: this.#mapInputFileName(f.srcFileName)
         }
      );

      return {
         files: outFiles,
         diag: []
      };

   }

   #mapInputFileName(fileName: string): string | undefined {

      for (const srcDir in this._options?.copy) {

         const aSrc = resolve(srcDir);

         if (fileName.startsWith(aSrc)) {
            let aTgt = resolve(this._context.fbConfig.distDir[this._context.fbConfig.env]);
            aTgt = join(aTgt, this._options!.copy[srcDir]);
            const targetFilenName = join(aTgt, fileName.substring(aSrc.length + 1));
            return targetFilenName;
         }

      }

      return undefined;
   }


}