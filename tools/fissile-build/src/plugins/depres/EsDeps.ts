/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2021 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as estree from "estree";
import { parseModule } from "meriyah";
import { traverse, NodePath } from "estree-toolkit";

import { EsDep } from "./EsDep";

/**
 * EcmaScript Dependency Analyzer
 * Searches for dependencies on another modules
 * Currently, CommonJS and esnext modules are supported (static and dynamic)
 */
export class EsDeps {

   /**
    * Returns list of dependencies collected from the file (string passed)
    * @param url Relative url to baseUrl of the file relative to the ulr base directory
    */
   public getDeps(script: string): EsDep[] {

      const program = parseModule(script, { next: true });

      const deps: EsDep[] = [];

      traverse(

         program,

         {
            "CallExpression": (path: NodePath<estree.SimpleCallExpression, estree.Node>): void => {
               this.#processRequireImport(path, deps);
            },

            "ImportDeclaration": (path: NodePath<estree.ImportDeclaration, estree.Node>): void => {
               this.#processImportDeclaration(path, deps);
            },

            "ImportExpression": (path: NodePath<estree.ImportExpression, estree.Node>): void => {
               this.#processImportExpression(path, deps);
            },

            "ExportAllDeclaration": (path: NodePath<estree.ExportAllDeclaration, estree.Node>) => {
               this.#processExportAllDeclaration(path, deps);

            },

            "ExportDeclaration": (path: NodePath<estree.ExportNamedDeclaration | estree.ExportDefaultDeclaration | estree.ExportAllDeclaration, estree.Node>): void => {
               this.#processExportDeclaration(path, deps);
            },

            "ExportDefaultDeclaration": (path: NodePath<estree.ExportDefaultDeclaration, estree.Node>) => {
               this.#processExportDefaultDeclaration(path, deps);
            }

         }

      );

      return deps;
   }


   /**
    *
    * @param path
    * @param deps
    * @returns
    */
   #processRequireImport = (path: NodePath<estree.SimpleCallExpression, estree.Node>, deps: EsDep[]): void => {

      // path node must exist
      if (!path.node) return;

      // look just for CallExpression with the require identifier
      if (path.node.callee.type !== "Identifier") return;

      if (path.node.callee.name !== "require") return;

      // // exactly 1 grgument must be used within the require/import CallExpression
      if (!path.node.arguments || path.node.arguments.length !== 1) return;

      if (path.node.arguments[0].type !== "Literal" || !typeof path.node.arguments[0].value) return;

      // identify if the require is static or dynamic (in meaning of ES6)

      let dynamic = false;

      // if require call argument is anything else than string literal it is a dynamic import

      if (path.node.arguments[0].type !== "Literal" || typeof path.node.arguments[0].value !== "string") dynamic = true;

      const module = dynamic ? undefined : (<string>(<estree.Literal>path.node.arguments[0]).value);

      let ancestor = path.parentPath;

      // also, if the require is not in the root block of the document it is definitely a dynamic import
      while(ancestor !== null) {
         if (ancestor.node?.type === "BlockStatement") {
            dynamic = true;
            break;
         }
         ancestor = ancestor.parentPath;
      }

      // push info about the discovered dependency
      deps.push({
         type: "cjs",
         dynamic,
         path,
         module
      });

   };

   /**
    *
    * @param path
    * @param deps
    */
   #processImportDeclaration(path: NodePath<estree.ImportDeclaration, estree.Node>, deps: EsDep[]): void {

      if (!path.node) return;

      if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

      deps.push({
         type: "mjs",
         dynamic: false,
         path,
         module: path.node.source.value
      });

   }


   /**
    *
    * @param path
    * @param deps
    */
   #processImportExpression(path: NodePath<estree.ImportExpression, estree.Node>, deps: EsDep[]): void {

      if (!path.node) return;

      if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

      deps.push({
         type: "mjs",
         dynamic: true,
         path,
         module: path.node.source.value
      });

   }

   /**
    *
    * @param path
    * @param deps
    */
   #processExportAllDeclaration(path: NodePath<estree.ExportAllDeclaration, estree.Node>, deps: EsDep[]): void {

      if (!path.node) return;
      if (!path.node.source) return;
      if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

      deps.push({
         type: "mjs",
         dynamic: false,
         path,
         module: path.node.source.value
      });

   }

   /**
    *
    * @param path
    * @param deps
    */
   #processExportDeclaration(path: NodePath<estree.ExportDefaultDeclaration | estree.ExportNamedDeclaration | estree.ExportAllDeclaration, estree.Node>, deps: EsDep[]): void {

      if (!path.node) return;
      if (path.node.type === "ExportDefaultDeclaration") return;
      if (!path.node.source) return;
      if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

      deps.push({
         type: "mjs",
         dynamic: false,
         path,
         module: path.node.source.value
      });

   }


   /**
    *
    * @param path
    * @param deps
    */
   #processExportDefaultDeclaration(path: NodePath<estree.ExportDefaultDeclaration, estree.Node>, deps: EsDep[]): void {

      if (!path.node) return;
      //if (path.node.source.type !== "Literal" || !path.node.source.value || typeof path.node.source.value !== "string") return;

   }

}
