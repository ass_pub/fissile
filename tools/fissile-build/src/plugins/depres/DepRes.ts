/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { KeyValue } from "../../lib/@types/KeyValue.js";
import { DiagEntry, FileInfo, Plugin, FissileBuildPipelineContext, ProcessedInfo } from "../Plugin.js";
import { EsDeps } from "./EsDeps.js";
import { EsDep } from "./EsDep";
import { dirname, extname, join, normalize, resolve } from "path";
import { existsSync, readFileSync, statSync } from "fs";


interface DepResConfigOtions {
   baseUrl: string;
   targetNodeModulesFolder: string;
}

export interface Module {
   filename: string;
   dependent: Module;
   references: Module[];
   requires: Module[];
   code?: string;
}

const defaultOptions: DepResConfigOtions = {
   baseUrl: "./",
   targetNodeModulesFolder: "node_modules"
};


export default class DepRes extends Plugin<DepResConfigOtions> {

   protected _displayName: string;
   protected _copyrights: string[];
   protected _version: string;
   protected _id: string;

   protected _configurable = true;

   #esDeps?: EsDeps;
   #deps: KeyValue<EsDep[]> = {};

   constructor(context: FissileBuildPipelineContext) {

      super(context);

      this._displayName =  "Dependency resolver";
      this._version = "1.0.0";
      this._id = "depres";
      this._copyrights = [ "Copyright (c)2021 Atom Software Studios, released under MIT license" ];

   }

   public override init(): void {

      this._options = this._loadPluginOptions(defaultOptions, this._context.pluginConfigPath);
      this.#esDeps = new EsDeps();

   }

   public override ls(): string[] | undefined {
      return undefined;
   }

   public override processFiles(files: FileInfo[]): ProcessedInfo {

      const diag: DiagEntry[] = [];

      if (!this.#esDeps) {

         diag.push({
            type: "error",
            message: "DepRes plugin has not been initialized before processFiles method has been called.",
            code: "",
            column: -1,
            line: -1,
            fileName: "",
            pos: -1,
            snip: "",
         });

         return {
            files,
            diag
         };

      }

      this.#updateDependencies(files, diag);

      return {
         files,
         diag
      };

   }


   #updateDependencies(files: FileInfo[], diag: DiagEntry[]): void {

      const newDepList: KeyValue<EsDep[]> = {};

      // update dependencies just for changed files and/or files missing in the dependency list
      for (const f of files) {

         if (!f.dstFileName) {

            diag.push({
               type: "warning",
               fileName: f.srcFileName,
               message: "Unable to resolve dependencies. Missing destination filename, check pipeline configuration.",
               code: "", line: -1, pos: -1, column: -1, snip: ""
            });

            continue;

         }

         switch (extname(f.dstFileName)) {

            case ".js":
            case ".cjs":
            case ".mjs":
               this.#resolveJavaScriptModuleDependencies(files, f, newDepList, diag);
               break;

            default:
               diag.push({
                  type: "warning",
                  fileName: f.srcFileName,
                  message: "Dependencies are not checked for this type of file",
                  code: "", line: -1, pos: -1, column: -1, snip: ""
               });

               continue;

         }

      }

      for (const k in newDepList) {
         this._context.diag.info(this.constructor.name, "updateDependencies", 0, `      File '${k}' depends on (just modified files are listed):`);
         for (const f of newDepList[k])
            this._context.diag.info(this.constructor.name, "updateDependencies", 0, `         '${f.module}'`);
      }

   }


   #resolveJavaScriptModuleDependencies(changedFiles: FileInfo[], dependent: FileInfo, depList: KeyValue<EsDep[]>, diag: DiagEntry[], resolved: string[] = []){

      if (resolved.indexOf(normalize(dependent.dstFileName!)) !== -1) return;

      const deps = this.#esDeps!.getDeps(dependent.fileContent.toString());

      depList[dependent.dstFileName!] = [];

      deps.forEach(d => {

         const mpath = this.#resolve(dependent.dstFileName!, d.module!);

         if (changedFiles.find(chf => normalize(chf.dstFileName!) === mpath) === undefined && !mpath?.endsWith(".json")) {
            this.context.diag.info(this.constructor.name, "resolveJavaScriptModuleDependencies", 0, `Dependency '${d.module}' was not modified, skipping.`);
            return;
         }

         if (mpath === undefined) {
            diag.push({
               type: "warning",
               fileName: dependent.srcFileName,
               message: "Unable to resolve dependencies. Missing destination filename, check pipeline configuration.",
               code: "", line: -1, pos: -1, column: -1, snip: ""
            });
            return;
         }

         const nmpath = normalize(mpath);

         switch (extname(nmpath)) {

            case ".js":
            case ".cjs":
            case ".mjs": {

               depList[dependent.dstFileName!].push(d);

               this.#resolveJavaScriptModuleDependencies(
                  changedFiles,
                  {
                     srcFileName: "",
                     fileContent: readFileSync(nmpath),
                     dstFileName: nmpath,
                  },
                  depList,
                  diag,
                  resolved
               );

               resolved.push(nmpath);

               break;

            }

            case ".json": {

               const pjsonm = this.#getPackageJsonMainOrModule(nmpath);

               if (changedFiles.find(chf => normalize(chf.dstFileName!) === pjsonm) === undefined) {
                  this.context.diag.info(this.constructor.name, "resolveJavaScriptModuleDependencies", 0, `Dependency '${d.module}' was not modified, skipping.`);
                  return;
               }

               if (pjsonm === undefined) {
                  diag.push({
                     type: "warning",
                     fileName: dependent.srcFileName,
                     message: `Unable to resolve module ${nmpath}`,
                     code: "", line: -1, pos: -1, column: -1, snip: ""
                  });
                  return;
               }

               this.#resolveJavaScriptModuleDependencies(
                  changedFiles,
                  {
                     srcFileName: "",
                     fileContent: readFileSync(pjsonm),
                     dstFileName: pjsonm,
                  },
                  depList,
                  diag,
                  resolved
               );

               break;
            }

         }

         // if (depJSFiles.indexOf(mpath) === -1) depJSFiles.push(mpath);

      });


   }


   #resolve(dependent: string, dependency: string): string | undefined {

      let filename: string | undefined;

      // try to locate module on relative path to parent
      if (dependency.startsWith("./") || dependency.startsWith("../")) {

         filename = this.#tryFiles(dirname(dependent), dependency);
         if (filename !== undefined && existsSync(filename)) return filename;

      // try to locate module on baseUrl or node_modules up to the fs root
      } else {

         const baseUrl = resolve(this._context.fbConfig.distDir[this._context.fbConfig.env], this._options.baseUrl);

         // firstly, try to locate file on base url
         filename = this.#tryFiles(baseUrl, dependency);
         if (filename !== undefined && existsSync(filename)) return filename;

         // go up the fs dirs and try to locate module in node_modules folder
         let currentPath = baseUrl;

         while (currentPath !== "/") {

            const nodeModules = resolve(currentPath, "./node_modules");

            filename = this.#tryFiles(nodeModules, dependency);
            if (filename !== undefined && existsSync(filename)) return filename;

            currentPath = resolve(currentPath, "..");

         }

      }

      return undefined;

   }


   // tries to locate the file on the passed path or according to defined rules
   #tryFiles(baseUrl: string, filename: string): string | undefined {

      const tests: string[] = [
         filename,
         filename + ".js",
         filename + ".cjs",
         filename + ".mjs",
         filename + "/package.json",
         filename + "/index.js",
         filename + filename.substring(filename.lastIndexOf("/")) + ".js",
         filename + filename.substring(filename.lastIndexOf("/")) + ".cjs",
         filename + filename.substring(filename.lastIndexOf("/")) + ".mjs"
      ];

      for (let i = 0; i < tests.length; i++) {
         const filename = resolve(baseUrl, tests[i]);
         if (existsSync(filename) && !statSync(filename).isDirectory()) {
            return filename;
         }
      }

      return undefined;

   }


   // tries to resolve main module defined in the package.json
   #getPackageJsonMainOrModule(filename: string): string | undefined {

      const base = dirname(filename);

      if (!existsSync(filename)) {
         return undefined;
      }

      const f = readFileSync(filename).toString();
      const pkg = JSON.parse(f);

      if (pkg.es2015) {
         const main = join(base, pkg.es2015);
         if (existsSync(main)) return main;
      }

      if (pkg.module) {
         const main = join(base, pkg.module);
         if (existsSync(main)) return main;
      }

      if (pkg.main) {
         const main = join(base, pkg.main);
         if (existsSync(main)) return main;
      }

      return undefined;
   }


}