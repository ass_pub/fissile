/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { existsSync, mkdirSync, writeFileSync } from "fs";
import { dirname } from "path";
import { DiagEntry, FileInfo, Plugin, FissileBuildPipelineContext, ProcessedInfo } from "../Plugin.js";


export default class Emit extends Plugin<void> {

   protected _displayName: string;
   protected _copyrights: string[];
   protected _version: string;
   protected _id: string;

   protected _configurable = false;

   constructor(context: FissileBuildPipelineContext) {

      super(context);

      this._displayName =  "Emit";
      this._version = "1.0.0";
      this._id = "emit";
      this._copyrights = [ "Copyright (c)2021 Atom Software Studios, released under MIT license" ];

   }

   public override init() {
      // Implement!
   }

   public override ls(): string[] | undefined {
      return undefined;
   }

   public override processFiles(files: FileInfo[]): ProcessedInfo {

      const diag: DiagEntry[] = [];

      files.forEach(
         f => {
            if (f.dstFileName) {

               try {

                  const path = dirname(f.dstFileName);
                  if (!existsSync(path)) mkdirSync(path, { recursive: true });

                  this._context.diag.info(this.constructor.name, "processFiles", 0, `      Writing '${f.fileContent.length}' bytes to '${f.dstFileName}'`);
                  writeFileSync(f.dstFileName, f.fileContent);

                  if (f.sourceMapfileContent) {
                     this._context.diag.info(this.constructor.name, "processFiles", 0, `      Writing '${f.sourceMapfileContent.length}' bytes to '${f.dstFileName}.map'`);
                     writeFileSync(f.dstFileName + ".map", f.sourceMapfileContent);
                  }

               } catch (e) {
                  this._context.diag.error(this.constructor.name, "processFiles", `      Failed to create ouptut files for '${f.srcFileName}': ${e}`);
               }

            } else {
               this._context.diag.warn(this.constructor.name, "processFiles", `      Destination file name not set for '${f.srcFileName}', skipping.`);
            }
         }
      );

      return {
         files,
         diag
      };

   }


}