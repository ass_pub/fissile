/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { existsSync, readFileSync, statSync } from "fs";
import { dirname, isAbsolute, resolve } from "path";

import { stripComments } from "../lib/json/index.js";
import { lsRecursive } from "../lib/filesystem.js";

import { PipelineManager } from "../pipeline/PipelineManager.js";
import FissileBuildContext from "../FissileBuildContext.js";


export interface FissileBuildPipelineContext extends FissileBuildContext {
   pipelineManager: PipelineManager;
   pluginConfigPath: string;
}


export interface PluginOptions {
   fileProvider: string;
   stopOnError: boolean;
   plugins: string[];
}


export interface FileInfo {
   srcFileName: string;
   dstFileName?: string;
   fileContent: Buffer;
   sourceMapfileContent?: Buffer;
   depfileContent?: Buffer;
   srcFileHash?: string;
}


export interface DiagEntry {
   type: "warning" | "error";
   message: string;
   code: string;
   fileName: string;
   pos: number;
   line: number;
   column: number;
   snip: string;
}


export interface ProcessedInfo {
   files: FileInfo[];
   diag: DiagEntry[];
}


export interface PluginConfig<T> {
   extends?: string;
   options?: T;
}


export interface PluginCtor<T> {
   new (context: FissileBuildPipelineContext): Plugin<T>;
}


export abstract class Plugin<T> {


   /**
    * Display name of the plugin i.e.
    * "TypeScript Fissile Build Plugin"
    */
   public get displayName(): string { return this._displayName; }
   protected abstract _displayName: string;


   /**
    * Copyright and license information i.e.
    * "Copyright (c)2021 Atom Software Studios, released under MIT license"
    * Copyrights of all libraries used directly by the plugin should be mentioned
    */
   public get copyrights(): string[] { return this._copyrights; }
   protected abstract _copyrights: string[];


   /**
    * Display version of the plugin
    * i.e. 1.0.0
    */
   public get version(): string { return this._version; }
   protected abstract _version: string;


   /**
    * Unique plugin id (to be used in the fissile-build.mjs) i.e.
    * "typescript"
    */
   public get id(): string { return this._id; }
   protected abstract _id: string;


   /**
    * Specifies if the plugin is configuratble through config file
    */
   public get configurable(): boolean { return this._configurable; }
   protected abstract _configurable: boolean;

   /**
    * Initializes a plugin
    * - Loads a config (or uses default if not found or can't be loaded)
    * - Initializes instances of internally used toolset
    */
   public abstract init(): void;

   /**
    * Processes list of passed files and returns processed results
    * @param files
    */
   public abstract processFiles(files: FileInfo[]): ProcessedInfo | Promise<ProcessedInfo>;


   /** Holds information about current build and pipeline context */
   public get context(): FissileBuildPipelineContext { return this._context; }
   protected _context: FissileBuildPipelineContext;


   /** Holds plugin configuration options */
   protected _options: T;


   /**
    * Creates instance of the plugin and stores the the plugin context info
    * @param context Information about current build and pipeline context
    */
   constructor(context: FissileBuildPipelineContext) {
      this._context = context;
      this._options = <any>{};
   }


   /**
    * Returns list of files in the scope of the plugin
    * If the plugin is not a file source plugin, undefined must be returned
    * This method is supposed to be overriden and called using the super.ls from the inherited ls method
    */
   public ls(srcDir = ""): string[] | undefined {

      if (!this._context) throw new Error("Plugin not initialized!");
      if (srcDir === "") throw new Error("Invalid call. Method is supposed to be overriden and called using super keywod!");

      const root = resolve(srcDir);

      try {
         return [ ...lsRecursive(root) ];
      } catch (e) {
         this._context.diag.warn(this.constructor.name, "ls", `   Failed to obtain list of files/directories: ${e} (no processing for this pipeline)`);
         return [];
      }

   }



   /**
    * Loads a standard plugin configuration file ( { extends: "", options: { ... } } )
    * @param defaultOptions Default plugin options to be extended with those loaded from config files
    * @param path Path to the config file to be loaded
    * @param jsonSchema Schema to be used for the JSON configuration file validation
    * @returns Default plugin options extended with options loaded from configuration files
    */
   protected _loadPluginOptions(defaultOptions: T, path: string, jsonSchema?: string): T {

      if (!path) return defaultOptions;

      const toExtend: T = JSON.parse(JSON.stringify(defaultOptions));
      const options = this._extendPluginOptions(toExtend, path, jsonSchema);

      this._context.diag.info(this.constructor.name, "_loadPluginOptions", 0, "      Config:", options);

      return options;

   }


   /**
    * Extends passed options with options located in the configuration file
    * @param options Options object to be extended
    * @param path Path to the configuration file to be loaded
    * @param jsonSchema Schema to be used for the JSON configuration file validation
    * @returns
    */
   protected _extendPluginOptions(options: T, path: string, jsonSchema?: string): T {

      let result: T = options;

      const aPath = resolve(path);
      if (!existsSync(aPath)) {
         this._context.diag.warn(this.constructor.name, "_extendPluginOptions", `      Failed to load config file ${aPath}: File not found (default config will be used)`);
         return result;
      }

      const stat = statSync(aPath);
      if (!stat.isFile()) {
         this._context.diag.warn(this.constructor.name, "_extendPluginOptions", `      Failed to load config file ${aPath}: Regular file expected (default config will be used)`);
         return result;
      }

      try {

         const extensionFile = readFileSync(aPath);
         const extensionJson = stripComments(extensionFile.toString());
         const extension: PluginConfig<T> = JSON.parse(extensionJson);

         if (jsonSchema && !this._validateJson(extension, jsonSchema)) {
            this._context.diag.warn(this.constructor.name, "_extendPluginOptions", `      Failed to load config file ${aPath}: Regular file expected (default config will be used)`);
            return result;
         }

         if (extension.options) {
            result = { ...result, ...extension.options };
         }

         if (extension.extends) {
            const extPath = isAbsolute(extension.extends) ? extension.extends : resolve(dirname(aPath), extension.extends);
            result = this._extendPluginOptions(result, extPath, jsonSchema);
         }

      } catch(e) {

         this._context.diag.warn(this.constructor.name, "_extendPluginOptions", `      Failed to load config file ${aPath}: ${e} (default config will be used)`);
         return result;

      }

      return result;

   }


   protected _validateJson(jsonObject: PluginConfig<T>, jsonSchema: string): boolean {
      this._context.diag.warn(this.constructor.name, "_validateJson", "      JSON Object validation not implemented yet");
      if (jsonSchema && jsonSchema) { /** Implement! */ }
      return true;
   }


}