/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2021 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { normalize } from "path";
import * as tsns from "typescript";

import { EventDispatcher } from "../../lib/events/EventDispatcher.js";
import { FileInfo, FissileBuildPipelineContext } from "../Plugin.js";

const ts: typeof tsns = (<any>tsns).default;

export interface TsCompilerHost extends tsns.CompilerHost {
   updateFileInfo: (fileInfo: FileInfo[]) => void;
   onFileWritten: EventDispatcher<string>;
}

export function createCompilerHost(
   context: FissileBuildPipelineContext,
   options: tsns.CompilerOptions
): TsCompilerHost {

   let pipelineFileInfo: FileInfo[] = [];

   const onFileWritten = new EventDispatcher<string>();
   const compilerHost = <TsCompilerHost>ts.createCompilerHost(options, false);
   const origWriteFile: tsns.WriteFileCallback = compilerHost.writeFile;

   /**
    * Supposed to be called before compilation will start
    * @param fileInfo List of files passed to the plugin for processing
    */
   function updateFileInfo(this: TsCompilerHost, fileInfo: FileInfo[]): void {

      pipelineFileInfo = fileInfo;

   }


   /**
    * Called from TypeScript to obtain a file source code
    * @param this Reference to the TsCompilerHost object
    * @param fileName Name of the file to be read
    * @param languageVersion Target language (ES6, ESNEXT...)
    * @param onError Callback to call when an error occurs
    * @param shouldCreateNewSourceFile Identifies if the source file should be created
    * @returns
    */
   function getSourceFile(
      this: TsCompilerHost,
      fileName: string,
      languageVersion: tsns.ScriptTarget,
      onError?: (message: string) => void,
      shouldCreateNewSourceFile?: boolean
   ): tsns.SourceFile | undefined {

      // first, try to find a file in list of files passed by pipeline
      const fileInfo = pipelineFileInfo.find(f => normalize(f.srcFileName) === normalize(fileName));
      if (fileInfo) return ts.createSourceFile(fileName, fileInfo.fileContent.toString(), languageVersion);

      // otherwise read the file from the disk
      const sourceText = ts.sys.readFile(fileName);
      const sourceFile = sourceText !== undefined ? ts.createSourceFile(fileName, sourceText, languageVersion) : undefined;

      return sourceFile;

   }

   function writeFile(
      this: TsCompilerHost,
      fileName: string,
      data: string,
      writeByteOrderMark: boolean,
      onError?: (message: string) => void,
      sourceFiles?: readonly tsns.SourceFile[]
   ): void {

      // first, try to find a source file in a list of files passed by pipeline
      // for now, ignore multiple source files
      if (sourceFiles?.length === 1) {
         const fileInfo = pipelineFileInfo.find(f => normalize(f.srcFileName) === normalize(sourceFiles[0].fileName));
         if (fileInfo) {
            fileInfo.fileContent = Buffer.from(data);
            fileInfo.dstFileName = fileName;
            return;
         }
      }

      origWriteFile(fileName, data, writeByteOrderMark, onError, sourceFiles);
      onFileWritten.dispatch(this, fileName);

   }

   compilerHost.updateFileInfo = updateFileInfo;
   compilerHost.getSourceFile = getSourceFile;
   compilerHost.writeFile = writeFile;
   compilerHost.onFileWritten = onFileWritten;

   return compilerHost;

}
