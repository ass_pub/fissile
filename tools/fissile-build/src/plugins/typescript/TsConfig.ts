/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import * as fs from "fs";
import * as path from "path";
import * as tsns from "typescript";

import FissileBuildContext from "../../FissileBuildContext";
import * as json from "../../lib/json/index.js";

const ts: typeof tsns = (<any>tsns).default;

export class TsConfig {

   #context: FissileBuildContext;

   #rootConfigPath: string;

   #allConfigFiles: string[];
   public get allConfigFiles(): string[] { return this.#allConfigFiles; }

   #parsedCmdLine: tsns.ParsedCommandLine | undefined;
   public get parsedCmdLine(): tsns.ParsedCommandLine | undefined { return this.#parsedCmdLine; }

   /**
    * Constructs the wrapper for typescript configuration file loader
    */
   constructor(context: FissileBuildContext) {

      this.#context = context;

      this.#rootConfigPath = "";

      this.#allConfigFiles = [];

      this.#parsedCmdLine = {
         errors: [],
         fileNames: [],
         options: {},
         projectReferences: [],
         typeAcquisition: {},
         raw: {},
         wildcardDirectories: {},
         compileOnSave: false
      };

   }

   /**
    * Should be called all the time when the configuration file is changed
    * @param fileName Main typescript compiler configuration file to be loaded
    */
   public updateConfigFile(configFileName?: string): boolean {

      let fileName = configFileName || this.#rootConfigPath;

      if (!fileName) {
         this.#context.diag.error(this.constructor.name, "updateConfigFile", "      Failed to update config file, no config file exist");
         return false;
      }

      // if the file tsconfig.<buildConfig>.json not exist, try tsconfig.json
      if (!fileName.endsWith("tsconfig.json")) {
         if (!fs.existsSync(fileName)) {
            this.#context.diag.warn(this.constructor.name, "updateConfigFile", `      Config '${fileName}' not found, trying tsconfig.json`);
            fileName = path.join(path.dirname(fileName), "tsconfig.json");
         }
      }

      this.#context.diag.info(this.constructor.name, "updateConfigFile", 1, `      Loading configuration file '${fileName}'`);

      // loads and parses the configuration file
      this.#rootConfigPath = fileName;
      if (!this.#loadConfig()) return false;

      // check for errors
      if (this.#parsedCmdLine && this.#parsedCmdLine.errors.length !== 0) {
         for (const err of this.#parsedCmdLine.errors) {
            this.#context.diag.error(this.constructor.name, "updateConfigFile", `      ${err.messageText.toString()}`);
         }
         return false;
      }

      // stores paths to all config files extended by the root (used when watch mode is enabled to monitor config files changes)
      this.#allConfigFiles = [];
      if (!this.#findAllConfigFiles(this.#rootConfigPath)) return false;

      this.#context.diag.info(this.constructor.name, "updateConfigFile", 1, "      Configuration updated successfully");

      if (configFileName) this.#configDiag();

      return true;

   }

   /**
    * Prints configuration diagnostic
    */
   #configDiag(): void {

      if (!this.#parsedCmdLine) {
         this.#context.diag.warn(this.constructor.name, "configDiag", "         No configuration available!");
         return;
      }

      this.#context.diag.info(this.constructor.name, "configDiag", 2, `         Main config file:      '${this.#rootConfigPath}'`);
      this.#context.diag.info(this.constructor.name, "configDiag", 2, `         Extends:               '${this.#allConfigFiles.length - 1}' config file/s`);

      for (let i = 1; i < this.#allConfigFiles.length; i++) {
         this.#context.diag.info(this.constructor.name, "configDiag", 2, `         Extended config file:  '${path.relative(path.dirname(this.#rootConfigPath), this.#allConfigFiles[i])}'`);
      }

      this.#context.diag.info(this.constructor.name, "configDiag", 2, "         Options:");

      const preventDisplay = [ "configFile", "configFilePath" ];
      for (const k in this.#parsedCmdLine.options) {

         let text = "";
         const option: tsns.CompilerOptionsValue | tsns.TsConfigSourceFile = this.#parsedCmdLine.options[k];

         switch (k) {
            case "moduleResolution":
               text = `${ts.ModuleResolutionKind[option as number]}`;
               break;
            case "module":
               text = `${ts.ModuleKind[option as number]}`;
               break;
            case "target":
               text = `${ts.ScriptTarget[option as number]}`;
               break;
            default:
               if (preventDisplay.indexOf(k) === -1) {
                  text = `${this.#parsedCmdLine.options[k]}`;
               }
         }

         if (text !== "") {
            this.#context.diag.info(this.constructor.name, "configDiag", 2, `            ${(k + ":                         ").substr(0, 25)}'${text}'`);
         }
      }

      const sfiles = this.#parsedCmdLine.fileNames.filter(
         (value: string) => { return !value.endsWith(".d.ts"); }
      );

      const tfiles = this.#parsedCmdLine.fileNames.filter(
         (value: string) => { return value.endsWith(".d.ts"); }
      );

      this.#context.diag.info(this.constructor.name, "configDiag", 2, `         '${sfiles.length}' source files will be included in the build process:`);

      for (const f of sfiles) {
         this.#context.diag.info(this.constructor.name, "configDiag", 4, `            '${f}'`);
      }

      this.#context.diag.info(this.constructor.name, "configDiag", 2, `         '${tfiles.length}' type files will be included in the build process:`);

      for (const f of tfiles) {
         this.#context.diag.info(this.constructor.name, "configDiag", 4, `            '${f}'`);
      }

   }

   /**
    * Loads a configuration file using the standard TypeScript API functions
    */
   #loadConfig(): boolean {

      this.#context.diag.info(this.constructor.name, "loadConfig", 1, "      Loading TypeScript configuration");

      let unrecoverableOccured = false;

      // configure parse config host
      const host: tsns.ParseConfigFileHost = {

         // current directory is always solution path
         getCurrentDirectory: (): string => {
            return process.cwd(); // this.#context.config.srcDir;
         },

         // some diagnostics
         onUnRecoverableConfigFileDiagnostic: (diagnostic: tsns.Diagnostic): void => {
            this.#context.diag.error(this.constructor.name, "loadConfig", `      ${diagnostic.messageText.toString()}`);
            unrecoverableOccured = true;
         },

         // read config and patch paths to fit solution and project
         readFile: (configPath: string): string | undefined => {
            return this.#readConfigAndPatch(configPath);
         },

         // use standard ts.sys functions for the rest
         useCaseSensitiveFileNames: ts.sys.useCaseSensitiveFileNames,
         readDirectory: ts.sys.readDirectory,
         fileExists: ts.sys.fileExists
      };

      // get the configuration for tsc - this path must point to the project root dir
      const configPath = path.resolve(this.#rootConfigPath);
      this.#parsedCmdLine = ts.getParsedCommandLineOfConfigFile(configPath, undefined, host);

      // patch options according the solution and project settings
      return !unrecoverableOccured;

   }

   /**
    * Checks if the config file extends another config file and tries to resolve and find all extended configs
    * @param configPath Path to config file to be checked if it extends another config file
    */
   #findAllConfigFiles(configPath: string): boolean {

      if (!fs.existsSync(configPath)) {
         this.#context.diag.error(this.constructor.name, "findAllConfigFiles", `      Config file '${configPath}' not found`);
         return false;
      }

      this.#allConfigFiles.push(configPath);

      if (configPath !== this.#rootConfigPath) {
         this.#context.diag.info(this.constructor.name, "findAllConfigFiles", 2, `      Found config file '${path.relative(path.dirname(this.#rootConfigPath), configPath)}'`);
      }

      let configExtends = "";

      try {
         const cfgContent = fs.readFileSync(configPath);
         const jsonText = json.stripComments(cfgContent.toString());
         const jsonJs = JSON.parse(jsonText);
         if (!Object.prototype.hasOwnProperty.call(jsonJs, "extends")) return true;
         configExtends = jsonJs.extends;
         this.#context.diag.info(this.constructor.name, "findAllConfigFiles", 2, `      Config '${path.basename(configPath)}' extends '${path.basename(configExtends)}'`);
      } catch (e) {
         this.#context.diag.error(this.constructor.name, "findAllConfigFiles", `      Failed to search for extended files: ${(<Error>e).message}`);
         return false;
      }

      const extpath = path.isAbsolute(configExtends) ? configExtends : path.resolve(path.dirname(configPath), configExtends);

      return this.#findAllConfigFiles(extpath);

   }

   /**
    * Reads a tsconfig file from disk and patches paths (baseUrl, rootDir, outDir, declarationDir)
    * @param configPath path to the config file to be loaded from the disk
    */
   #readConfigAndPatch(configPath: string): string | undefined {

      if (!fs.existsSync(configPath)) return undefined;

      const cfgContent = fs.readFileSync(configPath);
      const jsonText = json.stripComments(cfgContent.toString());
      const jsonJs = JSON.parse(jsonText);

      //this.#patchConfig(jsonJs);

      return JSON.stringify(jsonJs);
   }

   /**
    * Patches the tsconfig to fit abuild paths
    * @param jsonJs config object to be patched
    */

   /*#patchConfig(jsonJs: any): void {

      const relProjectSrc = "./" + path.relative(this.__project.solution.path, this.__etsConfig.projectSrcPath);

      // compile on save is not supported
      if (jsonJs.hasOwnProperty("compileOnSave")) {
         delete jsonJs.compileOnSave;
      }

      // references are not supported
      if (jsonJs.hasOwnProperty("references")) {
         delete jsonJs.references;
      }

      // fix include paths to match the project directory
      if (jsonJs.hasOwnProperty("include") && jsonJs.include instanceof Array) {
         for (let i = 0 ; i < jsonJs.include.length; i++) {
            jsonJs.include[i] = path.normalize(`${relProjectSrc}/${jsonJs.include[i]}`);
         }
      }

      // fix exclude paths to match the project directory
      if (jsonJs.hasOwnProperty("exclude") && jsonJs.exclude instanceof Array) {
         for (let i = 0 ; i < jsonJs.exclude.length; i++) {
            jsonJs.exclude[i] = path.normalize(`${relProjectSrc}/${jsonJs.exclude[i]}`);
         }
      }

      // remove unsupported compiler options and fix or add necessary paths
      if (!jsonJs.hasOwnProperty("compilerOptions")) jsonJs.compilerOptions = {};

      jsonJs.compilerOptions.rootDir = path.relative(this.__project.solution.path, this.__etsConfig.projectSrcPath);
      jsonJs.compilerOptions.baseUrl = path.relative(this.__project.solution.path, path.join(this.__project.solution.path, this.__project.solution.srcDirName));
      jsonJs.compilerOptions.outDir = path.relative(this.__project.solution.path, this.__etsConfig.projectOutPath);

      if (jsonJs.compilerOptions.sourceMap) {
         jsonJs.compilerOptions.sourceRoot = path.relative(this.__project.solution.path, this.__etsConfig.projectSrcPath);
      }

      if (jsonJs.compilerOptions.declaration || jsonJs.compilerOptions.composite) {
         jsonJs.compilerOptions.declarationDir = this.__etsConfig.projectTypesOutPath;
      }

      // jsonJs.outFile			projectTarget + filename
      // jsonJs.declarationDir		projectTarget + @types


   }*/

}
