/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2021 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import * as path from "path";
import * as tsns from "typescript";

import { FissileBuildPipelineContext } from "../Plugin.js";
import { TsCompilerHost, createCompilerHost } from "./TsCompilerHost.js";
import { TsConfig } from "./TsConfig";

const ts: typeof tsns = (<any>tsns).default;

interface CompilationResult {
   preEmitDiagnostics: readonly tsns.Diagnostic[];
   emitResult: tsns.EmitResult | undefined;
   writtenFiles: string[];
}

/**
 * Incremental TypeScript compiler implementation
 */
export class TsCompiler {

   #context: FissileBuildPipelineContext;

   /**
    * Holds extended TypeScript configuration loaded form tsconfig file(s)
    */
   #config: TsConfig;

   public get config(): TsConfig { return this.#config; }
   public set config(config: TsConfig) {
      this.#config = config;
      this.#program = undefined;
   }

   /**
    * Returns TypeScript compiler version
    */
   public get version(): string {
      return ts.version;
   }


   /**
    * Formats TypeScript diagnostics
    */
   public get formatDiagnostics(): typeof ts.formatDiagnostic {
      return ts.formatDiagnostic;
   }


   /**
    * Stores a compiler host
    */
   #compilerHost: TsCompilerHost | undefined;
   public get compilerHost(): TsCompilerHost | undefined { return this.#compilerHost; }


   /**
    * Stores instance of program when the initial compilation is done
    */
   #program: tsns.Program | undefined;
   public get program(): tsns.Program | undefined { return this.#program; }


   /**
    * Creates the instance of the TypeScript compiler wrapper
    * @param context Context to be used for diag and so on
    * @param tsConfig Instance of the TsConfig used to manipulate TypeScript config files
    */
   constructor(context: FissileBuildPipelineContext, tsConfig: TsConfig) {

      this.#context = context;
      this.#config = tsConfig;
      this.#compilerHost = undefined;
      this.#program = undefined;

   }

   /**
    * Initial or updated config file is used to create the compiler host
    * It must be called prior compilation or in case of incremental compilation after every config file(s) change.
    */
   public updateConfigFile(configFileName?: string): boolean {

      if (!this.#config.updateConfigFile(configFileName)) return false;
      if (!this.#config.parsedCmdLine) return false;

      try {
         this.#compilerHost = createCompilerHost(this.#context, this.#config.parsedCmdLine.options);
      } catch (e) {
         this.#context.diag.error(this.constructor.name, "updateConfigFile", `Failed to update config file: '${e}'`);
         return false;
      }

      this.#program = undefined;
      return true;

   }

   /**
    * Compiles a TypeScript project. Can be used also for incremental compilation.
    * @param emit Specifies if files shouls be emitted after compilation
    * @param fileNames List of files to be compiled. Only necessary or changed files should be passed.
    */
   public compile(emit = false, fileNames?: string[]): CompilationResult {

      if (!this.#config.parsedCmdLine) throw "Missing parsedCmdLine. Nothing to do!";
      if (!this.#compilerHost) throw "updateConfigFile must be called prior the compilation can be started!";

      // prepare list of files written to the disk
      // emit info returned from TSC does not need to contain complete list of files so compiler host is used
      const writtenFiles: string[] = [];

      // hook to file write event
      this.#compilerHost.onFileWritten.addListener((host: TsCompilerHost, fileName: string) => {
         writtenFiles.push(fileName);
      });

      // during incremental compilation it is necessary to identify all dependent files in order to be possible to perform full type checking of changed files
      if (this.#program && (<any>this.#program).getRefFileMap && fileNames instanceof Array && fileNames.length > 0) {
         this.#includeDependents(fileNames);
      }

      // check if files exist in config and eventually add them
      this.#config.updateConfigFile();

      // standard TSC API - compile program
      this.#program = ts.createProgram(
         this.#config.parsedCmdLine.fileNames,
         this.#config.parsedCmdLine.options,
         this.#compilerHost,
         this.#program,
         this.#config.parsedCmdLine.errors
      );

      // collect only compiled files matching input files and their dependednts

      const sourceFiles: tsns.SourceFile[] = [];

      if (fileNames && fileNames instanceof Array) {
         for (const fileName of fileNames) {
            const sourceFile = this.#program.getSourceFile(fileName);
            if (sourceFile) sourceFiles.push(sourceFile);
         }
      }

      // prepare diag info array and emit result

      const preEmitDiagnostics: tsns.Diagnostic[] = [];
      let emitResult: { emitSkipped: boolean, diagnostics: tsns.Diagnostic[], emittedFiles?: string[] } | undefined;


      // if some copiled files matches to input files
      if (sourceFiles.length > 0) {

         // collected diag just for files in interrest.
         for (const sf of sourceFiles) {
            const pED = ts.getPreEmitDiagnostics(this.#program, sf);
            for (const d of pED) {
               preEmitDiagnostics.push(d);
            }
         }

         // emit files, collect emit diag and list of files

         if (emit) {

            emitResult = {
               emitSkipped: false,
               diagnostics: [],
               emittedFiles: []
            };

            for (const sf of sourceFiles) {

               const er = this.#program.emit(sf);

               if (er.diagnostics && er.diagnostics instanceof Array) {
                  for (const d of er.diagnostics) emitResult.diagnostics.push(d);
               }

               if (er.emittedFiles && er.emittedFiles instanceof Array) {
                  for (const f of er.emittedFiles) emitResult.emittedFiles?.push(f);
               } else {
                  emitResult.emittedFiles?.push(sf.fileName);
               }

            }
         }

      } else {

         // if no input files matched just collect pre-emit diagnostics for the whole project

         const pED = ts.getPreEmitDiagnostics(this.#program);
         for (const d of pED) {
            preEmitDiagnostics.push(d);
         }

         emitResult = emit ? <any>this.#program.emit() : { emitSkipped: true, diagnostics: [] };

      }

      return { preEmitDiagnostics, emitResult, writtenFiles };

   }


   /**
    * Include dependent files
    * This method is using non-published TypeScript API methods and possibly can stop working with any future TypeScript version
    * @param fileNames List of file names on which other files depends
    */
   #includeDependents(fileNames: string[]): void {

      const tmp = [];
      const map = (<any>this.#program).getRefFileMap(); // non ts-api published

      for (const f of fileNames) {

         // ref file map is using / in path, even on windows
         let fn = f.replace(/\\/g, "/");

         // ref file map stores files in lower case if useCaseSensitiveFileNames returns false
         if (!this.#compilerHost?.useCaseSensitiveFileNames()) fn = fn.toLocaleLowerCase();

         // get dependents
         const refs = map.get(fn);
         if (!refs) continue;

         // for each dependent get its full file name with correct letter case and slashes
         for (const ref of refs) {
            const srcFile = this.#program?.getSourceFileByPath(ref.file);
            if (srcFile) tmp.push(path.normalize(srcFile.fileName));
         }

      }

      // extend passed fileNames with obtained information
      for (const f of tmp) {
         if (fileNames.indexOf(f) === -1) fileNames.push(f);
      }

   }

}