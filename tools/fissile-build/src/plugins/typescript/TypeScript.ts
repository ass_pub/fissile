/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import { createHash } from "crypto";
import { readFileSync } from "fs";
import * as path from "path";
import { resolve } from "path";
import { LineAndCharacter } from "typescript";
import { KeyValue } from "../../lib/@types/KeyValue.js";
import { DiagEntry, FileInfo, Plugin, FissileBuildPipelineContext, ProcessedInfo } from "../Plugin.js";
import { TsCompiler } from "./TsCompiler.js";

import { TsConfig } from "./TsConfig.js";

export default class TypeScript extends Plugin<void> {

   protected _displayName: string;
   protected _copyrights: string[];
   protected _version: string;
   protected _id: string;

   protected _configurable = true;

   #tsConfig: TsConfig;
   #tsCompiler: TsCompiler;

   #configHashes: KeyValue<string>;

   constructor(context: FissileBuildPipelineContext) {

      super(context);

      this._displayName =  "TypeScript";
      this._version = "1.0.0";
      this._id = "typescript";
      this._copyrights = [
         "Copyright (c)2021 Atom Software Studios, released under MIT license",
         "Copyright (c)Microsoft, released under Apache 2.0 license"
      ];

      this.#tsConfig = new TsConfig(context);
      this.#tsCompiler = new TsCompiler(context, this.#tsConfig);

      this.#configHashes = {};

   }


   public override init(): void {

      const configFile = this.context.fbConfig.pluginConfig[this.context.fbConfig.env][this.id];
      this.context.diag.info(this.constructor.name, "init", 0, `      TypeScript config file '${configFile} will be used'`);
      this.#tsCompiler.updateConfigFile(configFile);

      // get configs hashes
      for(const fn of this.#tsConfig.allConfigFiles) {
         const rfn = resolve(fn);
         const fc = readFileSync(rfn);
         this.#configHashes[rfn] = createHash("md5").update(fc).digest("hex");
      }

   }


   public override ls(): string[] | undefined {

      return [
         ...this.#tsConfig.allConfigFiles.map(cf => path.resolve(cf)),
         ...this.#tsConfig.parsedCmdLine ? this.#tsConfig.parsedCmdLine.fileNames.map(f => path.normalize(f)) : []
      ];

   }


   public override processFiles(files: FileInfo[]): ProcessedInfo {

      const diag: DiagEntry[] = [];

      const configs = files.filter(f => f.srcFileName.endsWith(".json"));
      const sources = files.filter(f => f.srcFileName.endsWith(".ts"));

      if (configs.length > 0) {

         let updateConfigFile = false;

         for (const config of configs) {

            if (this.#configHashes[config.srcFileName] === undefined) {
               updateConfigFile = true;
               this.#configHashes[config.srcFileName] = config.srcFileHash!;
            }

            if (this.#configHashes[config.srcFileName] !== config.srcFileHash) {
               updateConfigFile = true;
               this.#configHashes[config.srcFileName] = config.srcFileHash!;
            }

         }

         if (updateConfigFile) {
            const configFile = this.context.fbConfig.pluginConfig[this.context.fbConfig.env][this.id];
            this.context.diag.info(this.constructor.name, "init", 0, `      TypeScript config file '${configFile} will be used'`);
            this.#tsCompiler.updateConfigFile(configFile);
         }

      }

      this.#tsCompiler.compilerHost?.updateFileInfo(sources);

      const results = this.#tsCompiler.compile(true, sources.map(s => s.srcFileName));

      results.preEmitDiagnostics.forEach(
         preEmitDiagnostics => {

            if (preEmitDiagnostics.category < 2) {

               const lineChar: LineAndCharacter = preEmitDiagnostics.file ?
                  preEmitDiagnostics.start !== undefined ? preEmitDiagnostics.file.getLineAndCharacterOfPosition(preEmitDiagnostics.start) : { line: -1, character: -1 }
                  :
                  { line: -1, character: -1 };

               diag.push({
                  fileName: preEmitDiagnostics.file ? preEmitDiagnostics.file.fileName : "Unknown",
                  line: lineChar.line >= 0 ? lineChar.line + 1 : lineChar.line,
                  column: lineChar.character >= 0 ? lineChar.character + 1 : lineChar.character,
                  pos: preEmitDiagnostics.start ? preEmitDiagnostics.start : -1,
                  message: typeof(preEmitDiagnostics.messageText) === "string" ? preEmitDiagnostics.messageText : "",
                  code: preEmitDiagnostics.code ? `${preEmitDiagnostics.code}` : "",
                  snip: "",
                  type: preEmitDiagnostics.category === 0 ? "warning" : "error"
               });

            }

         }
      );



      return {
         files: sources,
         diag
      };

   }


}