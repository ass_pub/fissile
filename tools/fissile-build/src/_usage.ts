/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2021 Atom Software Studios. All rights reserved.

Inspired by:

   Original Library
      - Copyright (c) Marak Squires

   Additional Functionality
      - Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

"use strict";

import colors from "./lib/diag/colors.js";

const usg = colors.white(
// eslint-disable-next-line indent
`${colors.white(colors.underline("Info:"))}

Fissile build task runner is used to build a Fissile application servable
directly by a web server. Please note that it is not using any bundler.
ES6 Modules are supposed to be used only and HTTP2 server supporting the
PUSH functionality should be used in order to a maximum network performance
is achieved.

${colors.white(colors.underline("Usage:"))}

   To run build tools, fissile-build.mjs file containing the configuration
   should be used. Please reffer to the default fissile-build.mjs for details.
   This file is located in the fissile repository in the tools folder or see
   an example on the end of this help.

   node ./fissile-build.mjs -h -d [level] -e [environment] -c -b -w

   ${colors.white(colors.underline("General options:"))}

   -h, --help       shows this help page

   ${colors.white(colors.underline("Logging options:"))}

   -d level         set logging verbosity
   --debug level    higher number = higher log verbostiy
                    if no level is specified, 0 will be used

   -n, --no-colors  disables colored console logging output

   ${colors.white(colors.underline("Configuration options:"))}

   -e, --env        specifies for what environment the selected task will be
                    performed.  Allowed values are 'dev' and 'prod', defaults
                    the value loaded from the config file or 'dev'.

   ${colors.white(colors.underline("Tasks:"))}

   Only single task is allowed. An error will be reported if multiple tasks are
   specified.

   -c, --clean      runs the clean task. Cleans the configured target folder
                    based on the configured environment.

   -b, --build      runs the build task. Starts configured pipelines to build
                    the project to the configured folder and based on the
                    configured environment

   -w, --watch      runs the watch task. Same as build, but when initial build
                    finishes the tool is switched to the watch mode for
                    continuous build.

${colors.white(colors.underline("Built-in plugins:"))}

For details about plugin configuration refer to the Fissile Tools
documentation. All internal plugins supports pipelining, map files (if
possible) and incremental builds.

   ESLint           Wrapper for the ESLint code checker can be used to watch
                    if the coding style is followed.

   TypeScript       Wrapper for TypeScript transpiler allowing development of
                    the application code in the TypeScript language

   Uglify           Wrapper for UglifyJS 3 allows to eliminate dead code,
                    minify and optimize the result code for faster loading
                    times.

   TreeShaker       Checks for unused exports and imports and removes them
                    if possible. Also, it allows minification of export and
                    import names.

   TplChecker       Validates Fissile Templates

   Copy             Copies the configured directories to configured target
                    locations

   DocGen           Documentation generator for TypeScript files

   Emit             File emitter

${colors.white(colors.underline("Custom plugins:"))}

   To use custom plugins, please refer to Fissile Tools documentation.

`);

export default usg;