/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as http from "http";

import express from "express";
import { Express, Request, Response, NextFunction } from "express";


import FissileDevServerContext from "../FissileDevServerContext";

export class Server {

   private initialized: boolean;
   private context: FissileDevServerContext;

   private express: Express;
   private httpServer?: http.Server;

   constructor(context: FissileDevServerContext) {

      this.context = context;
      this.initialized = false;

      this.express = express();

   }


   public init() {

      this.context.diag.info(this.constructor.name, "init", 0, "Initializing development web server");

      this.express.use(
         (req: Request, res: Response, next: NextFunction) => this.requestLogger(req, res, next),
         (req: Request, res: Response, next: NextFunction) => this.requestFilter(req, res, next),
         (req: Request, res: Response, next: NextFunction) => this.serveProdFile(req, res, next),
         (req: Request, res: Response, next: NextFunction) => this.serveSrcFile(req, res, next),
         (req: Request, res: Response, next: NextFunction) => this.serveSpa(req, res, next)
      );

      this.httpServer = http.createServer(this.express);

      this.initialized = true;

      this.context.diag.info(this.constructor.name, "init", 0, "Development web server initialized, ready to start");

   }


   public start() {

      if (!this.initialized) {
         this.context.diag.error(this.constructor.name, "start", "Server must be initialized prior the start method is called");
         return;
      }

      this.context.diag.info(this.constructor.name, "start", 0, `Starting development server - listening on (${this.context.config.host}:${this.context.config.port})`);

      try {

         // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
         const server = this.httpServer!.listen(
            this.context.config.port,
            this.context.config.host
         );

         server.on("error", (e: Error) => {
            this.context.diag.error(this.constructor.name, "start", `Server error: ${e.message}`);
         });

      } catch (e) {
         this.context.diag.error(this.constructor.name, "start", "Failed to start development server.");
      }

   }

   private async requestLogger(req: Request, res: Response, next: NextFunction) {

      this.context.diag.info(this.constructor.name, "requestLogger", 0, `REQUEST: (from ${req.socket.remoteAddress}:${req.socket.remotePort}) ${req.method} ${req.path}`);
      if (req.method !== "GET") {
         // stop processing
      } else {
         next();
      }

   }

   private async requestFilter(req: Request, res: Response, next: NextFunction) {

      if (req.method !== "GET") {
         this.context.diag.warn(this.constructor.name, "requestFilter", `REQUEST: (from ${req.socket.remoteAddress}:${req.socket.remotePort}) 405 Method Not Allowed`);
         res.send(405);
         return;
      }


      next();
   }

   private async serveProdFile(req: Request, res: Response, next: NextFunction) {

      const prodFilePath = path.resolve(this.context.config.rootDir, "." + req.path);

      if (prodFilePath.startsWith(path.resolve(this.context.config.rootDir))) {
         this.serveFile(req, res, next, prodFilePath);
      } else {
         next();
      }

   }

   private async serveSrcFile(req: Request, res: Response, next: NextFunction) {

      const srcFilePath =  path.resolve(this.context.config.srcDir, ".." + req.path);

      if (srcFilePath.startsWith(path.resolve(this.context.config.srcDir))) {
         this.serveFile(req, res, next, srcFilePath);
      } else {
         next();
      }

   }

   private async serveSpa(req: Request, res: Response, next: NextFunction) {

      const indexPath = path.resolve(this.context.config.rootDir, this.context.config.spaIndex);
      this.serveFile(req, res, next, indexPath);


   }

   private async serveFile(req: Request, res: Response, next: NextFunction, fileName: string) {

      try {

         const stat = await fs.promises.stat(fileName);
         if (!stat.isFile) { next(); return; }

         const file = await fs.promises.readFile(fileName);

         this.context.diag.info(this.constructor.name, "serveFile", 0, `REQUEST: (from ${req.socket.remoteAddress}:${req.socket.remotePort}) Sending file ${fileName}`);

         res.type(path.extname(fileName));
         res.send(file);

      } catch(e) {
         next();
         return;
      }

   }

}