/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Server } from "./server/Server.js";
import { FissileDevServerConfig } from "./config/FissileDevServerConfig";
import FissileDevServerContext from "./FissileDevServerContext.js";

import * as diag from "./lib/diag/index.js";
import diagHeader from "./_header.js";

const context: FissileDevServerContext = <any>{};

function setupContext(config: FissileDevServerConfig) {

   context.config = config;
   context.diag = {
      info: diag.info,
      warn: diag.warn,
      error: diag.error
   };

}


export default async function main(config: FissileDevServerConfig): Promise<void> {

   setupContext(config);

   context.diag.info(diagHeader);

   const server = new Server(context);

   server.init();
   server.start();

}