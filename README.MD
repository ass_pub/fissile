# Fissile Framework

---

Copyright 2016-2021&copy; Atom Software Studios & Contributors<br />
Released under the MIT license

---

# Introduction

TBD. (Documentation is not complete and is subject of change)

There were two main reasons, why the framework was developed and both are very old.

The **first** one was to try to find a way to completely avoid mixing of the code representing the view (in this case HTML with CSS) with any other code allowing the view code manipulation before it is rendered (so called templating language or extensions). For many years, there exists bunch of scripting or pseudo scripting templating languages allowing the template code manipulation and new and new still pops up. These languages were introduced to make it possible conditionally render template parts, repeating of some template parts, replacing of template parts and many other manipulations based on the view state data. Examples of such languages can be ASP.NET, JSF, PHP, Smarty and basically, all modern major and widely used JavaScript frameworks and templating libraries, such as React, Angular, Vue, Aurelia, Handlebars and many others follows the same way, so the source code of the template (HTML syntax) is extended with a HTML non-standard syntax and/or elements and attributes allowing to perform mentioned operation over the template. Although this works well there are some disadvantages of this approach. Some of them can be complexity of the final code, complexity of the build tool chain, harder maintenance, impossibility to develop templates by designers without knowledge of some templating language or worse the whole framework OR need of rewriting of templates by developers, less or more steep learning curve of templating languages and frameworks, harder possibility to separate development team roles and responsibilities, hard implementation of template authoring tools, harder possibility to replace reusable components by others. By harder is in this case meant not impossible, but usually more expensive and more time consuming.

The **second** reason was to try to find a simple, standards compliant and ideally performant way, how to change look of components without direct modification of class names or styles of rendered html tags, tags being rendered or tags being changing their look based on the view state change. It is currently common practice to manipulate the list of classes applied to particular HTML element or to change the style of the element directly. Again, as in previous case, this usually works well, but it is required to change the application code when some class names are modified and/or look is redefined. Also, direct modification of the element styles can lead to unpredicted results when style sheets are changed. It is more complicated to implement theming for example and also, this techniques usually prevents possibility of simple restyling of reused components (i.e. changing of the button color from silver to green) as it is necessary to know, what is defined with stylesheets and what is changed dynamically using the JavaScript in runtime.

As the first idea is fully integrated to the framework and the framework is basically built on it, the second idea exploration and research in the end led just to list of recommendations of what can be achieved and how. These recommendations will be mentioned later and are used in all examples.

Most of the functionality was developed already during first iterations of testing and designing the framework. Currently, the complete framework (previously named Ajs) is being dramatically simplified by employing latest API versions and features supported by major browsers.

An example of the application running on the first, unoptimized, work-in-progress, research and learning (and currently deprecated) version of the framework can be seen [here](https://ajsfw.azurewebsites.net)).

# Pre-release notes

- Fissile is currently developed in Visual Studio Code and all IDE configurations integrations are currently configured and available only for it

- Currently, only Chrome browser is supported for debugging and it is also configured in the launch.json file under the .vscode directory. You need to change this if another browser is preferred. Also, hot module reload (or another auto-reload like functionality) is not currently supported, but it is planned to be implemented as soon as possible.

- Currently, the framework, basic app project and the toolset will be distributed all together and only using the GIT until the decision will be changed (if it ever happens). Although this is probably not optimal it allows simpler development of all features as it is possible to simply monitor all tools and simply jump to the functionality which generated the log message. However, please note, that each tool is developed separately in separate instance of Visual Code and if you would like to play with it is recommended to do the same.

- No module bundling is NOT currently supported (and probably never will be) as only ES6 (ESM) modules are in use and the
preferred method for the application delivery will be HTTP2 employing the PUSH method. For this purposes additional tooling (i.e. for generating dependencies of all JavaScript, HTML and CSS files will be developed). This will be implemented similarly to source map files - for each file there will be a .dep text file containing references to all dependencies so these dependencies can be pushed to the client employing the HTTP2 push method. Of course, application servers have to support this functionality or it must be implemented as a plugin. Local development server will support will this functionality and plugin for the NodeJS Express server will be released soon.

- Currently, the configuration of the toolset is limited and the project folder structure should follow the documentation (or kept as is until better documentation will be released)

- App, framework and tooling is tested on latest versions of major browsers only (Edge, Chrome, Chrome Mobile, FireFox, Safari, Safari Mobile), latest LTS version of NodeJS and with latest versions of 3rd party libraries. Its known that some features are unfinished, buggy or not working in some browsers and/or platforms.

- Tools are currently tested mainly Windows platform and time to time on Linux. Mac platform will be tested later, but as it is mostly compatible with Linux platform it should be possible to run tools over there too.

- Functionality is currently very limited

- 3rd party modules can't be currently imported to the final application code

- Performance of the code is not optimized yet

- Documentation is mostly missing

---

# Features

Please note the list of features is currently very limited (until the first complete release).

For details about specific features please refer to the documentation (once it will be available - till that time please see the source code.)

#### Framework

- Asynchronous DI Container
- MVVM
   - Templating (View)
      - completely separating views from the code (no more code or special language in HTML)
      - Mapping of the view state object tree directly to view HTML templates (optionally with code behind) including updating
      - Using templates directly from templates (without need of any coding)
      - View "code behind" directly in the template (but separated from the HTML code)
      - View "code behind"
      - Template stylesheets
   - ViewModel helpers allowing to build and update the view state efficiently
   - Support for custom service injection (data or data model providers)

Features to be implemented:

- Templating
   - Performance of array data updates
   - Multiple templates in single template file
   - DOM to View "code behind" event handling
   - DOM to EventBus event translation
- MessageBus (RXJS based)
   - supporting splitting modules to workers and keeping main thread free just for rendering
   - supporting server communication
   - supporting global application state management
- Routing (to ViewModels)
- Navigation (separated from routing)
- Application modules (async loaded ESM + resources)
-
- i18 Language support (Template translations, ViewState translations)
- Data validation

#### Tools

##### fissile-build

- Configurable target environment (Dev/Prod)
- Configurable task pipelines (build, watch, clean)
- Incremental build mode (for both, Dev/Prod - both can run simultaneously)
- File processing pipelines with plugins
- Custom (3rd. party) plugin support (currently limited)
- Configurable plugins
   - Folder copy (copy resources from source folders to the www root)
   - TypeScript build

Features to be implemented:

- CommonJS to ESM dependency module converter
- Module minification & dead code elimination (Uglify / Tree shaking)
- Plugins for CSS processors (sass, less)
- TypeScript documentation generator with custom pages support

##### fissile-dev-server

- Serving SPA application
- Serving source files from another than www root directory
- Serving static application resources

Features to be implemented:

- Hot module and resource reloading

- Visual template builder
- Visual framework (basic and advanced components such as input, layout, navbars, menus)

##### fissile-dev-server

Used to initialize the project after the repository cloning. It just installs required dependencies and compiles the tools in order to be possible to use them.

---

# Installation

#### Prerequisites
- See pre-release notes
- NodeJS with NPM installed
- Currently tested only with:
   - NodeJS version 16.13.0
   - NPM version 8.1.0
   - but should be working even with older and newer Node/NPM versions without problems

#### Installation

Clone the repository:

```
git clone https://gitlab.com/ass_pub/fissile.git
```

#### Project initialization

Prior the application can be started, dependencies must be installed and
tools TypeScript files needs to be transpiled to executable .js files.

To install all dependencies for the project and build build-tools run:

```
cd ./fissile
npm run fissile-init
```

or

```
cd ./fissile
node ./tools/fissile-init.mjs
```

#### First run

It is possible to use NPM or VSCode to build and run the application. To use VSCode, please open the folder to which you cloned the repository.

##### NPM way

Make sure the CWD is root of the Fissile project folder

To build files and setup the **dist** folder structure run:
```
npm run build
```

To start development server run:
```
npm run serve
```

This starts the development server listening on **localhost:8080**. The host and port options can be configured in the *fissile-dev-server.mjs* script located in the project root folder.

As browser is currently not executed automatically, just open it manually and navigate to [http://localhost:8080](http://localhost:8080).

##### VSCode way

1) Open project folder in VS code

2) Run build task "Fissile - Build"
```
   Menu -> Terminal -> Run Task -> Fissile - Build

   or

   Menu -> Terminal -> Run Task -> Show All Tasks -> Fissile - Build
```

3) Start the code with debugging
```
   Menu -> Run -> Start Debugging
```

---

# Project folder structure

Please note the folder structure will be probably changed during a development of the framework and toolset.

#### Directories located in the root project directory

Directory name | Description
---------------|------------
.vscode        | VSCode IDE Settings, tasks, launch configurations used during the development of the project and recommended for the application development
build          | Contains the built application ready to be served by the application server. There can be two folders under it, *dev* and *prod* containing appropriate builds. The **fissile-dev-server** tool is (by default) using the build/dev folder as a www root.
config         | Will be renamed to build-config, to be clear. Contains configuration of toolset used to build the final application. For details please refer to the build documentation once it will be available.
doc            | Will contain the project documentation once the **doc-gen** build plugin will be migrated from Ats and updated accordingly. **fissile-doc-viewer** tool will be developed soon and will (by default) serve the documentation from this folder.
node_modules   | Standard folder where dependencies are installed (currently only dev dependencies, runtime 3rd dependencies are not supported yet)
pub            | Public folder contain the static files of the application. It basically represents a www root and is directly copied to the appropriate build folder during the application build.
src            | Application source code including the Fissile framework code. Fissile framework is located in the **_fissile** folder. The rest of files in the folder represents the example application developed with the fissile framework.
templates      | Will be renamed to Views soon. Contain HTML templates including the styling. For details refer to the Fissile Development Guide once the documentation will be released.
tools           | Tools folder contain all tools helpful to build and debug the Fissile application. For details refer to the **Available Tools** section bellow.


#### Files located in the root project directory

File name      | Description
---------------|------------
.eslintignore  | Standard ESLint ignore file. Used to ignore everything under **build** and **tools** folders.
.eslintrc.json | Standard ESLint configuration file used by VSCode IDE. It just extends another ESLint configuration file located in the **config** folder. The file located in this folder is used by both, VSCode IDE and build tools.
.gitignore     | Standard GIT ignore file. Ignores **node_modules**, build target folders and bin folders to which tools are transpiled.
.npmrc         | Standard NPM configuration file. Currently used just to prevent creation of the package.json.lock file as it is not needed before first release of the framework and tooling
LICENSE        | Contain license for the Fissile framework, including licenses to all used tools and modules
package.json   | Standard NPM package file describing the project. Currently, there are no runtime dependencies (as not currently supported), just development ones. **Content of this file has to be modified in order to set custom project settings, such as name, license and so on**.
README.MD      | This file
tsconfig.json  | Standard TypeScript configuration file used by VSCode IDE. It is extended with the development configuration located in the **config** folder.


---

# Available Tools

All tools except the init are integrated with the VSCode IDE in form of tasks.

#### fissile-init

Script used to initialize the project after cloning. This can also be done manually, but
it is much easier to use this script.

a) Installs both, dev and prod dependencies for the project itself
b) Installs both, dev and prod dependencies for all tools mentioned in this paragraph
c) Transpiles the TypeScript code of all tools to executable form

This script basically runs only the following commands under each tool folder:

```
npm install
tsc
```

And the following command under the project root folder:

```
npm install
```

In future it will allow to set package.json properties in the same way as the
npm init is doing it in order to be possible to simply modify basic properties of the NPM package being developed.

#### fissile-build

Tool to build all TypeScript files and place all application resources to the
dist folder.

The (limited) tool configuration can be done in fissile-build.mjs file.

For details refer to the tools/fissile-build/README.md file (or the source code).

#### fissile-dev-server

Simple development server allowing debugging the application locally.

The (limited) tool configuration can be done in fissile-dev-server.mjs file.

For details refer to the tools/fissile-dev-server/README.md file (or the source code).

---

# Contribution

Please note it is probably too early to contribute, but if you have any ideas or enough of free time for testing of framework features or tooling and reporting issues, suggestions or ideas, you are very welcome to.

---

# License

All code, including the tooling is released under MIT License.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
