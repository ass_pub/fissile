/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { inject } from "../lib/dic/inject.decorator.js";

import { DIC } from "../lib/dic/DIC.js";
import  { RenderQueue } from "./RenderQueue.js";

import { IIDependentInjectable } from "../lib/dic/Injectable.js";
import { createIndexedUniqueId } from "../lib/uniqueId.js";
import { getAllPropNames } from "../lib/getAllPropNames.js";

type RemoveFirst<T extends unknown[]> = T extends [any, ...infer R] ? R : T;


export type ViewModelCtor<T extends ViewModel = ViewModel> = new (ccArgs: BaseViewModelCreationArgs, ...args: any[]) => T;


export interface BaseViewModelCreationArgs {
   dic: DIC,
   renderQueue: RenderQueue,
   templateName: string,
   parent: ViewModel
}


export interface ChildViewModelCreationInfo<T extends typeof ViewModel = typeof ViewModel> {
   ctor: ViewModelCtor<InstanceType<T>>;
   args?: RemoveFirst<ConstructorParameters<T>>;
}


export const baseViewModelCreationArgs = {
   dic: DIC,
   renderQueue: RenderQueue,
   templateName: "TemplateName",
   parent: IIDependentInjectable
};


@inject(baseViewModelCreationArgs)
export class ViewModel {

   #isInitializing: boolean;

   #dic: DIC;
   #renderQueue: RenderQueue;

   #rootRenderTarget = "body";
   get rootRenderTarget(): string { return this.#rootRenderTarget; }

   #id: string;
   get id(): string { return this.#id; }

   #parent: ViewModel;
   get parent(): ViewModel { return this.#parent; }

   #templateName: string;
   get templateName(): string { return this.#templateName; }


   constructor(ccArgs: BaseViewModelCreationArgs) {

      this.#isInitializing = true;

      this.#dic = ccArgs.dic;
      this.#renderQueue = ccArgs.renderQueue;
      this.#templateName = ccArgs.templateName;
      this.#parent = ccArgs.parent;
      this.#id = createIndexedUniqueId("C");

   }


   async initAsync() {

      this.#rootRenderTarget = await this.#dic.resolveAsync("RootRenderTarget");
      await this._initAsync();

      this.#isInitializing = false;
      this._render();

   }


   protected async _initAsync(): Promise<void> {
      // Has to be overriden
   }


   protected async _createViewModel<T extends ViewModelCtor>(ctor: T, ...args: any[]): Promise<InstanceType<T>>;
   protected async _createViewModel<T extends ViewModelCtor>(info: ChildViewModelCreationInfo<T>): Promise<InstanceType<T>>;
   protected async _createViewModel<T extends ViewModelCtor>(ctorOrInfo: T | ChildViewModelCreationInfo<T>, ...args: any[]): Promise<InstanceType<T>> {
      const ctor = typeof ctorOrInfo === "function" || ctorOrInfo === undefined ? ctorOrInfo : ctorOrInfo.ctor;
      const injections = typeof ctorOrInfo === "function" || ctorOrInfo === undefined ? args : ctorOrInfo.args;
      return this.#dic.resolveAsync<InstanceType<T>>(ctor, this, injections);
   }


   protected async _render(): Promise<void> {

      if (this.#isInitializing) return;

      // find the view model in the document
      const slot = document.getElementById(this.#id);

      // if the view model was found, update it
      if (slot) {

         const viewState = this.#buildViewState();

         const vs: any = {};
         vs[`#${this.#id}`] = viewState;

         this.#renderQueue.queueViewModel(this, vs);

         return;


      }

      // otherwise, render parent if exist
      if (this.parent) {
         this.parent._render();
         return;
      }

      // otherwise, if therre is no parent, render view model to the root element
      const viewState = this.#buildViewState();

      const vs: any = {};
      vs[this.#rootRenderTarget] = viewState;

      this.#renderQueue.queueViewModel(this, vs);

   }


   #buildViewState(): { viewState: any, viewModels: ViewModel[] } {

      const propNames = getAllPropNames(this);

      const viewModelViewState: any = {};
      viewModelViewState[this.#templateName] = {};

      const viewState = viewModelViewState[this.#templateName];

      const self = <any>this;

      viewState.id = this.id;

      for (const propName of propNames) {

         if (self[propName] instanceof ViewModel) {

            viewState[propName] = (<ViewModel>self[propName]).#buildViewState();

         } else if (self[propName] instanceof Array) {

            viewState[propName] = [];
            for(const item of self[propName]) {

               if (item instanceof ViewModel) {
                  viewState[propName].push((<ViewModel>item).#buildViewState());
               } else {
                  viewState[propName].push(item);
               }

            }

         } else {

            viewState[propName] = self[propName];

         }

      }

      return viewModelViewState;

   }

}