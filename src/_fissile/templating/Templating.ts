/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { inject } from "../lib/dic/inject.decorator.js";
import { KeyValue } from "../lib/@types/KeyValue";
import { createIndexedUniqueId } from "../lib/uniqueId.js";
import { DOMPatcher } from "./DOMPatcher.js";
import { htmlTags } from "./html5.js";
import { ViewCtor } from "./View.js";

export class TemplatingException extends Error {}

@inject("Templating-templatesPath", DOMPatcher)
export class Templating {

   #loadedTemplates: KeyValue<{ template: HTMLTemplateElement, viewCtor?: ViewCtor }>;
   #templatesPath: string;
   #domPatcher: DOMPatcher;

   constructor(
      templatesPath: string,
      domPatcher: DOMPatcher
   ) {
      this.#templatesPath = templatesPath;
      this.#domPatcher = domPatcher;
      this.#loadedTemplates = {};
   }


   async render(viewState: any): Promise<void> {

      // viewstate has to have a single property of type array
      const keys = Object.keys(viewState);
      if (keys.length !== 1) throw new TemplatingException("View state must specify the render target!");

      const selector = keys[0];
      if (
         typeof viewState !== "object" ||
         viewState[selector] instanceof Array ||
         Object.keys(viewState[selector]).length !== 1
      ) throw new TemplatingException("Template object expected!");

      // root selector is defined in the view state, so it must be found in the document
      const nodes = document.querySelectorAll(selector);
      if (nodes.length !== 1) throw new TemplatingException("Only single taget element was expected to be found.");
      const target = nodes.item(0);

      const templateName = Object.keys(viewState[selector])[0];
      const templateViewState = viewState[selector][templateName];

      const templatesNode: Node = await this.#renderTemplate(templateName, templateViewState);

      this.#domPatcher.patch(target, templatesNode);

   }


   async #getTemplate(templateName: string): Promise<HTMLTemplateElement> {

      const tplName = templateName.replace(/\./, "/");

      if (Object.prototype.hasOwnProperty.call(this.#loadedTemplates, tplName)) {
         return this.#loadedTemplates[tplName].template;
      }

      const response = await fetch(`${this.#templatesPath}/${tplName}.html`);
      if (response.status !== 200) throw new TemplatingException(response.statusText);
      const templateData = await response.text();

      return await this.#parseTemplate(tplName, templateData);

   }


   async #parseTemplate(templateName: string, templateData: string): Promise<HTMLTemplateElement> {

      const { preprocessedTemplate, viewCode } = this.#preprocessTemplate(templateData);

      const template = document.createElement("template");
      template.innerHTML = preprocessedTemplate.trim();

      const viewCtor: ViewCtor | undefined = viewCode !== "" ? await this.#buildViewClass(viewCode) : undefined;

      if (viewCtor) {
         console.log(viewCtor);
         const a = new viewCtor();
         a.test();
         if (a.onConnect) a.onConnect();
         if (a.onUpdate) a.onUpdate();
         if (a.onDisconnect) a.onDisconnect();
      }

      this.#loadedTemplates[templateName] = {
         template,
         viewCtor
      };

      return template;

   }


   #preprocessTemplate(templateData: string): { preprocessedTemplate: string, viewCode: string } {

      const withoutUnpairTags = this.#convertUnpairTags(templateData);

      const { templateWithoutScripts: withoutScripts, scriptString } = this.#extractScript(withoutUnpairTags);

      return {
         preprocessedTemplate: withoutScripts,
         viewCode: scriptString
      };

   }


   /**
    * Replaces unpair tags with paired ones
    * @param templateData HTML Template string
    * @returns Updated HTML template with all unpair tags converted to pair ones
    */
   #convertUnpairTags(templateData: string): string {

      let result = templateData;

      const unpairTags = /<.*\/>/gm.exec(result);
      if (unpairTags instanceof Array) {
         for (const unpairTag of unpairTags) {
            const rx = /<([^\s|/|>]*)/.exec(unpairTag);
            const tagName = rx ? rx[1] : "";
            if (tagName === "" || htmlTags.indexOf(tagName) !== -1) continue;
            const fixedUnpairTag = unpairTag.replace(/\s*\/>/, ">");
            result = result.replace(unpairTag, fixedUnpairTag + `</${tagName}>`);
         }
      }

      return result;

   }


   /**
    * Extracts and removes scripts from the template code (only first script tag content is returned)
    * @param templateData HTML Template string
    * @returns Updated HTML template and
    */
   #extractScript(templateData: string): { templateWithoutScripts: string, scriptString: string } {

      // extract and remove view scripts

      const scriptRegex = /<script((.|\s)*?)>()((.|\s)*?)<\/script>/gm;

      let i = 0;
      let script: RegExpExecArray | null;
      let resultScript = "";

      while (null != (script = scriptRegex.exec(templateData))) {
         if (i === 0) {
            resultScript = script[4];
         } else {
            console.warn("Only single script tag is currently supported in the template!");
         }
         i++;
      }

      return {
         templateWithoutScripts: templateData.replace(scriptRegex, ""),
         scriptString: resultScript
      };

   }

   /**
    * Builds a view class from the passed script fragment
    * @param script Script fragment speifying a class body
    */
   async #buildViewClass(script: string): Promise<ViewCtor> {

      const classDef = await (Function(`

         return new Promise(


            async (resolve, reject) => {

               try {

                  const module = await import("./View.js");

                  let View = class Test extends module.View {
                     ${script}
                  }

                  resolve(View);

               } catch (e) {
                  reject(e);
               }
            }

         )

      `)());

      return classDef;

   }



   async #renderTemplate(templateName: string, viewState: any, templateWithoutViewModel = false): Promise<Node> {

      let template: HTMLTemplateElement;

      try {
         template = await this.#getTemplate(templateName);
      } catch(e) {
         throw new TemplatingException(`Failed to render template '${templateName}', can't get the template code: ${e}`);
      }

      const slot = document.createElement("slot");
      slot.setAttribute("data-type", "template");
      slot.setAttribute("data-template", templateName);

      if (!templateWithoutViewModel) {
         slot.setAttribute("id", viewState.id);
      }

      const templateClone = template.content.cloneNode(true);
      slot.appendChild(templateClone);
      await this.#applyTemplate(slot, viewState);

      return slot;

   }


   async #applyTemplate(slot: HTMLSlotElement, viewState: any): Promise<void> {

      for(let i = 0; i < slot.childNodes.length; i++) {
         await this.#processNode(slot.childNodes.item(i), viewState);
      }

   }


   async #processNode(node: Node, viewState: any): Promise<void> {

      switch (node.nodeType) {

         case node.ELEMENT_NODE: {

            const nodeName = node.nodeName.toLowerCase();

            if (htmlTags.indexOf(nodeName) === -1) {
               await this.#processCustomElement(nodeName, node, viewState);
               break;
            }

            if (node.nodeName.toLowerCase() === "slot") {
               await this.#processSlotElement(node, viewState);
               break;
            }

            await this.#processElement(<Element>node, viewState);

            break;
         }

         case node.ATTRIBUTE_NODE:
            break;

         case node.TEXT_NODE:
            node.textContent = node.textContent ? this.#applyViewStateToTextNode(node.textContent, viewState) : "";
            break;

         case node.CDATA_SECTION_NODE:
            break;

         case node.PROCESSING_INSTRUCTION_NODE:
            break;

         case node.COMMENT_NODE:
            break;

         case node.DOCUMENT_NODE:
            break;

         case node.DOCUMENT_TYPE_NODE:
            break;


      }

   }


   async #processCustomElement(nodeName: string, node: Node, viewState: any): Promise<void> {

      const id: string | null = (<Element>node).getAttribute("id");
      const vs = id !== null && Object.prototype.hasOwnProperty.call(viewState, id) ? viewState[id] : viewState;

      const templateNode = await this.#renderTemplate(nodeName, vs, id === null);

      if (node && node.parentNode && templateNode) {
         node.parentNode.insertBefore(templateNode, node);
         node.parentNode.removeChild(node);
      }

   }


   async #processSlotElement(node: Node, viewState: any): Promise<void> {

      const id = (<Element>node).getAttribute("id");

      if (!id || !Object.prototype.hasOwnProperty.call(viewState, id) || viewState[id] === undefined) return;

      const vs = viewState[id];

      if (typeof vs !== "object") throw new TemplatingException("Invalid state for slot element (object or array of templates expected)");

      if (vs instanceof Array) {
         for (const tplvs of vs) {
            if (Object.keys(tplvs).length !== 1) throw new TemplatingException("Invalid state for slot element (object is expected to have a single node)");
            await this.#processSlotTemplate(node, tplvs);
         }

      } else {
         if (Object.keys(vs).length !== 1) throw new TemplatingException("Invalid state for slot element (object is expected to have a single node)");
         await this.#processSlotTemplate(node, vs);
      }

   }

   async #processSlotTemplate(node: Node, viewState: any): Promise<void> {

      const templateName = Object.keys(viewState)[0];
      const templateState = viewState[templateName];

      const nodesToRemove: Node[] = [];

      for (let i = 0; i < node.childNodes.length; i++) {

         const innerNode = node.childNodes.item(i);

         if (innerNode.nodeType !== node.ELEMENT_NODE) continue;

         const target = (<Element>innerNode).getAttribute("target");
         if (!target) continue;

         const nodeName = innerNode.nodeName.toLowerCase().replace(/\./g, "/");
         if (htmlTags.indexOf(nodeName) !== -1) continue;

         nodesToRemove.push(innerNode);

         if (templateState[target] === undefined) {
            templateState[target] = {} ;
            templateState[target][nodeName] = {};
            templateState[target][nodeName].id = createIndexedUniqueId("C");
         }

      }

      for (const ntr of nodesToRemove) node.removeChild(ntr);

      const templateNode = await this.#renderTemplate(templateName, templateState);
      node.appendChild(templateNode);

   }


   async #processElement(element: Element, viewState: any): Promise<void> {

      for (let i = 0; i < element.attributes.length; i++) {
         const item = element.attributes.item(i);
         if (item) item.value = this.#applyViewStateToTextNode(item.value, viewState);
      }

      if (element.hasAttribute("id")) {

         const id = <string>element.getAttribute("id");

         if (viewState[id]) {

            if (viewState[id] instanceof Array) {

               if (viewState[id].length === 0) {

                  if (element.parentElement) element.parentElement.removeChild(element);
                  return;

               } else {

                  const cloned = element.cloneNode(true);
                  (<Element>cloned).attributes.removeNamedItem("id");
                  const slot = document.createElement("slot");
                  slot.setAttribute("data-type", "array");
                  slot.setAttribute("id", id);

                  if (element.parentNode) {
                     element.parentNode.insertBefore(slot, element);
                     element.parentNode.removeChild(element);
                  }

                  for(let i = 0; i < viewState[id].length; i++) {
                     const ca = cloned.cloneNode(true);
                     await this.#processElement(<Element>ca, viewState[id][i]);
                     slot.appendChild(ca);
                  }
                  return;

               }

            }

            element.childNodes.forEach(n => this.#processNode(n, viewState[id]));
            return;

         } else {
            if (element.parentElement) element.parentElement.removeChild(element);
            return;
         }

      }

      for (let i = 0; i < element.childNodes.length; i++) {
         await this.#processNode(element.childNodes[i], viewState);
      }

   }

   #applyViewStateToTextNode(text: string, viewState: any): string {

      const toReplace: string[] = [];
      let textContent: string = text;

      const rx = /(?<=\{)[^}]*/g;
      let rxea: RegExpExecArray | null;

      // eslint-disable-next-line no-cond-assign
      while (rxea = rx.exec(textContent)) {
         toReplace.push(rxea[0]);
      }

      toReplace.push("{}");

      for (let i = 0; i < toReplace.length; i++) {

         if (viewState[toReplace[i]] !== undefined) {
            let replacement = viewState[toReplace[i]];
            if (typeof replacement === "boolean") replacement = replacement ? "true" : "false";
            textContent = textContent.replace("{" + toReplace[i] + "}", replacement);
         }
         if (toReplace[i] === "") {
            textContent = textContent.replace("{}", viewState);
         }

      }

      return textContent;

   }


}