/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { eventAttributes } from "./html5.js";

export class DOMPatcher {

   patch(target: Node, source: Node) {
      this.#updateNodeTree(source, target);
   }


   #updateNodeTree(src: Node, dst: Node) {

      for (let i = 0; i < src.childNodes.length; i++) {

         const srcNode = src.childNodes.item(i);

         if (srcNode.nodeType !== srcNode.ELEMENT_NODE && srcNode.nodeType !== srcNode.TEXT_NODE) {
            continue;
         }

         if (i >= dst.childNodes.length) {
            this.#appendNode(srcNode, dst);
            continue;
         }

         const dstNode = dst.childNodes.item(i);

         if (srcNode.nodeType !== dstNode.nodeType) {
            this.#replaceNode(srcNode, dstNode);
            continue;
         }

         if (srcNode.nodeType === srcNode.TEXT_NODE) {
            this.#updateNode(srcNode, dstNode);
            continue;
         }

         if (srcNode.nodeName !== dstNode.nodeName) {
            this.#replaceNode(srcNode, dstNode);
            continue;
         }

         this.#updateNode(srcNode, dstNode);

      }

      // skip child nodes removal in some special cases
      if (dst instanceof HTMLTextAreaElement) return;

      while (dst.childNodes.length > src.childNodes.length) {
         const node = dst.childNodes.item(src.childNodes.length);
         if (node.parentNode) node.parentNode.removeChild(node);
      }

   }


   #createNode(node: Node): Node | null {

      if (node.nodeType === node.ELEMENT_NODE) {
         const srcElement: Element = <Element>node;
         const element: Element = document.createElement(node.nodeName);

         this.#updateElementAttributes(srcElement, element);

         return element;
      }

      if (node.nodeType === node.TEXT_NODE && node.textContent !== null) {
         return document.createTextNode(this.#encodeText(node.textContent));
      }

      return null;
   }


   #appendNode(srcNode: Node, dst: Node): void {

      const node: Node | null = this.#createNode(srcNode);

      if (node === null) return;

      dst.appendChild(node);
      this.#updateNodeTree(srcNode, node);

   }


   /*#insertNode(srcNode: Node, dst: Node, index: number): void {
      const node: Node = this.#createNode(srcNode);

      if (node !== null) {
         const nodeAfter = dst.childNodes.item(index);
         nodeAfter.parentNode.insertBefore(node, nodeAfter);
      }

      this.#updateNodeTree(srcNode, node);
   }*/


   #replaceNode(srcNode: Node, dstNode: Node): void {

      const node: Node | null = this.#createNode(srcNode);

      if (node === null) return;

      if (dstNode.parentNode) dstNode.parentNode.replaceChild(node, dstNode);
      this.#updateNodeTree(srcNode, node);

   }


   #updateNode(srcNode: Node, dstNode: Node): void {

      if (srcNode.nodeType === srcNode.TEXT_NODE) {

         const enc = srcNode.nodeValue ? this.#encodeText(srcNode.nodeValue) : "";

         if (dstNode.parentNode && dstNode.parentNode.nodeName === "TEXTAREA") {
            (<any>dstNode.parentNode).value = enc;
         } else {
            if (dstNode.nodeValue !== enc) dstNode.nodeValue = enc;
         }

         return;

      }

      if (srcNode.nodeType === srcNode.ELEMENT_NODE) {

         const srcElement: Element = <Element>srcNode;
         const dstElement: Element = <Element>dstNode;

         /*
         if (srcElement.hasOwnProperty("customData")) {
               (<any>dstElement)["customData"] = srcElement.customData;
         }
         */

         this.#updateElementAttributes(srcElement, dstElement);
         this.#updatePropsFromAttrs(srcElement, dstElement);

         this.#updateNodeTree(srcNode, dstNode);

         return;
      }

   }


   #updateElementAttributes(srcElement: Element, dstElement: Element): void {

      // Non-existing attribute
      const attrsToRemove: string[] = [];

      for (let i = 0; i < dstElement.attributes.length; i++) {

         const item = <Attr>dstElement.attributes.item(i);

         if (!srcElement.hasAttribute(item.nodeName)) {
            attrsToRemove.push(item.nodeName);
         }

      }

      for (let i = 0; i < attrsToRemove.length; i++) {
         dstElement.removeAttribute(attrsToRemove[i]);
      }

      // Non-existing event listeners
      const listenersToRemove: any[] = [];

      if ((<any>dstElement)._ajsListeners) {

         // find listeners not matching src element attributes
         for (const listener of (<any>dstElement)._ajsListeners) {

            if (!srcElement.hasAttribute(listener.event)) {
               dstElement.removeEventListener(listener.event, listener.handler);
               listenersToRemove.push(listener);
            }

         }

         // remove listeners from _ajsListeners collection
         for (const listener of listenersToRemove) {

            const i = (<any>dstElement)._ajsListeners.indexOf(listener);
            (<any>dstElement)._ajsListeners.splice(i, 1);

         }

      }

      // attribute addition or update

      for (let i = 0; i < srcElement.attributes.length; i++) {

         // because of some special cases it is necessary to set the value of the element object instead of
         // changing the attribute itself (it is mainly the text area)

         const srcAttr = <Attr>srcElement.attributes.item(i);

         if (srcAttr.nodeName === "value") {

            // the file accepts just empty string so if the value is empty, clear selected files, otherwise skip this step
            if (dstElement instanceof HTMLInputElement && dstElement.type === "file") {
               if (srcAttr.nodeValue) dstElement.value = "";
               continue;
            }

            // the value is set only in case the dst element has the value property and it is different than the value to be set
            if (dstElement !== undefined && (<any>dstElement).value !== srcAttr.nodeValue) {
               (<any>dstElement).value = srcAttr.nodeValue;
            }

            continue;

         }

         // attribute update (no event listeners)
         if (dstElement.hasAttribute(srcAttr.nodeName)) {

            if (eventAttributes.indexOf(srcAttr.nodeName) === -1) {

               if (dstElement.getAttribute(srcAttr.nodeName) !== <string>srcAttr.nodeValue) {
                  dstElement.setAttribute(srcAttr.nodeName, <string>srcAttr.nodeValue);
               }

            }


         // attribute addition / event listener addition - update is not currently necessary as it will definitely point to the same handler
         } else {

            // event listeners
            if (eventAttributes.indexOf(srcAttr.nodeName) !== -1) {

               const evtName = srcAttr.nodeName.substr(2);

               // check if the listener was added or not
               let exist = false;
               if ((<any>dstElement)._ajsListeners) {

                  for (const listener of (<any>dstElement)._ajsListeners) {

                     if (listener.event === evtName) {
                        exist = true;
                        break;
                     }

                  }

               }

               // if not, add it
               if (!exist) {
                  /*
                  dstElement.addEventListener(
                        evtName,
                        srcElement.attributes.item(i).customData
                  );

                  (<any>dstElement)._ajsListeners = (<any>dstElement)._ajsListeners || [];
                  (<any>dstElement)._ajsListeners.push({
                        event: evtName,
                        handler: srcElement.attributes.item(i).customData
                  });
                  */
               }

            // attributes
            } else {
               dstElement.setAttribute(srcAttr.nodeName, <string>srcAttr.nodeValue);
            }

         }
      }

   }


   #updatePropsFromAttrs(srcElement: Element, dstElement: Element): void {

      if (dstElement instanceof HTMLOptionElement) {
         dstElement.selected = srcElement.hasAttribute("selected");
      }

   }


   #encodeText(text: string): string {
      let r = text;
      r = r.replace(/&nbsp;/gm, "\u00A0");
      r = r.replace(/&lt;/gm, "\u003C");
      r = r.replace(/&gt;/gm, "\u003E");
      return r;
   }



}