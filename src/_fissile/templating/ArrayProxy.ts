/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

interface ChangeDescriptor {

   property: string | symbol;
   value: any;

}


export function createArrayProxy<T = any>(...items: any[]): T[] {

   let changes: ChangeDescriptor[] = [];

   const array: Array<T> = new Array<T>(...items);

   const proxy = new Proxy(array, {

      set: (target: T[], property: string | symbol, value: any /*, receiver: any*/) => {
         changes.push({ property, value });
         return true;
      }

   });

   (<any>proxy).changes = () => {
      const ret = changes;
      changes = [];
      return ret;
   };

   (<any>proxy).isProxy = true;

   return proxy;

}
