/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Injectable } from "./Injectable";
import { DependencyInjectionContainer } from "./DepencencyInjectionContainer";
import { InjectableDescriptor } from "./InjectableDescriptor";
import { MapDep } from "./MapDep";
import { MapDeps } from "./MapDeps";
import { InjectableCtor } from "./InjectableCtor";
import { InjectablesManager } from "./InjectablesManager";

/**
 * InjectableRegistrationConfig is used to reconfigure dependency mapping of the injectable being registered
 * Time to time, multiple injectables using the
 */
export class InjectableRegistrationConfig implements MapDep, MapDeps, DependencyInjectionContainer {

   #dic: DependencyInjectionContainer;
   #injectableDescriptor: InjectableDescriptor;

   constructor(dic: DependencyInjectionContainer, injectableDescriptor: InjectableDescriptor) {
      this.#dic = dic;
      this.#injectableDescriptor = injectableDescriptor;
   }

   mapDep(injected: unknown, registered: unknown): MapDep {
      if (this.#injectableDescriptor.mapDep) this.#injectableDescriptor.mapDep.push({ injected, registered });
      return <MapDep>this;
   }

   mapDeps(registered: unknown[]): DependencyInjectionContainer {
      this.#injectableDescriptor.mapDeps = registered;
      return this.#dic;
   }

   /**
    * Proxy container properties & methods
    */

   addSingleton(keyOrCtor: unknown | InjectableCtor, injectableCtor?: InjectableCtor): MapDep & MapDeps {
      return this.#dic.addSingleton(keyOrCtor, injectableCtor);
   }

   addTransient(keyOrCtor: unknown | InjectableCtor, injectableCtor?: InjectableCtor): MapDep & MapDeps {
      return this.#dic.addTransient(keyOrCtor, injectableCtor);
   }

   addInjectable(key: unknown, value: unknown): DependencyInjectionContainer {
      return this.#dic.addInjectable(key, value);
   }


   registerInjectablesManager(injectablesManager: InjectablesManager, injectablesToManage: InjectableCtor[]): void {
      return this.#dic.registerInjectablesManager(injectablesManager, injectablesToManage);
   }

   async resolveAsync<T>(key: unknown, dependant?: Injectable): Promise<T> {
      return this.#dic.resolveAsync<T>(key, dependant);
   }

}