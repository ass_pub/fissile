/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

/**
 * Indentifier can be used to force DiC to inject the injectable dependent on injectable being instantied
 * This need more deep explanation, but it has something to do with injectables manager and objects
 * in AJS (i.e. view is injected to the control placed on it in order to be possible to meessage
 * with it), but injectables manager has to map the dependency correctly
 */
export const IIDependentInjectable = [ "IIDependentInjecable" ];


/**
 * Basic interface for the generic injectable
 */
export interface Injectable extends Object {

   init?(): void;
   initAsync?(): Promise<void>

}


/**
 * Interface for the disposable (transient) injectable
 * If the injectable is disposable it must expect the injectables manager to be
 * injected to itself and call injectableManager.disposeInjectable(this) once it
 * is being disposed
 *
 * Example:
 * @inject(IInjectablesManager, someInjectable)
 * class CoolInjectable {
 *
 *    #injectablesManager: InjectablesManager;
 *    #someInjectable: SomeInjectable;
 *
 *    constructor(injectablesManager: InjectablesManager, someInjectable: SomeInjectable) {
 *       this.#injectablesManager = injectablesManager;
 *       this.#someInjectable = someInjectable;
 *    }
 *
 *    dispose() {
 *       this.injectablesManager.disposeInjectable(this);
 *    }
 *
 * }
 */
export interface DisposableInjectable {
   dispose(): void;
}