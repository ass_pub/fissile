/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { InjectableCtor } from "./InjectableCtor.js";
import { InjectablesManager } from "./InjectablesManager.js";
import { InjectableCtorToInjectablesManagerMapperEntry } from "./InjectablesToInjectablesManagersMapper.js";

/**
 * Holds map of injectable constructors and keys to be injected to them
 */
export default class InjectableCtorsToInjectablesManagersMapper {

   #entries: InjectableCtorToInjectablesManagerMapperEntry[];

   constructor() {
      this.#entries = [];
   }

   /**
    * Adds and InjectableCtor to InjectablesManager mapping entry
    * @param injectablesManager Injectables manager to be used for the given injectable
    * @param injectableCtor Constructor of the injectable requiring dependecies to be injected
    */
   add(injectablesManager: InjectablesManager, injectableCtor: InjectableCtor): boolean {

      const entry = this.findByInjectableCtor(injectableCtor);
      if (!entry) return false;

      const newEntry: InjectableCtorToInjectablesManagerMapperEntry = {
         injectableCtor: injectableCtor,
         injectablesManager: injectablesManager
      };

      this.#entries.push(newEntry);

      return true;

   }

   /**
    * Searches an entry in the injection map and returns it if found, otherwise null is returned
    * @param injectableCtor Injectable sonstructor to be searched for in the injection map
    */
   findByInjectableCtor(injectableCtor: InjectableCtor): InjectableCtorToInjectablesManagerMapperEntry | null {

      const entries = this.#entries.filter(entry => entry.injectableCtor === injectableCtor);
      if (entries.length === 0) return null;
      return entries[0];

   }

   /**
    * Returns all injectables managers existing in the mapper
    */
   allInjectablesManagers(): InjectablesManager[] {
      return this.#entries.map(entry => entry.injectablesManager);
   }

}
