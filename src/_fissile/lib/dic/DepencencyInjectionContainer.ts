/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Injectable } from "./Injectable.js";
import { MapDep } from "./MapDep.js";
import { MapDeps } from "./MapDeps.js";
import { InjectableCtor } from "./InjectableCtor.js";
import { InjectablesManager } from "./InjectablesManager.js";

export interface DependencyInjectionContainer {

   /**
    * Registers injectables manager for the given list of injectables constructors
    * If the another injectables manager is registered for given injectable already the first one is kept
    * Injectables managers are used to manage lifecycle (call specific injectable methods) of the injectable, especially during registration, instantiation and disposing
    * @param injectablesManager Injectables manager to be used for given injectable classes
    * @param injectablesToManage List of injectable classes to be used by the manager specified
    */
   registerInjectablesManager(injectablesManager: InjectablesManager, injectablesToManage: InjectableCtor[]): void;


   /**
    * Registers a singleton injectable
    * Singleton inectable instance lives for the whole application life cycle since its created
    * @param keyOrCtor key under which the injectable will be registered or injectable constructor itsef (used also as the key)
    * @param InjectableCtor injectable constructor to be registered
    */
   addSingleton(keyOrCtor: unknown | InjectableCtor, inectableCtor?: InjectableCtor): MapDep & MapDeps;


   /**
    * Registers a transient inectable
    * New transient inectable instance is created always when injection is required
    * @param keyOrCtor key under which the injectable will be registered or injectable constructor itsef (used also as the key)
    * @param InjectableCtor injectable constructor to be registered
    */
   addTransient(keyOrCtor: unknown | InjectableCtor, inectableCtor?: InjectableCtor): MapDep & MapDeps;


   /**
    * Registers any injectable other than a injectable constructor (i.e. object, array, string, number, boolean)
    *
    * If the array is passed as a value in it will checked if it is arrayof previously registered keys
    * and if so the array of injectables will be later resolved and injected to the injectable as a single
    * array value
    *
    * example:
    * @inject("Providers")
    * class ProviderResolver {
    *    constructor(providers: Provider[]) {
    *       ...
    *    }
    * }
    *
    * dic
    *    .addSingleton("provider1", SampleProvider1)
    *    .addSingleton("provider2", SampleProvider2)
    *    .addInjectable("providers", [ "provider1", "provider2" ]
    *
    * @param key Key under which the injectable is stored
    * @param value Value to be injected when requested
    */
   addInjectable(keyOrValue: unknown, value?: unknown): DependencyInjectionContainer;


   resolveAsync<T>(key: unknown, dependant?: Injectable): Promise<T>;

}