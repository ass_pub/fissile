/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Injectable, IIDependentInjectable } from "./Injectable.js";
import { InjectableCtor } from "./InjectableCtor.js";
import { InjectablesManager } from "./InjectablesManager.js";
import { InjectableCtorsToInjectablesManagersMapper } from "./InjectablesToInjectablesManagersMapper.js";
import { InjectableDescriptor, InjectableType, InjectableScope } from "./InjectableDescriptor.js";
import { DependencyInjectionContainer } from "./DepencencyInjectionContainer.js";
import { MapDep } from "./MapDep.js";
import { MapDeps } from "./MapDeps.js";

import injectableInjectionsMapper from "./InjectableInjectionsMapper.js";
import { InjectableRegistrationConfig } from "./InjectablesRegistrationConfig.js";

/**
 * Default dependency injection container
 * Inspired by Ajs DIC, simplified a bit
 * - DIC does not count and allows the classes decorated using @inject are extended (use composition instead)
 */
export class DIC implements DependencyInjectionContainer {

   /** Default injectables manager to be used when injectables manager realted to the injectable is not found */
   #defaultInjectableManager: InjectablesManager;

   /** Mapper to be used for mapping of injectable constructors to appropriate injectables manager */
   #injectableCtorsToInjectablesManagersMapper: InjectableCtorsToInjectablesManagersMapper | undefined;

   /** Holds all injectables configured on DIC (singletons, transient, objects, values...) */
   #injectableDescriptors: InjectableDescriptor[];


   /**
    * Creates instance of the DI Container
    * @param defaultInjectablesManager Default injectables manager to be used when injectables manager realted to the injectable is not found
    * @param injectableCtorsToInjectablesManagersMapper Mapper to be used for mapping of injectable constructors to appropriate injectables manager
    */
   constructor(defaultInjectablesManager: InjectablesManager, injectableCtorsToInjectablesManagersMapper?: InjectableCtorsToInjectablesManagersMapper) {

      this.#defaultInjectableManager = defaultInjectablesManager;
      this.#injectableCtorsToInjectablesManagersMapper = injectableCtorsToInjectablesManagersMapper;
      this.#injectableDescriptors = [];

      if (this.#defaultInjectableManager.registeredWithDic) this.#defaultInjectableManager.registeredWithDic(this);

      if (this.#injectableCtorsToInjectablesManagersMapper) {
         this.#injectableCtorsToInjectablesManagersMapper.allInjectableManagers().forEach(im => {
            if (im.registeredWithDic) im.registeredWithDic(this);
         });
      }

   }

   /**
    * Registers injectables manager for the given list of injectable constructors
    * If the another injectables manager is registered for given injectable already the first one is kept
    * Injectable managers are used to manage lifecycle (call specific injectable methods) of the injectable, especially during registration, instantiation and disposing
    * @param injectablesManager Injectables manager to be used for given injectable classes
    * @param injectablesToManage List of injectable classes to be used by the manager specified
    */
   registerInjectablesManager(injectablesManager: InjectablesManager, injectablesToManage: InjectableCtor[]): void {

      if (!this.#injectableCtorsToInjectablesManagersMapper) throw new Error("DIC: injectableCtorsToInjectablesManagersMapper was not injected to DIC. Cannot register injectables manager");

      injectablesToManage.forEach(
         injectableToManage => (<InjectableCtorsToInjectablesManagersMapper>this.#injectableCtorsToInjectablesManagersMapper).add(injectablesManager, injectableToManage)
      );

   }


   /**
    * Registers a singleton injectable
    * Singleton injectable instance lives for the whole application life cycle since its created
    * @param keyOrCtor
    */
   addSingleton(keyOrCtor: unknown | InjectableCtor, injectableCtor?: InjectableCtor): MapDep & MapDeps {

      const key: any = keyOrCtor;
      const ctor: InjectableCtor = injectableCtor ? injectableCtor : <InjectableCtor>keyOrCtor;
      const id = this.#addInjectableLocal(InjectableScope.Singleton, key, ctor);

      return new InjectableRegistrationConfig(this, id);
   }


   /**
    * Registers a transient injectable
    * New transient injectable instance is created always when injection is required
    * @param keyOrCtor
    */
   addTransient(keyOrCtor: unknown | InjectableCtor, injectableCtor?: InjectableCtor): MapDep & MapDeps {

      const key: any = keyOrCtor;
      const ctor: InjectableCtor = injectableCtor ? injectableCtor : <InjectableCtor>keyOrCtor;
      const id = this.#addInjectableLocal(InjectableScope.Transient, key, ctor);

      return new InjectableRegistrationConfig(this, id);

   }


   /**
    * Registers any injectable other than a injectable constructor (i.e. object, array, string, number, boolean)
    *
    * If the array is passed as a value in it will checked if it is arrayof previously registered keys
    * and if so the array of injectables will be later resolved and injected to the injectable as a single
    * array value
    *
    * example:
    * @inject("Providers")
    * class ProviderResolver {
    *    constructor(providers: Provider[]) {
    *       ...
    *    }
    * }
    *
    * dic
    *    .addSingleton("provider1", SampleProvider1)
    *    .addSingleton("provider2", SampleProvider2)
    *    .addInjectable("providers", [ "provider1", "provider2" ]
    *
    * @param key Key under which the injectable is stored or the value which will also be used as a key
    * @param value Value to be injected when requested
    */
   addInjectable(keyOrValue: unknown, value?: unknown): DIC {

      const key: any = keyOrValue;
      const val: any = value ? value : keyOrValue;

      const exists = this.#getRegisteredInjectableDescriptor(key);
      if (exists) throw new Error("DIC: Another injectable identified with passed key is alerady registred within the DIC");

      let isArrayOfRegisteredKeys: boolean;

      if (val instanceof Array) {

         isArrayOfRegisteredKeys = true;
         val.forEach(
            key => isArrayOfRegisteredKeys = isArrayOfRegisteredKeys && this.#getRegisteredInjectableDescriptor(key) !== null
         );

      } else {

         isArrayOfRegisteredKeys = false;

      }

      const injectableDescriptor: InjectableDescriptor = {
         key,
         injectableType: isArrayOfRegisteredKeys ? InjectableType.Injectables : InjectableType.Injectable,
      };

      if (isArrayOfRegisteredKeys) {
         injectableDescriptor.deps = <Array<unknown>>val;
      } else {
         injectableDescriptor.instance = val;
      }

      this.#injectableDescriptors.push(injectableDescriptor);

      return this;

   }


   /**
      * Asyngronously instantiates the injectable while it also resolves and eventually instantiates all its dependencies
      * @param id Injectable descriptor of the injectable being instantiated
      * @param dependent NEEDS TO BE DESCRIBED - See IIDependentInjectable constant defined in "Injectable.ts" for details
      */
   async resolveAsync<T>(key: unknown, dependant?: Injectable): Promise<T>;
   async resolveAsync<T>(key: unknown, directInject?: Array<any>): Promise<T>;
   async resolveAsync<T>(key: unknown, dependant?: Injectable, directInject?: Array<any>): Promise<T>;
   async resolveAsync<T>(key: unknown, dependant?: Injectable, directInject?: Array<any>): Promise<T> {

      if (dependant instanceof Array) {
         directInject = dependant;
         dependant = undefined;
      }

      const id: InjectableDescriptor | null = this.#getRegisteredInjectableDescriptor(key);

      if (id === null) throw new Error(`DIC: Failed to resolve required injectable '${key}'`);
      if (!id.deps) id.deps = [];
      if (id.injectableType === InjectableType.Injectables) return await this.#resolveDepsAsync(id, dependant) as any;
      if (id.injectableType !== InjectableType.InjectableClass && (!id.deps || id.deps.length === 0)) return <T>(id.instance);

      // if there are no deps defined for the ctor, try to find inherited class
      if (id.deps.length === 0) {

         let currentCtor = Object.getPrototypeOf(id.ctor);

         while (currentCtor) {
            const iientry = injectableInjectionsMapper.findEntry(currentCtor);
            if (iientry.keys.length > 0) {
               const inheritedId: InjectableDescriptor = {
                  ...id,
                  deps: iientry.keys
               };
               return this.#resolveLocalAsync<T>(inheritedId, dependant, directInject);
            }
            currentCtor = Object.getPrototypeOf(currentCtor);
         }

      }

      return this.#resolveLocalAsync<T>(id, dependant, directInject);

   }


   /**
    * Notifies appropriate registered injectables manager the injectable was registered within the DIC
    * @param descriptor Descripto of the injectable registered
    */
   #notifyInjectableRegistered(descriptor: InjectableDescriptor): void {
      const im: InjectablesManager = this.#getRegisteredInjectablesManager(descriptor);
      if (im.injectableRegistered) im.injectableRegistered(descriptor);
   }


   /**
    * Notifies appropriate registered injectables manager the injectable is being instantiated
    * @param descriptor
    */
   #notifyInjectableInstantiating(descriptor: InjectableDescriptor): void {
      const im: InjectablesManager = this.#getRegisteredInjectablesManager(descriptor);
      if (im.injectableInstantiating) im.injectableInstantiating(descriptor);
   }


   /**
    * Notifies appropriate registered injectables manager the injectable was instantiated
    * @param descriptor
    */
   async #notifyInjectableInstantiated(descriptor: InjectableDescriptor, instance: Injectable): Promise<void> {
      const im: InjectablesManager = this.#getRegisteredInjectablesManager(descriptor);
      if (im.injectableInstantied) return im.injectableInstantied(descriptor, instance);
   }


   /**
    * Returns the injectables manager for given injectable constructor contained within the injectable descriptor
    * @param descriptor Injectable descriptor containing the injectable constructor for which the injectables manager is searched for
    */
   #getRegisteredInjectablesManager(descriptor: InjectableDescriptor): InjectablesManager {

      let im: InjectablesManager | undefined;

      if (this.#injectableCtorsToInjectablesManagersMapper) {
         im = this.#injectableCtorsToInjectablesManagersMapper.findByInjectableCtor(<InjectableCtor>descriptor.ctor).injectablesManager;
      }

      return im || this.#defaultInjectableManager;

   }


   /**
    * Finds a registered injectable and returns it if found, otherwise null is returned
    * @param key Key under the injectable is registered
    */
   #getRegisteredInjectableDescriptor(key: unknown): InjectableDescriptor | null {

      const ids = this.#injectableDescriptors.filter(injectable => injectable.key === key);

      if (ids.length > 0) {
         return ids[0];
      } else {
         return null;
      }

   }


   /**
    * Helper to add singleton or transient injectable to DIC
    * @param injectableScope Injectable scope
    * @param key Key under which the injectable is registered
    * @param ctor Injectable constructor
    */
   #addInjectableLocal(injectableScope: InjectableScope, key: unknown, ctor: InjectableCtor): InjectableDescriptor {

      const exists = this.#getRegisteredInjectableDescriptor(key);
      if (exists) throw new Error("DIC: Another injectable identified with passed key is alerady registred within the DIC");

      const injectableDescriptor: InjectableDescriptor = {
         key,
         injectableType: InjectableType.InjectableClass,
         scope: injectableScope,
         ctor,
         mapDep: [],
         deps: injectableInjectionsMapper.findEntry(ctor).keys
      };

      this.#injectableDescriptors.push(injectableDescriptor);

      this.#notifyInjectableRegistered(injectableDescriptor);

      return injectableDescriptor;

   }


   /**
    * Asyngronously instantiates the injectable while it also resolves and eventually instantiates all its dependencies
    * @param id Injectable descriptor of the injectable being instantiated
    * @param dependent NEEDS TO BE DESCRIBED - See IIDependentInjectable constant defined in "Injectables.ts" for details
    */
   async #resolveLocalAsync<T>(id: InjectableDescriptor, dependent?: Injectable, directInject?: Array<any>): Promise<T> {

      directInject = directInject || [];

      if (id.scope === InjectableScope.Singleton && id.instance) return <T>id.instance;

      this.#notifyInjectableInstantiating(id);

      if (id.scope === InjectableScope.Singleton && (id.deps === undefined || id.deps.length === 0)) {
         id.instance = new (<InjectableCtor>id.ctor)(...directInject);
         await this.#notifyInjectableInstantiated(id, <Injectable>id.instance);
         return <T>(id.instance);
      }

      if (id.scope === InjectableScope.Transient && (id.deps === undefined || id.deps.length === 0)) {
         const instance: T = <T>new (<InjectableCtor>id.ctor)(...directInject);
         await this.#notifyInjectableInstantiated(id, instance);
         return instance;
      }

      const deps: any[] = await this.#resolveDepsAsync(id, dependent);

      if (id.scope === InjectableScope.Singleton) {
         /* eslint-disable prefer-spread */
         id.instance = new ((<InjectableCtor>id.ctor).bind.apply((<InjectableCtor>id.ctor), [ null, ...deps, ...directInject ]));
         await this.#notifyInjectableInstantiated(id, <Injectable>id.instance);
         return <T>(id.instance);
      }

      if (id.scope === InjectableScope.Transient) {
         /* eslint-disable prefer-spread */
         const instance = new ((<InjectableCtor>id.ctor).bind.apply((<InjectableCtor>id.ctor), [ null, ...deps, ...directInject ]));
         await this.#notifyInjectableInstantiated(id, instance);
         return instance;
      }

      throw new Error("DIC: Resole fatal error, probably invalid scope specified.");

   }

   /**
    * Resolves all dependancies of the injectable being instantiated
    * @param id Descriptor of the injectable injectable being instantiated
    * @param dependent NEEDS TO BE DESCRIBED - See IIDependentInjectable constant defined in "Injectables.ts" for details
    */
   async #resolveDepsAsync(id: InjectableDescriptor, dependent?: Injectable): Promise<any[]> {

      const resolvedDeps: any[] = [];
      const mappedDeps: any[] = this.#mapDeps(id);

      for (const mappedDep of mappedDeps) {

         // dependencies grouped to object
         if (typeof mappedDep === "object" && this.#getRegisteredInjectableDescriptor(mappedDep) === null) {

            const objDeps: any = {};

            for (const key in mappedDep) {

               const dep = mappedDep[key];

               if (dep === IIDependentInjectable) {
                  objDeps[key] = dependent;
                  continue;
               }

               if (id.mapDep) {
                  const mapping = id.mapDep.find(m => m.injected === mappedDep[key] );
                  if (mapping) {
                     objDeps[key] = await this.resolveAsync(mapping.registered, dependent);
                     continue;
                  }
               }

               objDeps[key] = await this.resolveAsync(dep, dependent);

            }

            resolvedDeps.push(objDeps);

         } else {

            if (mappedDep === IIDependentInjectable/* && dependent */) {
               resolvedDeps.push(dependent);
            } else {
               resolvedDeps.push(await this.resolveAsync(mappedDep, dependent));
            }

         }

      }

      return resolvedDeps;

   }

   /**
      * Maps the dependency keys declared using the @inject decorator to keys registered in the DI
      * @param id Descriptor of the injectable being instantiated
      */
   #mapDeps(id: InjectableDescriptor): any[] {

      if (id.mapDeps) return id.mapDeps;

      const map: any[] = [];

      if (!id.deps) return map;

      for (const dep of id.deps) {

         let remapped = false;

         if (id.mapDep) {

            for (const mapdep of id.mapDep) {
               if (dep === mapdep.injected) {
                  map.push(mapdep.registered);
                  remapped = true;
                  break;
               }
            }

         }

         if (!remapped) map.push(dep);

      }

      return map;
   }


}
