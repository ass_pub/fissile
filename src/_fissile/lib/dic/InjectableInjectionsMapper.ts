/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { InjectableCtor } from "./InjectableCtor";

interface InjectionEntry {
   target: InjectableCtor | null;
   keys: any[]
}

/**
 * Holds map of injectable constructors and keys to be injected to them
 */
class InjectableInjectionsMapper {

   private entries: InjectionEntry[];

   constructor() {
      this.entries = [];
   }

   /**
    * Adds and entry to injection map
    * @param injectableCtor Constructor of the injectable requiring dependecies to be injected
    * @param keys Keys to be looked up in DIC and injected
    */
   addEntry(injectableCtor: InjectableCtor, keys: any[]): boolean {

      const entry = this.findEntry(injectableCtor);
      if (!entry) return false;

      const newEntry: InjectionEntry = {
         target: injectableCtor,
         keys: keys
      };

      this.entries.push(newEntry);

      return true;

   }

   /**
    * Searches an entry in the injection map and returns it if found, otherwise null is returned
    * @param injectableCtor Injectable sonstructor to be searched for in the injection map
    */
   findEntry(injectableCtor: InjectableCtor): InjectionEntry {

      const entries = this.entries.filter(entry => entry.target === injectableCtor);
      if (entries.length === 0) return { target: null, keys: [] };
      return entries[0];

   }


}

/**
 * Injection map instance used within the application
 */
const injectableInjectionsMapper = new InjectableInjectionsMapper();

export default injectableInjectionsMapper;
