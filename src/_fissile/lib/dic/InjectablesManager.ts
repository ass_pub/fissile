/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { DependencyInjectionContainer } from "./DepencencyInjectionContainer";
import { InjectableDescriptor } from "./InjectableDescriptor";
import { Injectable } from "./Injectable";

/**
 * Injectables managers are used to manage the injectables registered to DIC
 * especially during registration, instantiating, property injections or injectable disposals
 * Injectables managers can also manage additional injectable life cycles
 */
export interface InjectablesManager {

   /**
    * Called when the injectables manager is registered within DIC
    * @param dic DIC which registered the injectables manager
    */
   registeredWithDic?(dic: DependencyInjectionContainer): void;

   /**
    * DIC Calls the injectableRegistered method once the injectable is sucessfully registered to DI container
    * @param descriptor Descriptor of the injectable being registered within DIC
    */
   injectableRegistered?(descriptor: InjectableDescriptor): void;

   /**
    * DIC calls the injectableInstantiating method when all dependencies are resolved but
    * before the injectable is instantiated
    * @param descriptor Descriptor of the injectable being instatniated
    */
   injectableInstantiating?(descriptor: InjectableDescriptor): void;

   /**
    * DIC calls the injectableInstantiated method when the injectable is instantiated but before properties
    * are injected to it
    * @param descriptor Descriptor of the instantiated injectable
    * @param instance Instance of the injectable created by DIC
    */

   injectableInstantied?(descriptor: InjectableDescriptor, instance: Injectable): Promise<void>;

   /**
    * Called by injectable when a transient injectable is being disposed
    * @param instance Injectable instance being disposed
    */
   injectableDisposing?(instance: Injectable): void;

}
