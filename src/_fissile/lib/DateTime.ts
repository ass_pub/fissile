/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

export const DateTime = {

   toYYYYMMDD: (date: number): string => {

      const dObj = new Date(date);

      let m = (dObj.getMonth() + 1).toString();
      let d = (dObj.getDate()).toString();
      const y = (dObj.getFullYear()).toString();

      if (m.length < 2) m = "0" + m;
      if (d.length < 2) d = "0" + d;

      return [ y, m, d ].join("-");

   },

   toYYYYMMDDHHMM: (date: number): string => {

      const dObj = new Date(date);

      let h = dObj.getHours().toString();
      let m = dObj.getMinutes().toString();

      if (h.length < 2) h = "0" + h;
      if (m.length < 2) m = "0" + m;

      return DateTime.toYYYYMMDD(date) + " " + [ h, m ].join(":");

   },

   toYYYYMMDDHHMMSS: (date: number): string => {

      const dObj = new Date(date);

      let h = dObj.getHours().toString();
      let m = dObj.getMinutes().toString();
      let s = dObj.getSeconds().toString();

      if (h.length < 2) h = "0" + h;
      if (m.length < 2) m = "0" + m;
      if (s.length < 2) s = "0" + s;

      return DateTime.toYYYYMMDD(date) + " " + [ h, m, s ].join(":");


   }

};