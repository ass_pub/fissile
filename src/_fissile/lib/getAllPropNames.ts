/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

export function getAllPropNames(
   object: unknown,
   excludeBrowserInternals = true,
   excludeProtected = true,
   excludeGetSet = false,
   excludeNames: string[] = [ "constructor", "init", "initAsync", "parent" ]
): string[] {

   const props: string[] = [];

   let obj: any = object;
   while (obj && Object.getPrototypeOf(obj) !== null) {

      const descriptors = (<any>Object).getOwnPropertyDescriptors(obj);

      for (const key of Object.getOwnPropertyNames(obj)) {

         if (excludeBrowserInternals && key.match(/__.*__/)) continue;
         if (excludeProtected && key.match(/_[^_].*[^_][^_]/)) continue;
         if (excludeGetSet && descriptors[key] && (descriptors[key].get !== undefined || descriptors[key].set !== undefined)) continue;
         if (excludeNames.indexOf(key) !== -1) continue;

         if (props.indexOf(key) !== -1) continue;

         props.push(key);

      }

      obj = Object.getPrototypeOf(obj);
   }

   return props;

}
