/* *****************************************************************************
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

This file is kept unminified just for education purposes.
It is strictly prohibited to use content of this file for any other purposes.
*******************************************************************************/


import { DummyViewModel } from "../common/DummyViewModel.js";
import { LayoutViewModel } from "../common/Layout/LayoutViewModel.js";
import { ChildViewModelCreationInfo, ViewModel } from "../../_fissile/viewmodels/ViewModel.js";

export class HomeViewModel extends ViewModel {

   layout: LayoutViewModel | undefined;

   eventHandler() {
      console.log("Handled");
   }

   override async _initAsync(): Promise<void> {

      this.layout = await this._createViewModel<typeof LayoutViewModel>(
         LayoutViewModel,
         undefined,//,<ChildViewModelCreationInfo>{ ctor: Dummy },
         <ChildViewModelCreationInfo>{ ctor: DummyViewModel },
         <ChildViewModelCreationInfo>{ ctor: DummyViewModel },
         <ChildViewModelCreationInfo>{ ctor: DummyViewModel },
         undefined//,<ChildViewModelCreationInfo>{ ctor: Dummy }
      );

   }

}
