/* *****************************************************************************
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

This file is kept unminified just for education purposes.
It is strictly prohibited to use content of this file for any other purposes.
*******************************************************************************/

import { ViewModel } from "../../_fissile/viewmodels/ViewModel.js";
import { DateTime } from "../../_fissile/lib/DateTime.js";
import { ArrayViewModel } from "./ArrayViewModel.js";

export class DummyViewModel extends ViewModel {

   #i = 1;

   test = "";
   array: unknown[] = [];

   templateArray: ArrayViewModel[] | undefined;

   protected override async _initAsync(): Promise<void> {

      this.templateArray = [];

      for (let i = 1; i < 4; i++) {
         this.templateArray.push(await this._createViewModel<typeof ArrayViewModel>(ArrayViewModel, i, i.toString()));
      }

      this.#update();

      setInterval(() => {

         this.#update();
         this._render();

      }, 0);

   }

   #update() {

      this.test = DateTime.toYYYYMMDDHHMMSS(Date.now());
      if (!this.templateArray) return;

      this.array = [];

      for (let i = 0; i < 3; i++) {
         this.array.push({ text: (this.#i + i).toString() });

         this.templateArray[i].index = this.#i + i;
         this.templateArray[i].value = (this.#i + i).toString();
      }

      this.#i += 3;

   }

}
