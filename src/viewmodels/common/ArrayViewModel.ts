/* *****************************************************************************
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

This file is kept unminified just for education purposes.
It is strictly prohibited to use content of this file for any other purposes.
*******************************************************************************/

import { BaseViewModelCreationArgs, ViewModel } from "../../_fissile/viewmodels/ViewModel.js";

export class ArrayViewModel extends ViewModel {

   protected _index: number;
   get index(): number { return this._index; }
   set index(value: number) { this._index = value; this._render(); }

   protected _value: string;
   get value(): string { return this._value; }
   set value(value: string) { this._value = value; this._render(); }

   constructor(
      ccArgs: BaseViewModelCreationArgs,
      index: number,
      value: string
   ) {
      super(ccArgs);
      this._index = index;
      this._value = value;
   }

}