/* *****************************************************************************
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

This file is kept unminified just for education purposes.
It is strictly prohibited to use content of this file for any other purposes.
*******************************************************************************/

import { BaseViewModelCreationArgs, ViewModel } from "../../../_fissile/viewmodels/ViewModel.js";

export class NavbarViewModel extends ViewModel {

   #test: string;

   constructor(
      ccArgs: BaseViewModelCreationArgs,
      test: string
   ) {
      super(ccArgs);

      this.#test = test;

      console.log(this.#test);
   }

   /*
   override async _initAsync() {
   }
   */

}