/* *****************************************************************************
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

This file is kept unminified just for education purposes.
It is strictly prohibited to use content of this file for any other purposes.
*******************************************************************************/

import { inject } from "../../../_fissile/lib/dic/inject.decorator.js";
import { baseViewModelCreationArgs, BaseViewModelCreationArgs, ChildViewModelCreationInfo, ViewModel } from "../../../_fissile/viewmodels/ViewModel.js";

@inject(baseViewModelCreationArgs)
export class LayoutViewModel extends ViewModel {

   protected _header?: ViewModel;
   get header(): ViewModel | undefined { return this._header; }

   protected _sidebarLeft?: ViewModel;
   get sidebarLeft(): ViewModel | undefined { return this._sidebarLeft; }

   protected _content?: ViewModel;
   get content(): ViewModel | undefined { return this._content; }

   protected _sidebarRight?: ViewModel;
   get sidebarRight(): ViewModel | undefined { return this._sidebarRight; }

   protected _footer?: ViewModel;
   get footer(): ViewModel | undefined { return this._footer; }

   protected _header_ = "HEADER";
   get header_(): string { return this._header_; }

   protected _sidebarLeft_ = "SIDEBAR_LEFT";
   get sidebarLeft_(): string { return this._sidebarLeft_; }

   protected _content_ = "CONTENT";
   get content_(): string { return this._content_; }

   protected _sidebarRight_ = "SIDEBAR_RIGHT";
   get sidebarRight_(): string { return this._sidebarRight_; }

   protected _footer_ = "FOOTER";
   get footer_(): string { return this._footer_; }

   constructor(
      ccArgs: BaseViewModelCreationArgs,
      protected _headerCI?: ChildViewModelCreationInfo,
      protected _sidebarLeftCI?: ChildViewModelCreationInfo,
      protected _contentCI?: ChildViewModelCreationInfo,
      protected _sidebarRightCI?: ChildViewModelCreationInfo,
      protected _footerCI?: ChildViewModelCreationInfo
   ) {
      super(ccArgs);
   }

   protected override async _initAsync() {

      if (this._headerCI) this._header = await this._createViewModel(this._headerCI);
      if (this._sidebarLeftCI) this._sidebarLeft = await this._createViewModel(this._sidebarLeftCI);
      if (this._contentCI) this._content = await this._createViewModel(this._contentCI);
      if (this._sidebarRightCI) this._sidebarRight = await this._createViewModel(this._sidebarRightCI);
      if (this._footerCI) this._footer = await this._createViewModel(this._footerCI);

   }

}