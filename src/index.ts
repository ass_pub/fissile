/* *****************************************************************************
Copyright (c)2016-2022 Atom Software Studios. All rights reserved.

This file is kept unminified just for education purposes.
It is strictly prohibited to use content of this file for any other purposes.
*******************************************************************************/

import { Templating } from "./_fissile/templating/Templating.js";
import { DIC } from "./_fissile/lib/dic/DIC.js";
import { InjectablesManagerDefault } from "./_fissile/lib/dic/InjectablesManagerDefault.js";
import { DOMPatcher } from "./_fissile/templating/DOMPatcher.js";

import { HomeViewModel } from "./viewmodels/home/HomeViewModel.js";
import { LayoutViewModel } from "./viewmodels/common/Layout/LayoutViewModel.js";
import { DummyViewModel } from "./viewmodels/common/DummyViewModel.js";
import { ArrayViewModel } from "./viewmodels/common/ArrayViewModel.js";
import { RenderQueue } from "./_fissile/viewmodels/RenderQueue.js";

import * as rxjs from "rxjs";

const $a = rxjs.EMPTY;
console.log($a, "");

const injectablesManagerDefault = new InjectablesManagerDefault();
const dic = new DIC(injectablesManagerDefault);


dic
   .addInjectable(DIC, dic)

   /** Templatig */
   .addInjectable("Templating-templatesPath", "./templates")    // path to templates root
   .addSingleton(DOMPatcher)
   .addSingleton(Templating)

   /** ViewModel configuration */
   .addInjectable("RootRenderTarget", "#root")                  // root render target (where views without a parent will be rendered)
   .addSingleton(RenderQueue)

   /**
    * ViewModels and mapping to view templates
    */
   .addInjectable("ArrayTemplatePath", "common/array")
   .addTransient(ArrayViewModel).mapDep("TemplateName", "ArrayTemplatePath")

   .addInjectable("DummyTemplatePath", "common/dummy")
   .addTransient(DummyViewModel).mapDep("TemplateName", "DummyTemplatePath")

   .addInjectable("HomeTemplatePath", "home/index")
   .addTransient(HomeViewModel).mapDep("TemplateName", "HomeTemplatePath")

   .addInjectable("LayoutTemplatePath", "common/layout/index")
   .addTransient(LayoutViewModel).mapDep("TemplateName", "LayoutTemplatePath");

dic.resolveAsync<HomeViewModel>(HomeViewModel);
