# Reusable Components

## Introduction

Fissile framework is using the Model-View-ViewModel design pattern. Just to make it clear, what it means in context of the Fissile framework it is shortly summarized here.

**View**

View is something that user can see on the screen. It is some kind of user interface allowing the user to read or enter some kind of data including multimedia (further referenced as a content), and/or execute some actions. Each view have usually bunch of properties defining the structure of the displayed content, look of the user interface and set of actions the user can execute. This set of properties is called **View State** and can be usually, at least partly, changed dynamically in application runtime. When the view state changes, the view structure, look, content and set of actions has to be changed accordingly. In some frameworks this is happening automatically, usually with automated data binding procedures, other frameworks needs an explicit call to some function or method to let the framework know the view state has been changed and updating of the view is necessary. The second case is valid for the Fissile framework.

In Fissile framework, the final and complete view (the HTML web page) is usually composed out of tree of **reusable views**. In another frameworks, these are usually called components, but the view name better describes the purpose and functionality relationship to the user interface as component can be any other application part related to any other functionality. Each view in Fissile framework is described with:

- **HTML template** As usually, the HTML code defines the structure of the content displayed to the user. Fissile HTML template is standard and valid HTML code free of any non-standard elements, attributes or templating language code. Only the one exception are data placeholders, but these are also valid according to the HTML standards.
- **CSS styling** Standard CSS styling placed directly to the template in form of the &lt;style&gt; tag or placed to the external style sheet file(s) defines the look of the content presented to the user.
- **View State** View state contains the information about the views to be used, the data mappings, data to be shown to the user and the data controlling the structure and look of the view.
- **View Code Behind** As it may be time to time required to manipulate a DOM directly or to execute some actions based on events occurred in browser (i.e. run animations or react to animation states, move elements over screen, compute a look which is currently unsupported by CSS standards, make some view functionality more performant than it can be achieved with the framework standard API), it has to be possible to write some JavaScript code implementing such functionalities. This code can be placed to the View Code Behind and is related just to the view.

View can be and usually is composed out of another **reusable views**. Details will be discussed later in this document.


**ViewModel**

ViewModels defines the behavior of the user interface. ViewModel in the Fissile framework is reusable encapsulation of the View State and behavior related to the single view or a tree of views. View models are responsible for making decisions, which, when and how views will be used. For this purposes they can receive events incoming from views and/or from another application components, they can implement internal functions for view state manipulation and data to view state transformations.

**Models**

Models are not directly implemented by the framework. Models will be usually implemented as injectable application components (usually services providing data from stores and/or server).

---

This has to be rewritten


Fissile framework follows the HTML (or better said DOM - Document Object Model) structure and behavior. DOM structure is a tree of objects representing the final look and behavior of the web page in the web browser. Normally, the DOM tree is constructed just from the HTML file and included or accompanying style sheets. Its behavior can be scripted using included or explicitly loaded Java Script.

When a frontend JavaScript framework is used, the DOM tree (and the final look of the web page) is usually constructed on the fly using a JavaScript code based on HTML templates which can be preprocessed and/or precompiled, even to the JavaScript code during the build time or, they parsed and processed on the fly when the view should be displayed or updated.

The base idea is the same for Fissile as it has been primarily inspired by the DOM patching presented by other frameworks. But the chosen approach is a little bit different, compared to other frameworks. Main idea and the main reason why the Fissile Framework has been developed was to make it possible to completely separate the HTML/CSS code (views) from the executive code (controllers, presenters, view models or other view controlling methods) and also to avoid mixing of the HTML code with templating language extensions or with JavaScript code. Of course, this is not completely possible as reusable HTML code has to have at least data placeholders defined, but out of that, this is achievable when some rules defined by the Fissile framework are followed.


 When the Fissile framework renders the final DOM structure it uses only standard HTML5 features and tags to extend source templates in order to keep templates accessible from the **View behind** code and reusable. The final DOM tree is built just from two sources - the **View State** and **Views**. View, in context of the Fissile framework, means just plain HTML code template with the data placeholders and CSS stylesheets used to define the look of the final page. As it is time to time to script some behavior of the DOM, i.e. react to some events, animation state changes or to manipulate the DOM directly, it is possible to use the **View code behind** scripts which will be described later. **View State** in Fissile framework means the plain JavaScript object tree following some rules. It defines, what templates will be used and when (with some exceptions described later) and it also holds the data to be filled to templates.

---

The following **View State** example can be defined as object literal and can be directly mapped to the final DOM using example templates shown bellow. For more details about **View State** and its composing see View State section bellow. For details about templating and its

```JavaScript
{
   // This is View State root node and it name specifies
   // standard CSS (or DOM) selector defining the target
   // node in the main index.html file to which the final
   // view DOM tree will be rendered
   "#rootSlotInTheIndexHTMLFileSelector": {

      "view1": { // path to the template to be used

         id: "someUniqueViewId",  // unique name of the rendered view
         viewProperty: "Hello",   // property to be replaced in the template by the text value

         childrenView: {          // property defining the view state branch for children views

            "test/view2": {       // path to the template to be used

               id: "anotherUniqueViewId"   // unique id of the rendered view
               viewProperty: "World!"      // some property
               viewArrayProperty: [        // another property - as it is an array the appropriate element
                  { value: 1 },            // tree identified by the viewArrayProperty id will be rendered
                  { value: 2 },            // multiple times
                  { value: 3 }
               ]

            }

         }
      }

   }
}
```

Corresponding (simplified) HTML templates to this View State can look like:

templates/view1.html
```HTML
<div>{viewProperty}</div>
<slot id="childrenView" /><!-- placeholder for children view(s) -->
```

templates/test/view2.html
```HTML
<div>{viewProperty}</div>
<div id="viewArrayProperty">{value}</div>
```


Previously defined View State and templates will be rendered as:

```HTML
...
<body>
   <!-- This is the root element located in the index.html file loaded and displayed by the browser -->
   <slot id="rootSlotInTheIndexHTMLFileSelector">

      <!-- this is the slot representing a unique view mapped to concrete view state object in the tree -->
      <slot data-type="template" data-template="view1" id="someUniqueViewId">
         <div>Hello</div><!-- div from the view1.html template filled with the viewProperty data from the view state -->
         <slot id="childrenView"><!-- slot from the template - it is a placeholder for children views -->
            <slot data-type="template" data-template="test/view1" id="anotherUniqueViewId">
               <div>World</div>
               <slot data-type="array" id="viewArrayProperty">
                  <div>1</div>
                  <div>2</div>
                  <div>3</div>
               </slot>
            </slot>
         </slot>
      </slot>

   </slot>
</body>
...
```

As you can see, there is a lot of generated slot elements in the final code. These slots are used to identify rendered components and makes it possible to simply and effectively update rendered components based on the state changes without need of using any non-standard DOM object annotations or properties.

Fissile components can consist of three main parts:

#### View Models

View model is standard JavaScript object containing the data to be rendered by the template, i.e:

#### Templates

Templates are reusable HTML snippets without containing any special JavaScript code and/or another templating language.

#### Views








---

##### Notes


This is just a note and must be elaborated!.



View state specifies templates to be used and data to be mapped to these templates

The following example contains a complete view state, but any part starting with a slot ID can be used:

Please note there is definitely still room for performance optimizations, especially in array processing.

```TypeScript
const viewState = {

   "body": { // Root target element - should be a slot - standard slector

      /** template or array of templates can reside in the slot (see id_slot_with_template_array bellow) */
      /** its ment mainly to be possible to render array of same components multiple times
      helloWorld: {

         /** These are standard properties - can be mapped to attribute values or text nodes */
         title: "This is title",
         hello_prop: "hello",
         world_prop: "world",

         /** This is an array property - the tag with same ID will be repeated as many times as array has elements */
         array_prop: [
            { index: 1, value: "one" },        /** properties for repeated tag */
            { index: 2, value: "two" },
            { index: 3, value: "three" },
         ],

         /** Id of the slot where children templates can be mapped to */]
         id_slot: {

            /** Name of (path to) children template */
            childTemplateName: {
               /** Same type of props in hello world can be here */
            }

         }

         /** Id of the slot as in previous case */
         id_slot_with_template_array: [

            /** Name of (path to) children template to be repeated multiple times */
            childTemplateName: {
               { /* props for repeated component */ }
               { /* props for repeated component */ }
            ]

         ]

      }
   }
};
```
